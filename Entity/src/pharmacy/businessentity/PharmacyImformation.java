package pharmacy.businessentity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pharmacy.entity.Customer;

public class PharmacyImformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String itemName;
	private String itemId;
	private String  medType;
	private String firstName;
	private String pharmacyName;
	private String employeName;
	private String presbectName;
	private String subName;
	private String regNumber;
	private String customerGender;
	
	private String  lastName;
	
	private Date invDate;
	
	private BigDecimal itemSum;
	private BigDecimal itemQty;
	private BigDecimal itempkId;
	private BigDecimal subPkId;
	private BigDecimal itemNumber;
	private BigDecimal itemPackage;
	private BigDecimal orderSum;
	private BigDecimal wareHouseBalance;
	
	private int customerAge;
	private int isPackage;
	
	private List<PharmacyImformation> pharmacyImformations;
	private List<PharmacyImformation> subOrganizations;
	
	public PharmacyImformation() {
		super();
	}
	
	public PharmacyImformation(BigDecimal pkId,String subName,BigDecimal qty) {
		this.subPkId =pkId;
		this.subName =subName;
		this.itemQty = qty;
	}
	
	//LogicPharmacy -> getItemReport()
	
	
	public PharmacyImformation(BigDecimal pkId,String subName) {
		this.subPkId=pkId;
		this.subName=subName;
	}
	
	public PharmacyImformation(String name ,String id,BigDecimal sum) {
		this.itemName=name;
		this.itemId =id;
		this.itemNumber =sum;
	}
	
	public PharmacyImformation(String name ,String id,String firstName,byte isPackage,BigDecimal sum) {
		this.itemName=name;
		this.itemId =id;
		this.itemNumber =sum;
		this.setIsPackage((int) isPackage);
		this.employeName =firstName;
	}

	
	
	public PharmacyImformation(String type,String itemName,String itemId,BigDecimal sum) {
		this.itemName=itemName;
		this.itemId=itemId;
		this.itemSum  = sum;
		this.medType = type;
	}
	
	// static impormation
	public PharmacyImformation(Date date,Customer customer,String employeName,String pharmacyName,BigDecimal sum) {
		this.invDate =date;
		this.firstName  =customer.getFirstName();
		this.lastName  =  customer.getLastName();
		this.orderSum =sum;
		this.pharmacyName =pharmacyName;
//		this.presbectName = prescribeName;
		this.employeName = employeName;
		this.customerAge = customer.getAge();
		this.customerGender = customer.getGenderString();
	}
	
	public PharmacyImformation(BigDecimal itemNumber) {
		this.itemNumber= itemNumber;
	}
	
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public BigDecimal getItemSum() {
		return itemSum;
	}

	public void setItemSum(BigDecimal itemSum) {
		this.itemSum = itemSum;
	}

	public String getMedType() {
		return medType;
	}

	public void setMedType(String medType) {
		this.medType = medType;
	}

	public BigDecimal getItempkId() {
		return itempkId;
	}

	public void setItempkId(BigDecimal itempkId) {
		this.itempkId = itempkId;
	}

	public BigDecimal getItemNumber() {
		return itemNumber;
	}

	public void setItemNumber(BigDecimal itemNumber) {
		this.itemNumber = itemNumber;
	}

	public BigDecimal getItemPackage() {
		return itemPackage;
	}

	public void setItemPackage(BigDecimal itemPackage) {
		this.itemPackage = itemPackage;
	}

	public List<PharmacyImformation> getPharmacyImformations() {
		if(pharmacyImformations==null)
			pharmacyImformations = new ArrayList<>();
		return pharmacyImformations;
	}

	public void setPharmacyImformations(List<PharmacyImformation> pharmacyImformations) {
		this.pharmacyImformations = pharmacyImformations;
	}

	
	public String getPharmacyName() {
		return pharmacyName;
	}

	public void setPharmacyName(String pharmacyName) {
		this.pharmacyName = pharmacyName;
	}

	public String getPresbectName() {
		return presbectName;
	}

	public void setPresbectName(String presbectName) {
		this.presbectName = presbectName;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public String getEmployeName() {
		return employeName;
	}

	public void setEmployeName(String employeName) {
		this.employeName = employeName;
	}

	public BigDecimal getWareHouseBalance() {
		if(wareHouseBalance==null)
			wareHouseBalance =  BigDecimal.ZERO;
		return wareHouseBalance;
	}

	public void setWareHouseBalance(BigDecimal wareHouseBalance) {
		this.wareHouseBalance = wareHouseBalance;
	}

	public String getSubName() {
		return subName;
	}

	public void setSubName(String subName) {
		this.subName = subName;
	}

	public BigDecimal getSubPkId() {
		return subPkId;
	}

	public void setSubPkId(BigDecimal subPkId) {
		this.subPkId = subPkId;
	}

	public BigDecimal getItemQty() {
		if(itemQty==null)
			itemQty =BigDecimal.ZERO;
		return itemQty;
	}

	public void setItemQty(BigDecimal itemQty) {
		this.itemQty = itemQty;
	}

	public List<PharmacyImformation> getSubOrganizations() {
		if(subOrganizations==null)
			subOrganizations = new ArrayList<>();
		return subOrganizations;
	}

	public void setSubOrganizations(List<PharmacyImformation> subOrganizations) {
		this.subOrganizations = subOrganizations;
	}

	public String getCustomerGender() {
		return customerGender;
	}

	public void setCustomerGender(String customerGender) {
		this.customerGender = customerGender;
	}

	public int getCustomerAge() {
		return customerAge;
	}

	public void setCustomerAge(int customerAge) {
		this.customerAge = customerAge;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getInvDate() {
		return invDate;
	}

	public void setInvDate(Date invDate) {
		this.invDate = invDate;
	}

	public BigDecimal getOrderSum() {
		return orderSum;
	}

	public void setOrderSum(BigDecimal orderSum) {
		this.orderSum = orderSum;
	}

	public int getIsPackage() {
		return isPackage;
	}

	public void setIsPackage(int isPackage) {
		this.isPackage = isPackage;
	}

}
