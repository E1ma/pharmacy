package pharmacy.businessentity;

import java.math.BigDecimal;
import java.util.Date;

public class CustomerExamination {
	private BigDecimal examinationPkId;
	private Date examinationDate;
	private int count;
	
	private String examinationName;
	private String examinationValue;
	private String elementName;
	
	public CustomerExamination(){
		super();
	}
	public CustomerExamination(String exaName,Date date,String value ,String elemenName){
		this.examinationName =exaName;
		this.examinationDate =date;
		this.examinationValue=value;
		this.elementName =elemenName;
	}

	public BigDecimal getExaminationPkId() {
		return examinationPkId;
	}

	public void setExaminationPkId(BigDecimal examinationPkId) {
		this.examinationPkId = examinationPkId;
	}

	public String getExaminationName() {
		return examinationName;
	}

	public void setExaminationName(String examinationName) {
		this.examinationName = examinationName;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Date getExaminationDate() {
		return examinationDate;
	}

	public void setExaminationDate(Date examinationDate) {
		this.examinationDate = examinationDate;
	}
	public String getExaminationValue() {
		return examinationValue;
	}
	public void setExaminationValue(String examinationValue) {
		this.examinationValue = examinationValue;
	}
	public String getElementName() {
		return elementName;
	}
	public void setElementName(String elementName) {
		this.elementName = elementName;
	}
	
	
}