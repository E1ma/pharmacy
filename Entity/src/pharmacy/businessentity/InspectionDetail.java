package pharmacy.businessentity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONObject;

import pharmacy.entity.CustomerDiagnose;
import pharmacy.entity.Inspection;
import pharmacy.entity.InspectionDtl;

public class InspectionDetail {
	private Inspection inspection;

	private List<JSONObject> pain;

	private List<JSONObject> checkup;

	private List<JSONObject> q;

	private List<JSONObject> plan;

	private List<CustomerDiagnose> customerDiagnoses;

	private List<InspectionDtl> inspectionDtl;


	private BigDecimal inspectionPkId;
	private BigDecimal typeDtlPkId;
	private String typeDtl;
	private String subName;
	private String employeeName;
	private Date date;

	public InspectionDetail() {
		super();
	}

	public InspectionDetail(String dtl, BigDecimal pkId) {
		this.typeDtl = dtl;
		this.typeDtlPkId = pkId;
	}

	public Inspection getInspection() {
		return inspection;
	}

	public InspectionDetail(List<JSONObject> p) {
		this.pain = p;
	}

	public void setInspection(Inspection inspection) {
		this.inspection = inspection;
	}

	public List<JSONObject> getPain() {
		if (pain == null)
			pain = new ArrayList<JSONObject>();
		return pain;
	}

	public void setPain(List<JSONObject> pain) {
		this.pain = pain;
	}

	public List<CustomerDiagnose> getCustomerDiagnoses() {
		if (customerDiagnoses == null)
			customerDiagnoses = new ArrayList<CustomerDiagnose>();
		return customerDiagnoses;
	}

	public void setCustomerDiagnoses(List<CustomerDiagnose> customerDiagnoses) {
		this.customerDiagnoses = customerDiagnoses;
	}

	public List<InspectionDtl> getInspectionDtl() {
		if (inspectionDtl == null)
			inspectionDtl = new ArrayList<InspectionDtl>();
		return inspectionDtl;
	}

	public void setInspectionDtl(List<InspectionDtl> inspectionDtl) {
		this.inspectionDtl = inspectionDtl;
	}

	public List<JSONObject> getCheckup() {
		if (checkup == null)
			checkup = new ArrayList<JSONObject>();
		return checkup;
	}

	public void setCheckup(List<JSONObject> checkup) {
		this.checkup = checkup;
	}

	public BigDecimal getTypeDtlPkId() {
		return typeDtlPkId;
	}

	public void setTypeDtlPkId(BigDecimal typeDtlPkId) {
		this.typeDtlPkId = typeDtlPkId;
	}

	public String getTypeDtl() {
		return typeDtl;
	}

	public void setTypeDtl(String typeDtl) {
		this.typeDtl = typeDtl;
	}

	public List<JSONObject> getQ() {
		if (q == null)
			q = new ArrayList<JSONObject>();
		return q;
	}

	public void setQ(List<JSONObject> q) {
		this.q = q;
	}


	public BigDecimal getInspectionPkId() {
		return inspectionPkId;
	}

	public void setInspectionPkId(BigDecimal inspectionPkId) {
		this.inspectionPkId = inspectionPkId;
	}

	public String getSubName() {
		return subName;
	}

	public void setSubName(String subName) {
		this.subName = subName;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}


	public List<JSONObject> getPlan() {
		if(plan == null)
			plan = new ArrayList<JSONObject>();
		return plan;
	}

	public void setPlan(List<JSONObject> plan) {
		this.plan = plan;
	}
}
