package pharmacy.businessentity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pharmacy.entity.*;

public class CustomerMedicineDeispense implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String  employeeName;
	private String  customerCardNumber;
	private String  medicineName;
	private String  medicineDescription;
	private String  dose;
	private String  orderDose;
	private String	medicineId;
	private String	type;
	private String subName;
	private String iNameMedicine;
	private String diagnoseId;
	private String diagnoseName;
	private String typeConstantName;
	private String roomNo;
	private String bedNumber;

	private int countMedicineOrder;
	private int countMedicineCustomerOrder;
	private int customerCountDiespense;
	private int medicineTime;
	private int medicineRepeat;
	private int customerDiespense;
	private int medicineBalance;

	private Long countDiespense;

	private BigDecimal  customerPkId;
	private BigDecimal  customerMedicinePkId;
	private BigDecimal inspectionPkId;
	private BigDecimal subPkId;
	private BigDecimal employeePkId;
	private BigDecimal inpatientHdrPkId;
	private BigDecimal invPkId;
	private BigDecimal medicinePkId;
	private BigDecimal nursingServiceListPkId;
	private BigDecimal orderQty;
	private BigDecimal medicineQty;
	private BigDecimal orderQtySub;
	private BigDecimal orderBalance;
	private BigDecimal itemPkId;
	private BigDecimal warehousePkId;
	
	private String averageNumber;

	private Date createDate;
	private Employee  employee;
	private SubOrganization  subOrganization;
	private Inspection currentInspection;
	private Customer  customer;
	private Employee  e;
	private CustomerMedicine  customerMedicine;
	private Medicine  medicine;
	private CustomerPastHistory  customerPastHistory;
	private View_ConstantMedicineType  constantMedicineType;
	private Inspection  inspection;
	private InvoiceDtl invoiceDtl;

	private byte medicineM;
	private byte medicineD;
	private byte medicineE;
	private byte confirm;

	private List<CustomerMedicineDeispense> listCreateDate;
	private List<CustomerMedicineDeispense> listCustomerDipenses;
	private List<CustomerMedicineDeispense>  listDiagnose;

	private Date invDate;
	private Date beginDate;

	public CustomerMedicineDeispense(){
		super();
	}

	//umnuh jor tailbar cursor

	public CustomerMedicineDeispense(Date service,String description,String firstName){
		this.createDate =service;
		this.medicineDescription =description;
		this.employeeName =  firstName;
	}

	
	//LogicPharmacy -> getMedicineSubOrganizationReport  ЭМИЙН  ЗАРЦУУЛСАН  ТОО  ОЛОХ
	public CustomerMedicineDeispense(BigDecimal handOn){
		this.itemPkId =handOn;
		this.orderBalance  = handOn;
	}

	//report 
	public CustomerMedicineDeispense(BigDecimal subPkId,String subName,Long counthdr){
		this.subName =subName;
		this.medicineBalance  = (int)(long)counthdr;
		this.subPkId =subPkId;
	}

	//Тасгийн  эм олгох 
	public CustomerMedicineDeispense(BigDecimal pkId,String name,Long sum, Long diespense){
		this.subPkId=pkId;
		this.subName=name;
		this.countMedicineOrder= (int)(long) sum;
		this.customerCountDiespense = (int)(long) diespense;
	}


	//Тасгийн  эм олгох 
	public CustomerMedicineDeispense(BigDecimal pkId,String name,BigDecimal sum){
		this.subPkId=pkId;
		this.subName=name;
		this.orderBalance =sum;
	}
	
	public CustomerMedicineDeispense(BigDecimal pkId,String name,BigDecimal sum,Long count){
		this.subPkId=pkId;
		this.subName=name;
		this.orderBalance =sum;
		this.customerCountDiespense = (int)(long) count;
	}

	public CustomerMedicineDeispense(BigDecimal itemPkId, Date date,String cardNumber, BigDecimal qty,BigDecimal orderQty,String id){
		this.medicineQty=qty;
		this.orderQty =orderQty;
		this.medicineId =id;
		this.customerCardNumber =cardNumber;
		this.invDate =date;
		this.itemPkId  = itemPkId;
	}


	public CustomerMedicineDeispense(BigDecimal  pkId,Long diespense){
		this.customerDiespense =  (int)(long) diespense;
		this.customerMedicinePkId = pkId;
	}

	//Uvtund em  ognoogoor olgoh
	public CustomerMedicineDeispense(InvoiceDtl dtl) {
		this.invoiceDtl =dtl;
	}

	//Uvtund em  ognoogoor olgoh savlah
	public CustomerMedicineDeispense(String firstName,String id,String name,String type,String dose,InvoiceDtl dtl) {
		this.invoiceDtl =dtl;
		this.medicineId =id;
		this.medicineName =name;
		this.employeeName=firstName;
		this.dose =dose;
		this.type = type;
	}

	public CustomerMedicineDeispense(InvoiceDtl dtl,String id,String name,String type,BigDecimal sum) {
		this.invoiceDtl =dtl;
		this.medicineId =id;
		this.medicineName =name;
		this.type  =  type;
		this.orderQtySub = sum;
	}


	//pharmacy umnuh jor 
	public CustomerMedicineDeispense(String dose,String description,int m, int d,int e,byte confirm ,String id,String name,BigDecimal pkId) {
		this.medicineId =id;
		this.medicineName =name;
		this.dose=dose;
		this.medicineDescription =description;
		this.medicineM = (byte) m;
		this.medicineD = (byte) d;
		this.medicineE = (byte) e;
		this.confirm  =confirm;
		this.nursingServiceListPkId = pkId;
	}


	public CustomerMedicineDeispense(String type,String name,Long sum){
		if("Cito".equals(type) || "CITO".equals(type)  )
			this.medicineId="<small class='label label-warning'>Яаралтай</small>";
		this.medicineName=name;
		this.countMedicineOrder= (int)(long) sum;
	}
	public CustomerMedicineDeispense(Date date,BigDecimal empPkId,BigDecimal subPkId,String empName,String subName){
		this.createDate=date;
		this.employeePkId = empPkId;
		this.subPkId = subPkId;
		this.employeeName=empName;
		this.subName=subName;
	}

	public CustomerMedicineDeispense(Date createdDate){
		super();
		this.createDate=createdDate;	
		this.invDate =createdDate;
	}

	//эм олгох 
	public CustomerMedicineDeispense(CustomerMedicine medicine,String name,String  iName){
		this.iNameMedicine=iName;
		this.medicineName=name;
		this.customerMedicine =medicine;
	}

	public CustomerMedicineDeispense(String id,String nameEn){
		this.diagnoseId =id;
		this.diagnoseName =nameEn;

		this.medicineId=id;
		this.medicineName =nameEn;
	}
	// em olgoh emeer bulegleh bolon ognoogoor 
	public CustomerMedicineDeispense(BigDecimal pkId,String id,String medName){
		this.medicineId=id;
		this.medicineName =medName;
		this.medicinePkId  =pkId;
	}

	public CustomerMedicineDeispense(String employeeName,Date  date,Customer customer,String dose,long countMedicine,int diespense){
		this.customer=customer;
		this.orderDose = dose;
		this.countMedicineCustomerOrder= (int)countMedicine;
		this.createDate =date;
		this.employeeName =employeeName;
		this.customerCountDiespense =diespense;
	}

	public CustomerMedicineDeispense(String id,String medicineName,String iName,String dose,long countOrderMed,BigDecimal pkId,Long countDies){
		this.medicineId= id;
		this.iNameMedicine=iName;
		this.medicineName=medicineName;
		this.dose=dose;
		this.countMedicineOrder= (int)countOrderMed;
		this.customerMedicinePkId =pkId;
		this.countDiespense = countDies;
		System.out.println(countDiespense);
	}

	public CustomerMedicineDeispense(Date createdDate,byte medicineM ,byte medicineD ,byte medicineE){
		super();
		this.createDate=createdDate;
		this.medicineM=medicineM;
		this.medicineD=medicineD;
		this.medicineE=medicineE;

	}

	public CustomerMedicineDeispense(BigDecimal pkId,String name,String dose,String medicineDescription, byte m, byte d,byte e){
		super();
		this.customerMedicinePkId =pkId;
		this.medicineName =name;
		this.dose =dose;
		this.medicineDescription =medicineDescription;
		this.medicineM=m;
		this.medicineD=d;
		this.medicineE=e;
	}

	public  CustomerMedicineDeispense(Employee e,SubOrganization sb){
		super();
		this.employee=e;
		this.subOrganization=sb;
	}

	public CustomerMedicineDeispense(CustomerMedicine cusMed,Medicine med,View_ConstantMedicineType type){
		super();
		this.countMedicineOrder = this.countMedicineCustomerOrder;
		this.customerMedicine=cusMed;
		this.medicine=med;
		this.constantMedicineType  =type;
	}

	public CustomerMedicineDeispense(CustomerMedicine cusMed,Inspection i,Medicine med){
		super();
		this.customerMedicine=cusMed;
		this.inspection  = i;
		this.medicine=med;
	}

	public CustomerMedicineDeispense(Customer cm){
		super();
		this.customer =cm;
	}

	// em olgoh  uilchluulegchiin  jagsaalt
	public CustomerMedicineDeispense(Customer cm,BigDecimal inHdrPkId,BigDecimal inpatientHdrPkId){
		super();
		this.customer =cm;
		this.invPkId  =inHdrPkId;
		this.inpatientHdrPkId =inpatientHdrPkId;
	}
	// emiin san 
	public CustomerMedicineDeispense(Customer cm,BigDecimal inHdrPkId,BigDecimal inpatientHdrPkId,
			String roomNo,String subName,Date beginDate,String bedNumber,BigDecimal orderQty,BigDecimal qty){
		super();
		this.customer =cm;
		this.invPkId  =inHdrPkId;
		this.inpatientHdrPkId =inpatientHdrPkId;
		this.roomNo =roomNo;
		this.subName=subName;
		this.beginDate = beginDate;
		this.medicineQty =qty;
		this.orderQty =orderQty;
		this.bedNumber =bedNumber;
	}
	// ognoo  group  Em olgoh 
	public CustomerMedicineDeispense(String empfirstName, String subName,BigDecimal insPkId){
		super();
		this.employeeName =empfirstName;
		this.subName=subName;
		this.inspectionPkId=insPkId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public SubOrganization getSubOrganization() {
		return subOrganization;
	}

	public void setSubOrganization(SubOrganization subOrganization) {
		this.subOrganization = subOrganization;
	}

	public Inspection getCurrentInspection() {
		return currentInspection;
	}

	public void setCurrentInspection(Inspection currentInspection) {
		this.currentInspection = currentInspection;
	}


	public Customer getCustomer() {
		if(customer==null)
			customer =  new Customer();
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Employee getE() {
		return e;
	}
	public void setE(Employee e) {
		this.e = e;
	}

	public CustomerMedicine getCustomerMedicine() {
		return customerMedicine;
	}

	public void setCustomerMedicine(CustomerMedicine customerMedicine) {
		this.customerMedicine = customerMedicine;
	}

	public Medicine getMedicine() {
		return medicine;
	}

	public void setMedicine(Medicine medicine) {
		this.medicine = medicine;
	}

	public String getSubName() {
		return subName;
	}

	public void setSubName(String subName) {
		this.subName = subName;
	}

	public BigDecimal getInspectionPkId() {
		return inspectionPkId;
	}

	public void setInspectionPkId(BigDecimal inspectionPkId) {
		this.inspectionPkId = inspectionPkId;
	}

	public List<CustomerMedicineDeispense> getListCustomerDipenses() {
		if(listCustomerDipenses==null)
			listCustomerDipenses  =  new ArrayList<>();
		return listCustomerDipenses;
	}

	public void setListCustomerDipenses(List<CustomerMedicineDeispense> listCustomerDipenses) {
		this.listCustomerDipenses = listCustomerDipenses;
	}

	public CustomerPastHistory getCustomerPastHistory() {
		return customerPastHistory;
	}

	public void setCustomerPastHistory(CustomerPastHistory customerPastHistory) {
		this.customerPastHistory = customerPastHistory;
	}

	public View_ConstantMedicineType getConstantMedicineType() {
		return constantMedicineType;
	}

	public void setConstantMedicineType(View_ConstantMedicineType constantMedicineType) {
		this.constantMedicineType = constantMedicineType;
	}

	public Inspection getInspection() {
		return inspection;
	}

	public void setInspection(Inspection inspection) {
		this.inspection = inspection;
	}

	public List<CustomerMedicineDeispense> getListCreateDate() {
		if(listCreateDate==null)
			listCreateDate =  new ArrayList<>();
		return listCreateDate;
	}

	public void setListCreateDate(List<CustomerMedicineDeispense> listCreateDate) {
		this.listCreateDate = listCreateDate;
	}

	public int getMedicineM() {
		return medicineM;
	}

	public byte getMedicineD() {
		return medicineD;
	}

	public void setMedicineD(byte medicineD) {
		this.medicineD = medicineD;
	}

	public byte getMedicineE() {
		return medicineE;
	}

	public void setMedicineE(byte medicineE) {
		this.medicineE = medicineE;
	}

	public void setMedicineM(byte medicineM) {
		this.medicineM = medicineM;
	}

	public String getMedicineName() {
		return medicineName;
	}

	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}

	public String getMedicineDescription() {
		return medicineDescription;
	}

	public void setMedicineDescription(String medicineDescription) {
		this.medicineDescription = medicineDescription;
	}

	public String getDose() {
		return dose;
	}

	public void setDose(String dose) {
		this.dose = dose;
	}

	public BigDecimal getCustomerMedicinePkId() {
		return customerMedicinePkId;
	}

	public void setCustomerMedicinePkId(BigDecimal customerMedicinePkId) {
		this.customerMedicinePkId = customerMedicinePkId;
	}

	public String getMedicineId() {
		return medicineId;
	}

	public void setMedicineId(String medicineId) {
		this.medicineId = medicineId;
	}

	public int getCountMedicineOrder() {
		return countMedicineOrder;
	}

	public void setCountMedicineOrder(int countMedicineOrder) {
		this.countMedicineOrder = countMedicineOrder;
	}

	public String getOrderDose() {
		return orderDose;
	}

	public void setOrderDose(String orderDose) {
		this.orderDose = orderDose;
	}

	public int getCountMedicineCustomerOrder() {
		return countMedicineCustomerOrder;
	}

	public void setCountMedicineCustomerOrder(int countMedicineCustomerOrder) {
		this.countMedicineCustomerOrder = countMedicineCustomerOrder;
	}

	public String getiNameMedicine() {
		return iNameMedicine;
	}

	public void setiNameMedicine(String iNameMedicine) {
		this.iNameMedicine = iNameMedicine;
	}

	public String getDiagnoseId() {
		return diagnoseId;
	}

	public void setDiagnoseId(String diagnoseId) {
		this.diagnoseId = diagnoseId;
	}

	public List<CustomerMedicineDeispense> getListDiagnose() {
		if(listDiagnose==null)
			listDiagnose = new ArrayList<>();
		return listDiagnose;
	}

	public void setListDiagnose(List<CustomerMedicineDeispense> listDiagnose) {
		this.listDiagnose = listDiagnose;
	}

	public String getDiagnoseName() {
		return diagnoseName;
	}

	public void setDiagnoseName(String diagnoseName) {
		this.diagnoseName = diagnoseName;
	}

	public Long getCountDiespense() {
		return countDiespense;
	}

	public void setCountDiespense(Long countDiespense) {
		this.countDiespense = countDiespense;
	}

	public int getCustomerCountDiespense() {
		return customerCountDiespense;
	}

	public void setCustomerCountDiespense(int customerCountDiespense) {
		this.customerCountDiespense = customerCountDiespense;
	}

	public BigDecimal getSubPkId() {
		return subPkId;
	}

	public void setSubPkId(BigDecimal subPkId) {
		this.subPkId = subPkId;
	}

	public int getMedicineTime() {
		return medicineTime;
	}

	public void setMedicineTime(int medicineTime) {
		this.medicineTime = medicineTime;
	}

	public int getMedicineRepeat() {
		return medicineRepeat;
	}

	public void setMedicineRepeat(int medicineRepeat) {
		this.medicineRepeat = medicineRepeat;
	}

	public String getTypeConstantName() {
		return typeConstantName;
	}

	public void setTypeConstantName(String typeConstantName) {
		this.typeConstantName = typeConstantName;
	}

	public int getCustomerDiespense() {
		return customerDiespense;
	}

	public void setCustomerDiespense(int customerDiespense) {
		this.customerDiespense = customerDiespense;
	}

	public BigDecimal getEmployeePkId() {
		return employeePkId;
	}

	public void setEmployeePkId(BigDecimal employeePkId) {
		this.employeePkId = employeePkId;
	}

	public int getMedicineBalance() {
		return medicineBalance;
	}

	public void setMedicineBalance(int medicineBalance) {
		this.medicineBalance = medicineBalance;
	}


	public BigDecimal getInpatientHdrPkId() {
		return inpatientHdrPkId;
	}
	public void setInpatientHdrPkId(BigDecimal inpatientHdrPkId) {
		this.inpatientHdrPkId = inpatientHdrPkId;
	}
	public BigDecimal getInvPkId() {
		return invPkId;
	}
	public void setInvPkId(BigDecimal invPkId) {
		this.invPkId = invPkId;
	}
	public BigDecimal getMedicinePkId() {
		return medicinePkId;
	}
	public void setMedicinePkId(BigDecimal medicinePkId) {
		this.medicinePkId = medicinePkId;
	}
	public Date getInvDate() {
		return invDate;
	}
	public void setInvDate(Date invDate) {
		this.invDate = invDate;
	}

	public String dateTime(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if(date!=null)
			return  format.format(date);
		return "";
	}
	public InvoiceDtl getInvoiceDtl() {
		if(invoiceDtl==null)
			invoiceDtl=  new InvoiceDtl();
		return invoiceDtl;
	}
	public void setInvoiceDtl(InvoiceDtl invoiceDtl) {
		this.invoiceDtl = invoiceDtl;
	}
	public String getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public byte getConfirm() {
		return confirm;
	}
	public void setConfirm(byte confirm) {
		this.confirm = confirm;
	}
	public BigDecimal getNursingServiceListPkId() {
		return nursingServiceListPkId;
	}
	public void setNursingServiceListPkId(BigDecimal nursingServiceListPkId) {
		this.nursingServiceListPkId = nursingServiceListPkId;
	}

	public BigDecimal getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(BigDecimal orderQty) {
		this.orderQty = orderQty;
	}

	public BigDecimal getMedicineQty() {
		return medicineQty;
	}

	public void setMedicineQty(BigDecimal medicineQty) {
		this.medicineQty = medicineQty;
	}

	public BigDecimal getOrderQtySub() {
		return orderQtySub;
	}

	public void setOrderQtySub(BigDecimal orderQtySub) {
		this.orderQtySub = orderQtySub;
	}

	public BigDecimal getCustomerPkId() {
		return customerPkId;
	}

	public void setCustomerPkId(BigDecimal customerPkId) {
		this.customerPkId = customerPkId;
	}

	public BigDecimal getItemPkId() {
		return itemPkId;
	}

	public void setItemPkId(BigDecimal itemPkId) {
		this.itemPkId = itemPkId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCustomerCardNumber() {
		return customerCardNumber;
	}

	public void setCustomerCardNumber(String customerCardNumber) {
		this.customerCardNumber = customerCardNumber;
	}

	public BigDecimal getOrderBalance() {
		return orderBalance;
	}

	public void setOrderBalance(BigDecimal orderBalance) {
		this.orderBalance = orderBalance;
	}

	public BigDecimal getWarehousePkId() {
		return warehousePkId;
	}

	public void setWarehousePkId(BigDecimal warehousePkId) {
		this.warehousePkId = warehousePkId;
	}

	public String getBedNumber() {
		return bedNumber;
	}

	public void setBedNumber(String bedNumber) {
		this.bedNumber = bedNumber;
	}

	public String getAverageNumber() {
		return averageNumber;
	}

	public void setAverageNumber(String averageNumber) {
		this.averageNumber = averageNumber;
	}




}
