package pharmacy.businessentity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import pharmacy.entity.ExaminationValueAnswer;
import pharmacy.entity.ExaminationValueHdr;
import pharmacy.entity.ExaminationValueQuestion;

public class ExaminationResults {

	private BigDecimal evqPkId;
	private BigDecimal examinationTemplatePkId;
	private String evqName;
	private List<String> values;
	private String range;
	private String measurement;
	private String value;
	private String answerType;
	private ExaminationValueQuestion evq;
	private String resultString;

	public ExaminationResults() {
		super();
	}

	public ExaminationResults(ExaminationValueQuestion question) {
		super();
		this.evqPkId = question.getPkId();
		this.examinationTemplatePkId = question.getExaminationTemplatePkId();
		this.evqName = question.getName();
		this.values = new ArrayList<String>();
		this.range = question.getReference();


	}

	public BigDecimal getEvqPkId() {
		return evqPkId;
	}

	public void setEvqPkId(BigDecimal evqPkId) {
		this.evqPkId = evqPkId;
	}

	public BigDecimal getExaminationTemplatePkId() {
		return examinationTemplatePkId;
	}

	public void setExaminationTemplatePkId(BigDecimal examinationTemplatePkId) {
		this.examinationTemplatePkId = examinationTemplatePkId;
	}

	public String getEvqName() {
		return evqName;
	}

	public void setEvqName(String evqName) {
		this.evqName = evqName;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public String getMeasurement() {
		return measurement;
	}

	public void setMeasurement(String measurement) {
		this.measurement = measurement;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAnswerType() {
		return answerType;
	}

	public void setAnswerType(String answerType) {
		this.answerType = answerType;
	}

	public ExaminationValueQuestion getEvq() {
		return evq;
	}

	public void setEvq(ExaminationValueQuestion evq) {
		this.evq = evq;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public String getResultString() {
		return resultString;
	}

	public void setResultString(String resultString) {
		this.resultString = resultString;
	}

}