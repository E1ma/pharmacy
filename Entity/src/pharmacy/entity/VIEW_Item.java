package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="VIEW_Item")
public class VIEW_Item  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="PkId",nullable=false,length=18)
	private BigDecimal pkId;
	
	@Column(name="Id")
	private  String id;
	
	@Column(name="Name")
	private  String name;
	
	@Column(name="IName")
	private  String iName;
	
	@Column(name="MedType")
	private  String medType;
	
	@Column(name="Dose")
	private  String dose;
	
	public VIEW_Item() {
		super();
	}


	public BigDecimal getPkId() {
		return pkId;
	}


	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getiName() {
		return iName;
	}


	public void setiName(String iName) {
		this.iName = iName;
	}


	public String getMedType() {
		return medType;
	}


	public void setMedType(String medType) {
		this.medType = medType;
	}


	public String getDose() {
		return dose;
	}


	public void setDose(String dose) {
		this.dose = dose;
	}
	
	
	
}
