package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
@Entity
@Table(name="Injection")
public class Injection implements  Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="PkId",length=18,nullable=false)
	private BigDecimal pkId;
	
	@Column(name="Id")
	private String id;
	
	@Column(name="Name")
	private String name;
	
	@Column(name="BioActive")
	private int bioActive;
	
	@Column(name="IName")
	private String iName;
	
	@Column(name="ATCPkId",length=18,nullable=false)
	private BigDecimal atcPkId;
	
	@Column(name="TypeInsjection")
	private String typeInsjection;
	
	
	@Column(name="Measurement")
	private String measurement;
	
	
	@Column(name="WarningMessage")
	private String warningMessage;
	
	
	@Column(name="Description")
	private String description;
	
	
	@Column(name="DrugDose")
	private String drugDose;
	
	@Column(name="Dose")
	private String dose;
	
	/* 0 бол  доош 1 дээш 2  бол  сонгох*/
	@Column(name="AgeAtive")
	private String ageAtive;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	@Column(name="CreatedBy")
	private BigDecimal CreatedBy;
	
	@Column(name="UpdatedBy")
	private BigDecimal UpdatedBy;
	
	/* 0  Бол тасраагүй 1 тасарсан*/
	@Column(name="Active")
	private byte active;
	
	/*0 бол үгүй 1  бол  бүх насныхан*/
	
	@Column(name="AllAge")
	private byte allAge;
	
	@Column(name="AgeDivision")
	private  int ageDivision;
	/*эмийн  нөлөөл  1 бол  сэтгэц нөлөөт мансууруулах  эм */
	
	@Column(name="InjectionImpact")
	private int injectionImpact;
	
	@Transient
	private String status;
	
	@Transient
	private boolean  activeValue;
	
	@Transient
	private boolean  allAgeValue;
	
	/*  Тарианы үнийн  хүснэгтийн  баганууд*/
	@Transient
	private BigDecimal price;
	
	@Transient
	private BigDecimal insurancePrice;
	
	@Transient
	private BigDecimal inpatientPrice;
	
	@Transient
	private byte isHasInsurance;
	
	@Transient
	private Date beginDate;
	
	@Transient 
	private boolean insuranceValue;
	
	public  Injection(){
		super();
	}
	
	public Injection(BigDecimal pkId, String id, String name, String iName) {
		super();
		this.pkId = pkId;
		this.id = id;
		this.name = name;
		this.iName = iName;
	}
	
	public Injection(BigDecimal pkId,String name ,String iName,String id ,String dose,BigDecimal price){
		this.pkId=pkId;
		this.name=name;
		this.iName =iName;
		this.dose =dose;
		this.price =price;
		this.id=id;
	}
	
	public Injection(BigDecimal pkId,String name,String iName,String id,BigDecimal atcPkId,String typeInsjection
				,String measurement,String warningMessage,String description,String drugDose 
				, String  dose,String ageAtive,Date createdDate,Date updatedDate,byte active
				,byte allAge,int  ageDivision,int injectionImpact,int bioActive,
				BigDecimal price ,BigDecimal insurancePrice,byte ishas,Date beginDate,BigDecimal inpatientPrice) {
		this.pkId=pkId;
		this.id=id;
		this.atcPkId=atcPkId;
		this.name=name;
		this.iName =iName;
		this.typeInsjection =typeInsjection;
		this.measurement=  measurement;
		this.warningMessage =warningMessage;
		this.description =description;
		this.drugDose =drugDose;
		this.dose =dose;
		this.ageAtive =ageAtive;
		this.createdDate =createdDate;
		this.updatedDate  =updatedDate;
		this.active  =active;
		this.allAge =allAge;
		this.ageDivision =ageDivision;
		this.injectionImpact =  injectionImpact;
		this.price=price;
		this.isHasInsurance =ishas;
		this.insurancePrice =insurancePrice;
		this.beginDate  =  beginDate;
		this.bioActive =bioActive;
		this.inpatientPrice = inpatientPrice;
	}

	public BigDecimal getPkId() {
		return pkId;
	}


	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getiName() {
		if(iName == null) iName = "";
		return iName;
	}


	public void setiName(String iName) {
		this.iName = iName;
	}


	public BigDecimal getAtcPkId() {
		return atcPkId;
	}


	public void setAtcPkId(BigDecimal atcPkId) {
		this.atcPkId = atcPkId;
	}


	public String getTypeInsjection() {
		return typeInsjection;
	}


	public void setTypeInsjection(String typeInsjection) {
		this.typeInsjection = typeInsjection;
	}


	public String getMeasurement() {
		return measurement;
	}


	public void setMeasurement(String measurement) {
		this.measurement = measurement;
	}


	public String getWarningMessage() {
		return warningMessage;
	}


	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getDrugDose() {
		return drugDose;
	}


	public void setDrugDose(String drugDose) {
		this.drugDose = drugDose;
	}


	public String getDose() {
		return dose;
	}


	public void setDose(String dose) {
		this.dose = dose;
	}


	public String getAgeAtive() {
		return ageAtive;
	}


	public void setAgeAtive(String ageAtive) {
		this.ageAtive = ageAtive;
	}


	public Date getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	public Date getUpdatedDate() {
		return updatedDate;
	}


	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


	public BigDecimal getCreatedBy() {
		return CreatedBy;
	}


	public void setCreatedBy(BigDecimal createdBy) {
		CreatedBy = createdBy;
	}


	public BigDecimal getUpdatedBy() {
		return UpdatedBy;
	}


	public void setUpdatedBy(BigDecimal updatedBy) {
		UpdatedBy = updatedBy;
	}


	public byte getActive() {
		return active;
	}


	public void setActive(byte active) {
		this.active = active;
	}


	public byte getAllAge() {
		
		return allAge;
	}


	public void setAllAge(byte allAge) {
		this.allAge = allAge;
	}


	public int getAgeDivision() {
		return ageDivision;
	}


	public void setAgeDivision(int ageDivision) {
		this.ageDivision = ageDivision;
	}


	public int getInjectionImpact() {
		return injectionImpact;
	}


	public void setInjectionImpact(int injectionImpact) {
		this.injectionImpact = injectionImpact;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public boolean isActiveValue() {
		return activeValue;
	}


	public void setActiveValue(boolean activeValue) {
		this.activeValue = activeValue;
	}


	public boolean isAllAgeValue() {
		return allAge==1 ? true:false;
	}


	public void setAllAgeValue(boolean allAgeValue) {
		this.allAge = (byte) (allAgeValue ? 1:0);
		if(allAge==1){
			setAgeDivision(0);
			setAgeAtive("2");
		}
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getInsurancePrice() {
		return insurancePrice;
	}
	public void setInsurancePrice(BigDecimal insurancePrice) {
		this.insurancePrice = insurancePrice;
	}
	public byte getIsHasInsurance() {
		return isHasInsurance;
	}
	public void setIsHasInsurance(byte isHasInsurance) {
		this.isHasInsurance = isHasInsurance;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public boolean isInsuranceValue() {
		return this.isHasInsurance==1 ? true:false;
	}
	public void setInsuranceValue(boolean insuranceValue) {
		this.isHasInsurance = (byte) (insuranceValue ? 1:0);
	}
	public int getBioActive() {
		return bioActive;
	}
	public void setBioActive(int bioActive) {
		this.bioActive = bioActive;
	}

	public BigDecimal getInpatientPrice() {
		return inpatientPrice;
	}

	public void setInpatientPrice(BigDecimal inpatientPrice) {
		this.inpatientPrice = inpatientPrice;
	}
	
	
	
	
}
