package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "Item")
public class Item implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name = "OrganizationPkId")
	private BigDecimal organizationPkId;

	@Column(name = "Name")
	private String name;
	
	@Column(name = "Id")
	private String id;
	
	@Column(name = "IName")
	private String iName;
	
	@Column(name = "Type")
	private String type;
	
	@Column(name = "ItemTypePkId", length = 18)
	private BigDecimal itemTypePkId;
	
	@Column(name = "ATCPkId", length = 18)
	private BigDecimal atcPkId;

	@Column(name = "BioActive")
	private byte bioActive;
	
	@Column(name = "MaxAge")
	private int maxAge;
	
	@Column(name = "MinAge")
	private int minAge;
	
	@Column(name = "DrugDose")
	private String drugDose;
	
	@Column(name = "DayDose")
	private String dayDose;
	
	@Column(name = "Dose")
	private String dose;
	
	@Column(name = "CalcDrugDose")
	private int calcDrugDose;
	
	@Column(name = "MedicineImpact")
	private int medicineImpact;
	
	@Column(name = "CalcDose")
	private int calcDose;
	
	@Column(name = "CalcType")
	private String calcType;
	
	@Column(name = "WarningMessage")
	private String warningMessage;
	
	@Column(name = "Description")
	private String description;
	
	@Column(name = "MixName")
	private String mixName;
	
	@Column(name = "SideEffect")
	private String sideEffect;
	
	@Column(name = "WarningPkId")
	private BigDecimal warningPkId;
	
	@Column(name = "IsDivide")
	private byte isDivide;
	
	@Column(name = "TraditionalBool")
	private byte traditionalBool;
	
	@Column(name = "UsageType")
	private String usageType;
	
	@Column(name = "RoomNumber")
	private String roomNumber;
	
	@Column(name = "IsDiscountOut")
	private byte isDiscountOut;
	
	@Column(name = "IsDiscountIn")
	private byte isDiscountIn;
	
	@Column(name = "HasDR")
	private byte hasDR;
	
	@Column(name = "DrugPurpose")
	private byte drugPurpose;
	
	@Column(name = "Active")
	private byte active;
	
	@Column(name = "ImageCount")
	private byte imageCount;
	
	@Column(name = "HasInsurance")
	private byte hasInsurance;
	
	@Column(name = "IsModel")
	private byte isModel;
	
	@Column(name = "ExaminationTemplatePkId")
	private BigDecimal examinationTemplatePkId;
	
	@Column(name = "windowType")
	private String windowType;

	@Column(name = "MedicineTypePkId")
	private BigDecimal medicineTypePkId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;

	@Column(name = "CreatedBy")
	private BigDecimal createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;

	@Column(name = "UpdatedBy")
	private BigDecimal updatedBy;
	
	@Column(name = "MeasurementPkId")
	private BigDecimal measurementPkId;

	@Transient
	private String status;
	
	@Transient 
	private BigDecimal itemCount;
	
	@Transient
	private BigDecimal cost;
	
	@Transient
	private BigDecimal amount;
	
	@Transient
	private String measurementName;
	
	
	public Item(){
		super();
	}
	
	public Item(BigDecimal pkId, String id, String name, BigDecimal entityPrice, Date priceUsageDate, BigDecimal measurementPkId, String measurementName) {
		super();
		this.pkId = pkId;
		this.id = id;
		this.name = name;
		this.measurementPkId = measurementPkId;
		this.measurementName = measurementName;
	}
	
	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getItemCount() {
		return itemCount == null ? BigDecimal.ZERO : itemCount;
	}

	public void setItemCount(BigDecimal itemCount) {
		this.itemCount = itemCount;
	}
	
	public BigDecimal getCost() {
		return cost == null ? BigDecimal.ZERO : cost;
	}
	
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}



	public BigDecimal getOrganizationPkId() {
		return organizationPkId;
	}



	public void setOrganizationPkId(BigDecimal organizationPkId) {
		this.organizationPkId = organizationPkId;
	}
	
	public BigDecimal getAmount() {
		if(amount == null)
			return BigDecimal.ZERO;
		else
			return amount;
		
	}
	
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public String getMeasurementName() {
		return measurementName;
	}
	public BigDecimal getMeasurementPkId() {
		return measurementPkId;
	}
	public void setMeasurementName(String measurementName) {
		this.measurementName = measurementName;
	}
	public void setMeasurementPkId(BigDecimal measurementPkId) {
		this.measurementPkId = measurementPkId;
	}

	public String getiName() {
		return iName;
	}

	public void setiName(String iName) {
		this.iName = iName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getItemTypePkId() {
		return itemTypePkId;
	}

	public void setItemTypePkId(BigDecimal itemTypePkId) {
		this.itemTypePkId = itemTypePkId;
	}

	public BigDecimal getAtcPkId() {
		return atcPkId;
	}

	public void setAtcPkId(BigDecimal atcPkId) {
		this.atcPkId = atcPkId;
	}

	public byte getBioActive() {
		return bioActive;
	}

	public void setBioActive(byte bioActive) {
		this.bioActive = bioActive;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public String getDrugDose() {
		return drugDose;
	}

	public void setDrugDose(String drugDose) {
		this.drugDose = drugDose;
	}

	public String getDayDose() {
		return dayDose;
	}

	public void setDayDose(String dayDose) {
		this.dayDose = dayDose;
	}

	public String getDose() {
		return dose;
	}

	public void setDose(String dose) {
		this.dose = dose;
	}

	public int getCalcDrugDose() {
		return calcDrugDose;
	}

	public void setCalcDrugDose(int calcDrugDose) {
		this.calcDrugDose = calcDrugDose;
	}

	public int getMedicineImpact() {
		return medicineImpact;
	}

	public void setMedicineImpact(int medicineImpact) {
		this.medicineImpact = medicineImpact;
	}

	public int getCalcDose() {
		return calcDose;
	}

	public void setCalcDose(int calcDose) {
		this.calcDose = calcDose;
	}

	public String getCalcType() {
		return calcType;
	}

	public void setCalcType(String calcType) {
		this.calcType = calcType;
	}

	public String getWarningMessage() {
		return warningMessage;
	}

	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMixName() {
		return mixName;
	}

	public void setMixName(String mixName) {
		this.mixName = mixName;
	}

	public String getSideEffect() {
		return sideEffect;
	}

	public void setSideEffect(String sideEffect) {
		this.sideEffect = sideEffect;
	}

	public BigDecimal getWarningPkId() {
		return warningPkId;
	}

	public void setWarningPkId(BigDecimal warningPkId) {
		this.warningPkId = warningPkId;
	}

	public byte getIsDivide() {
		return isDivide;
	}

	public void setIsDivide(byte isDivide) {
		this.isDivide = isDivide;
	}

	public byte getTraditionalBool() {
		return traditionalBool;
	}

	public void setTraditionalBool(byte traditionalBool) {
		this.traditionalBool = traditionalBool;
	}

	public String getUsageType() {
		return usageType;
	}

	public void setUsageType(String usageType) {
		this.usageType = usageType;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public byte getIsDiscountOut() {
		return isDiscountOut;
	}

	public void setIsDiscountOut(byte isDiscountOut) {
		this.isDiscountOut = isDiscountOut;
	}

	public byte getIsDiscountIn() {
		return isDiscountIn;
	}

	public void setIsDiscountIn(byte isDiscountIn) {
		this.isDiscountIn = isDiscountIn;
	}

	public byte getHasDR() {
		return hasDR;
	}

	public void setHasDR(byte hasDR) {
		this.hasDR = hasDR;
	}

	public byte getDrugPurpose() {
		return drugPurpose;
	}

	public void setDrugPurpose(byte drugPurpose) {
		this.drugPurpose = drugPurpose;
	}

	public byte getActive() {
		return active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public byte getImageCount() {
		return imageCount;
	}

	public void setImageCount(byte imageCount) {
		this.imageCount = imageCount;
	}

	public byte getHasInsurance() {
		return hasInsurance;
	}

	public void setHasInsurance(byte hasInsurance) {
		this.hasInsurance = hasInsurance;
	}

	public byte getIsModel() {
		return isModel;
	}

	public void setIsModel(byte isModel) {
		this.isModel = isModel;
	}

	public BigDecimal getExaminationTemplatePkId() {
		return examinationTemplatePkId;
	}

	public void setExaminationTemplatePkId(BigDecimal examinationTemplatePkId) {
		this.examinationTemplatePkId = examinationTemplatePkId;
	}

	public String getWindowType() {
		return windowType;
	}

	public void setWindowType(String windowType) {
		this.windowType = windowType;
	}

	public BigDecimal getMedicineTypePkId() {
		return medicineTypePkId;
	}

	public void setMedicineTypePkId(BigDecimal medicineTypePkId) {
		this.medicineTypePkId = medicineTypePkId;
	}
	
	
	
}
