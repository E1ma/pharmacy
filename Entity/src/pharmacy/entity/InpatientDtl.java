package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "InpatientDtl")
public class InpatientDtl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name="InpatientHdrPkId")
	private BigDecimal inpatientHdrPkId;
	
	public BigDecimal getInpatientHdrPkId() {
		return inpatientHdrPkId;
	}

	public void setInpatientHdrPkId(BigDecimal inpatientHdrPkId) {
		this.inpatientHdrPkId = inpatientHdrPkId;
	}

	/**
	 * Эмчилгээ - INPATIENTDTLSTATUS_TREATMENT
	 * Үзлэг - INPATIENTDTLSTATUS_INSPECTION
	 * Эхний үзлэг - INPATIENTDTLSTATUS_FIRSTINSPECTION
	 * Шинэжилгээ - 
	 * Оношилгоо - INPATIENTDTLSTATUS_DIAGNOSE
	 * Хэвтсэн - INPATIENTDTLSTATUS_IN
	 * Гарсан - INPATIENTDTLSTATUS_OUTOFHOSPITAL
	 * Сувилагчын тэмдэглэл - NURSING_NOTE
	 * */
	
	@Column(name = "Type")
	private String type;
	
	@Column(name = "TypePkId")
	private BigDecimal typePkId;
	
	@Column(name = "EmployeePkId")
	private BigDecimal employeePkId;
	
	@Column(name = "Date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	/**
	 * 1 
	 * */
	@Column(name = "Status")
	private byte status;
	
	@Column(name = "CreatedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name = "CreatedBy")
	private BigDecimal createdBy;
	
	@Column(name = "UpdatedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;
	
	@Column(name = "UpdatedBy")
	private BigDecimal updatedBy;
	
	
	@Transient
	private String lastName;

	@Transient
	private String firstName;
	
	@Transient
	private String regNumber;
	
	@Transient
	private int gender;
	
	@Transient
	private String roomNumber;
	
	@Transient
	private String employeeFirstName;
	
	@Transient
	private Date hdrBeginDate;
	
	@Transient
	private Date hdrEndDate;
	
	@Transient
	private String subOrgaName;
	
	@Transient
	private String labelType;
	
	public InpatientDtl(){
		super();
	}
	
	public InpatientDtl(InpatientDtl dtl,String firstName){
		this.date =dtl.getDate();
		this.employeeFirstName=firstName;
		this.type = type(dtl.getType());
	}
	
	
	public InpatientDtl(BigDecimal hdrpkId , BigDecimal pkid ,Date createdDate,String regNumber,String lastName,String firstName,int gender,String roomNo,String eFirstName,Date beginDate,Date endDate,String subName,BigDecimal insPkId){
		this.inpatientHdrPkId=hdrpkId;
		this.pkId=pkid;
		this.date =createdDate;
		this.regNumber =regNumber;
		this.lastName =lastName;
		this.firstName =firstName;
		this.gender =gender;
		this.employeeFirstName=eFirstName;
		this.roomNumber=roomNo;
		this.hdrBeginDate=beginDate;
		this.hdrEndDate =endDate;
		this.subOrgaName=subName;
		this.typePkId=insPkId;
	}
	
	public String type(String types){
		if("assessmentPain".equals(types)){
			type ="Өвдөлтийн хуудас бүртэсэн";
			labelType ="label label-nc-1"; 
		}
		else if("ioCheck".equals(types)){
			type ="I/O бүртэсэн";
			labelType ="label label-info"; 
		}
		else if("bloodSugar".equals(types)){
			type ="BST бүртэсэн";
			labelType ="label label-danger"; 
		}
		else if("order".equals(types)){
			type ="Захиалга хийсэн";
			labelType ="label label-success"; 
		}
		else if("Acting".equals(types)){
			type ="Acting бүртэсэн";
			labelType ="label label-primary"; 
		}
		else if("vitalSign".equals(types)){
			type ="VitalSign бүртэсэн";
			labelType ="labels label-nc-4 label-tag"; 
		}
		else if("nutrition".equals(types)){
			type ="Nutrition бүртэсэн";
			labelType ="labels label-nc-2"; 
		}
		return type;
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getTypePkId() {
		return typePkId;
	}

	public void setTypePkId(BigDecimal typePkId) {
		this.typePkId = typePkId;
	}

	public BigDecimal getEmployeePkId() {
		return employeePkId;
	}

	public void setEmployeePkId(BigDecimal employeePkId) {
		this.employeePkId = employeePkId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}
	
	public  String getDateConvert(Date d){
		SimpleDateFormat  format =  new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return  format.format(d);
	}
	
	public  String getHourConvert(Date d){
		SimpleDateFormat  format =  new SimpleDateFormat("hh:mm a");
		return  format.format(d);
	}
	
	public  String getDayConvert(Date d){
		SimpleDateFormat  format =  new SimpleDateFormat("yyyy-MM-dd");
		return  format.format(d);
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getEmployeeFirstName() {
		return employeeFirstName;
	}

	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}

	public Date getHdrBeginDate() {
		return hdrBeginDate;
	}

	public void setHdrBeginDate(Date hdrBeginDate) {
		this.hdrBeginDate = hdrBeginDate;
	}

	public Date getHdrEndDate() {
		return hdrEndDate;
	}

	public void setHdrEndDate(Date hdrEndDate) {
		this.hdrEndDate = hdrEndDate;
	}

	public String getSubOrgaName() {
		return subOrgaName;
	}

	public void setSubOrgaName(String subOrgaName) {
		this.subOrgaName = subOrgaName;
	}

	public String getLabelType() {
		return labelType;
	}

	public void setLabelType(String labelType) {
		this.labelType = labelType;
	}
}
