package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="WarningMedicineMaxDay")
public class WarningMedicineMaxDay implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="PkId",nullable=false)
	private BigDecimal pkId;
	
	@Column(name="MedicinePkId")
	private BigDecimal medicinePkId;
	
	@Column(name="MaxDay")
	private BigDecimal maxDay;
	
	@Column(name="Dose")
	private String dose;
	
	@Transient
	private boolean select;
	
	public WarningMedicineMaxDay(){
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getMedicinePkId() {
		return medicinePkId;
	}

	public void setMedicinePkId(BigDecimal medicinePkId) {
		this.medicinePkId = medicinePkId;
	}

	public BigDecimal getMaxDay() {
		return maxDay;
	}

	public void setMaxDay(BigDecimal maxDay) {
		this.maxDay = maxDay;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public boolean isSelect() {
		return select;
	}
	
	public void setSelect(boolean select) {
		this.select = select;
	}
	
	public String getDose() {
		return dose;
	}
	
	public void setDose(String dose) {
		this.dose = dose;
	}
	
	

}
