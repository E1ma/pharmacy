package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "Room")
public class Room implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	/**
	 * BuilddingInfo TYPE = 0
	 * */
	@Column(name = "BlockPkId")
	private BigDecimal blockPkId;
	
	/**
	 * BuilddingInfo TYPE = 1
	 * */
	@Column(name = "FlatPkId")
	private BigDecimal flatPkId;
	
	/**
	 * BuilddingInfo TYPE = 2
	 * */
	@Column(name = "AislePkId")
	private BigDecimal aislePkId;
	
	@Column(name = "RoomNo")
	private String roomNo;
	
	/**
	 * 0 - HEVTEN EMCHLUULEH OROO BISH
	 * 1 - HEVTEN EMCHLUULEH OROO
	 * */
	@Column(name = "HasInpatient")
	private byte hasInpatient;
	
	/**
	 * 0 - VIP BISH
	 * 1 - VIP MON
	 * */
	@Column(name = "HasVIP")
	private byte hasVIP;
	
	@Column(name = "SubOrganizationTypePkId")
	private BigDecimal subOrganizationTypePkId;
	
	@Column(name="SubOrganizationPkId")
	private BigDecimal subOrganizationPkId;
	
	/**
	 * 0 - Eregtei
	 * 1 - Emegtei
	 * 2 - Holimog
	 * */
	@Column(name = "RoomType")
	private byte roomType;
	
	@Column(name = "BedCount")
	private int bedCount;
	
	@Column(name="CreatedBy")
	private BigDecimal createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private BigDecimal updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Transient
	private String status;
	
	
	@Transient
	private byte bedInfo;
	
	@Transient
	private long count;
	
	@Transient
	private BigDecimal bedRoomPkId;
	
	@Transient
	private BigDecimal bedInfoPkId;
	
	@Transient
	private List<Room> roomBeds;
	
	
	@Transient
	private long count1;
	
	@Transient
	private long count2;
	
	@Transient
	private long count3;
	
	@Transient
	private long count4;
	
	@Transient
	private long count5;
	
	@Transient
	private long count6;
	
	@Transient
	private int emptyBedCount;
	
	public Room(){
		super();
	}
	
	public Room(BigDecimal pkId, String roomNo){
		super();
		this.pkId = pkId;
		this.roomNo = roomNo;
	}
	public Room(BigDecimal pkId ,BigDecimal subPkId,int bedCount,String roomNo){
		this.pkId =pkId;
		this.bedCount =bedCount;
		this.roomNo =roomNo;
		this.subOrganizationTypePkId =subPkId;
	}
	public Room(BigDecimal pkId,String roomNo,Long count){
		this.pkId=pkId;
		this.count=(int)(long)count;
		this.roomNo =roomNo;
	}
	public Room(BigDecimal pkId, String roomNo, int bedCount){
		super();
		this.pkId = pkId;
		this.roomNo = roomNo;
		this.bedCount = bedCount;
	}
	public Room(BigDecimal infoPkId){
		super();
		this.bedInfoPkId = infoPkId;
	}
	
	public Room(Long count,int bedCount,BigDecimal roomPkId){
		this.emptyBedCount=(int)(long) count;
		this.bedCount=bedCount;
		this.pkId=roomPkId;
	}
	
	public Room(BigDecimal pkId, BigDecimal blockPkId, BigDecimal flatPkId, BigDecimal aislePkId, String roomNo,
			byte hasInpatient, byte hasVIP, BigDecimal subOrganizationTypePkId, byte roomType, int bedCount,
			BigDecimal createdBy, Date createdDate, BigDecimal updatedBy, Date updatedDate){
		super();
		this.pkId = pkId;
		this.blockPkId = blockPkId;
		this.flatPkId = flatPkId;
		this.aislePkId = aislePkId;
		this.roomNo = roomNo;
		this.hasInpatient = hasInpatient;
		this.hasVIP = hasVIP;
		this.subOrganizationTypePkId = subOrganizationTypePkId;
		this.roomType = roomType;
		this.bedCount = bedCount;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	
	public Room(BigDecimal pkId, BigDecimal blockPkId, BigDecimal flatPkId, BigDecimal aislePkId, String roomNo,
			byte hasInpatient, byte hasVIP, BigDecimal subOrganizationTypePkId, byte roomType, int bedCount,
			BigDecimal createdBy, Date createdDate, BigDecimal updatedBy, Date updatedDate,BigDecimal subOrganizationPkId){
		super();
		this.pkId = pkId;
		this.blockPkId = blockPkId;
		this.flatPkId = flatPkId;
		this.aislePkId = aislePkId;
		this.roomNo = roomNo;
		this.hasInpatient = hasInpatient;
		this.hasVIP = hasVIP;
		this.subOrganizationPkId =subOrganizationPkId;
		this.subOrganizationTypePkId = subOrganizationTypePkId;
		this.roomType = roomType;
		this.bedCount = bedCount;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	
	public Room(BigDecimal pkId, String roomNo, long count, int bedCount){
		super();
		this.pkId = pkId;
		this.roomNo = roomNo;
		this.count = count;
		this.bedCount = bedCount;
	}
	
	public Room(Long count1){
		super();
		if(count1==null)
			count1 = (long) 0;
		this.count =(int)(long) count1;
		this.emptyBedCount = (int)(long) count1;
	}
	
	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getBlockPkId() {
		return blockPkId;
	}

	public void setBlockPkId(BigDecimal blockPkId) {
		this.blockPkId = blockPkId;
	}

	public BigDecimal getFlatPkId() {
		return flatPkId;
	}

	public void setFlatPkId(BigDecimal flatPkId) {
		this.flatPkId = flatPkId;
	}

	public BigDecimal getAislePkId() {
		return aislePkId;
	}

	public void setAislePkId(BigDecimal aislePkId) {
		this.aislePkId = aislePkId;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public byte getHasInpatient() {
		return hasInpatient;
	}

	public void setHasInpatient(byte hasInpatient) {
		this.hasInpatient = hasInpatient;
	}

	public byte getHasVIP() {
		return hasVIP;
	}

	public void setHasVIP(byte hasVIP) {
		this.hasVIP = hasVIP;
	}

	public BigDecimal getSubOrganizationTypePkId() {
		return subOrganizationTypePkId;
	}

	public void setSubOrganizationTypePkId(BigDecimal subOrganizationTypePkId) {
		this.subOrganizationTypePkId = subOrganizationTypePkId;
	}

	public byte getRoomType() {
		return roomType;
	}

	public void setRoomType(byte roomType) {
		this.roomType = roomType;
	}

	public int getBedCount() {
		return bedCount;
	}

	public void setBedCount(int bedCount) {
		this.bedCount = bedCount;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public BigDecimal getBedRoomPkId() {
		return bedRoomPkId;
	}

	public void setBedRoomPkId(BigDecimal bedRoomPkId) {
		this.bedRoomPkId = bedRoomPkId;
	}

	public BigDecimal getBedInfoPkId() {
		return bedInfoPkId;
	}

	public void setBedInfoPkId(BigDecimal bedInfoPkId) {
		this.bedInfoPkId = bedInfoPkId;
	}

	public List<Room> getRoomBeds() {
		if(roomBeds==null)
			roomBeds =new ArrayList<>();
		return roomBeds;
	}

	public void setRoomBeds(List<Room> roomBeds) {
		this.roomBeds = roomBeds;
	}
	
	public long getCount1() {
		return count1;
	}

	public void setCount1(long count1) {
		this.count1 = count1;
	}

	public long getCount2() {
		return count2;
	}

	public void setCount2(long count2) {
		this.count2 = count2;
	}

	public long getCount3() {
		return count3;
	}

	public void setCount3(long count3) {
		this.count3 = count3;
	}

	public int getEmptyBedCount() {
		return emptyBedCount;
	}

	public void setEmptyBedCount(int emptyBedCount) {
		this.emptyBedCount = emptyBedCount;
	}
	
	public long getCount4() {
		return count4;
	}
	
	public void setCount4(long count4) {
		this.count4 = count4;
	}
	
	public long getCount5() {
		return count5;
	}
	
	public void setCount5(long count5) {
		this.count5 = count5;
	}
	
	public long getCount6() {
		return count6;
	}
	
	public void setCount6(long count6) {
		this.count6 = count6;
	}

	public byte getBedInfo() {
		return bedInfo;
	}

	public void setBedInfo(byte bedInfo) {
		this.bedInfo = bedInfo;
	}


	public BigDecimal getSubOrganizationPkId() {
		return subOrganizationPkId;
	}

	public void setSubOrganizationPkId(BigDecimal subOrganizationPkId) {
		this.subOrganizationPkId = subOrganizationPkId;
	}


	

}

