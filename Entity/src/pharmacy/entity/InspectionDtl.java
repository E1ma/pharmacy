package pharmacy.entity;

import pharmacy.businessentity.Tool;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "InspectionDtl")
public class InspectionDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;

	@Column(name = "InspectionPkId")
	private BigDecimal inspectionPkId;

	/**
	 * XRAY EXAMINATION TREATMENT SURGERY INPATIENT
	 */
	@Column(name = "Type")
	private String type;

	@Column(name = "TypePkId")
	private BigDecimal typePkId;

	@Column(name = "Used")
	private int used;

	@Column(name = "Qty")
	private String qty;

	@Column(name = "Times")
	private int times;

	@Column(name = "DayLength")
	private int dayLength;

	@Column(name = "Description")
	private String description;

	@Column(name = "Organ")
	private String organ;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Date")
	private Date date;

	/**
	 * 0 ehleegui 1 ehelsen 2 duussan 3 zogsooson
	 */
	@Column(name = "NursingStatus")
	private byte nursingStatus;

	@Column(name = "OutOfHospital")
	private int outOfHospital;

	@Transient
	private String id;

	@Transient
	private String name;

	@Transient
	private BigDecimal cost;

	@Transient
	private boolean done;

	@Transient
	private String status;

	@Transient
	private boolean select;

	@Transient
	private String displayStr1;

	@Transient
	private String displayStr2;

	@Transient
	private int doneCount;

	@Transient
	private String typeName;

	@Transient
	private String doctorName;

	@Transient
	private Date inspectionDate;

	@Transient
	private Date labTestDate;

	@Transient
	private String ocsListType;

	@Transient
	private BigDecimal examinationDtlsSumPrice;

	@Transient
	private BigDecimal subOrganizationPkId;

	@Transient
	private String subOrganizationName;

	@Transient
	private BigDecimal employeePkId;

	@Transient
	private String employeeName;

	@Transient
	private Date beginDate;

	@Transient
	private String beginDateStr;

	@Transient
	private int inpatientReason;

	@Transient
	private String inpatientReasonStr;

	@Transient
	private BigDecimal nursingGroupPkId;

	@Transient
	private BigDecimal nursingPkId;

	@Transient
	private String nursingStatusStyle;

	@Transient
	private Date dateTime;

	@Transient
	private String inpatientAdvice;

	@Transient
	private boolean out;

	@Transient
	private boolean hasNursing;

	@Transient
	private boolean hasInjection;

	@Transient
	private boolean hasMedicine;

	@Transient
	private Date employeeRequestDate;

	public InspectionDtl() {
		super();
	}
	
	//saveInpatientPayment -> INPATIENT PAYMENT
	public InspectionDtl(InspectionDtl dtl, String employeeName, BigDecimal employeePkId)
	{
		super();
		this.pkId = dtl.getPkId();
		this.inspectionPkId = dtl.getInspectionPkId();
		this.type = dtl.getType();
		this.typePkId = dtl.getTypePkId();
		this.qty = dtl.getQty();
		this.times = dtl.getTimes();
		this.dayLength = dtl.getDayLength();
		this.description = dtl.getDescription();
		this.qty = dtl.getQty();
		this.organ = dtl.getOrgan();
		this.date = dtl.getDate();
		this.nursingStatus = dtl.getNursingStatus();
		this.outOfHospital = dtl.getOutOfHospital();
		this.employeeName = employeeName;
		this.employeePkId = employeePkId;
	}

	public InspectionDtl(String type, BigDecimal typePkId, String bid, String bname, String cid, String cname,
			String did, String dname, String eid, String ename) {
		// xaxaxa
		super();
		this.type = type;
		this.typePkId = typePkId;
		if (Tool.INSPECTIONTYPE_TREATMENT.equals(type)) {
			this.id = bid;
			this.name = bname;
		} else if (Tool.INSPECTIONTYPE_XRAY.equals(type)) {
			this.id = cid;
			this.name = cname;
		} else if (Tool.INSPECTIONTYPE_SURGERY.equals(type)) {
			this.id = did;
			this.name = dname;
		} else if (Tool.INSPECTIONTYPE_EXAMINATION.equals(type)) {
			this.id = eid;
			this.name = ename;
		} else {
			this.id = (bid == null ? "" : bid) + (cid == null ? "" : cid) + (did == null ? "" : did)
					+ (eid == null ? "" : eid);
			this.name = (bname == null ? "" : bname) + (cname == null ? "" : cname) + (dname == null ? "" : dname)
					+ (ename == null ? "" : ename);
		}

	}

	public InspectionDtl(String type, BigDecimal typePkId, String id, String name) {
		super();
		this.type = type;
		this.typePkId = typePkId;
		this.id = id;
		this.name = name;
	}

	public InspectionDtl(BigDecimal pkId, BigDecimal inspectionPkId, BigDecimal treatmentPkId, BigDecimal xrayPkId,
			BigDecimal toothPkId, BigDecimal cost, String treatmentName, String xrayName) {
		super();
		this.pkId = pkId;
		this.inspectionPkId = inspectionPkId;
		this.status = Tool.UNCHANGED;
	}

	public InspectionDtl(BigDecimal pkId, String id, String name, int type) {
		this.typePkId = pkId;
		this.id = id;
		this.name = name;
		this.status = Tool.ADDED;
		if (type == 1)
			this.type = Tool.INSPECTIONTYPE_EXAMINATION;
		if (type == 2)
			this.type = Tool.INSPECTIONTYPE_SURGERY;
		if (type == 3)
			this.type = Tool.INSPECTIONTYPE_TREATMENT;
		if (type == 4)
			this.type = Tool.INSPECTIONTYPE_XRAY;
		if (type == 5)
			this.type = Tool.INSPECTIONTYPE_INPATIENT;
	}

	public InspectionDtl(InspectionDtl dtl, String treatmentName, String xrayName, String exaName, String surgeryName) {
		super();
		this.pkId = dtl.getPkId();
		this.inspectionPkId = dtl.getInspectionPkId();
		this.type = dtl.getType();
		this.typePkId = dtl.getTypePkId();
		this.qty = dtl.getQty();
		this.times = dtl.getTimes();
		this.dayLength = dtl.getDayLength();
		if ("XRAY".equals(dtl.getType()))
			this.name = xrayName;
		else if ("TREATMENT".equals(dtl.getType()))
			this.name = treatmentName;
		else if ("EXAMINATION".equals(dtl.getType()))
			this.name = exaName;
		else if ("SURGERY".equals(dtl.getType()))
			this.name = surgeryName;
		else if ("SURGERY".equals(dtl.getType()))
			this.name = surgeryName;
		this.organ = dtl.getOrgan();
	}

	public InspectionDtl(InspectionDtl dtl, String treatmentName, String xrayName, String exaName, String surgeryName,
			String medicine, String injection, String nursingServiceName) {
		super();
		this.pkId = dtl.getPkId();
		this.inspectionPkId = dtl.getInspectionPkId();
		this.type = dtl.getType();
		this.typePkId = dtl.getTypePkId();
		this.qty = dtl.getQty();
		this.times = dtl.getTimes();
		this.dayLength = dtl.getDayLength();
		
		if (dtl.getType() != null) {
			System.out.println(dtl.getType());
			if (Tool.INSPECTIONTYPE_XRAY.toUpperCase().equals(dtl.getType().toUpperCase()))
				this.name = xrayName;
			else if (Tool.INSPECTIONTYPE_TREATMENT.toUpperCase().equals(dtl.getType().toUpperCase()))
				this.name = treatmentName;
			else if (Tool.INSPECTIONTYPE_EXAMINATION.toUpperCase().equals(dtl.getType().toUpperCase()))
				this.name = exaName;
			else if (Tool.INSPECTIONTYPE_SURGERY.toUpperCase().equals(dtl.getType().toUpperCase()))
				this.name = surgeryName;
			else if (Tool.INPATIENTDTLSTATUS_Medicine.toUpperCase().equals(dtl.getType().toUpperCase()))
				this.name = medicine;
			else if (Tool.INPATIENTDTLSTATUS_INJECTION.toUpperCase().equals(dtl.getType().toUpperCase()))
				this.name = injection;
			else if (Tool.INPATIENTDTLSTATUS_Nursing.toUpperCase().equals(dtl.getType().toUpperCase()))
				this.name = nursingServiceName;
		}
		this.organ = dtl.getOrgan();
	}

	public InspectionDtl(InspectionDtl dtl, String treatmentName, String xrayName, String exaName, String surgeryName,
			String treatmentId, String xrayId, String exaId, String surgeryId) {
		super();
		this.pkId = dtl.getPkId();
		this.inspectionPkId = dtl.getInspectionPkId();
		this.type = dtl.getType();
		this.typePkId = dtl.getTypePkId();
		this.qty = dtl.getQty();
		this.times = dtl.getTimes();
		this.dayLength = dtl.getDayLength();
		if ("XRAY".equals(dtl.getType())) {
			this.id = xrayId;
			this.name = xrayName;
		} else if ("TREATMENT".equals(dtl.getType())) {
			this.id = treatmentId;
			this.name = treatmentName;
		} else if ("EXAMINATION".equals(dtl.getType())) {
			this.id = exaId;
			this.name = exaName;
		} else if ("SURGERY".equals(dtl.getType())) {
			this.id = surgeryId;
			this.name = surgeryName;
		}

	}

	public InspectionDtl(InspectionDtl dtl, String id, String name) {
		super();
		this.pkId = dtl.getPkId();
		this.inspectionPkId = dtl.getInspectionPkId();
		this.type = dtl.getType();
		this.typePkId = dtl.getTypePkId();
		this.qty = dtl.getQty();
		this.times = dtl.getTimes();
		this.dayLength = dtl.getDayLength();
		this.id = id;
		this.name = name;
		this.select = true;
		this.description = dtl.getDescription();
	}

	public InspectionDtl(InspectionDtl dtl, String treatmentName, String treatmentTypeName, Date inspectionDate,
			int doneCount, String doctorName) {
		super();
		this.pkId = dtl.getPkId();
		this.inspectionPkId = dtl.getInspectionPkId();
		this.type = dtl.getType();
		this.typePkId = dtl.getTypePkId();
		this.qty = dtl.getQty();
		this.times = dtl.getTimes();
		this.dayLength = dtl.getDayLength();
		this.name = treatmentName;
		this.typeName = treatmentTypeName;
		this.inspectionDate = inspectionDate;
		this.doneCount = doneCount;
		this.doctorName = doctorName;
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getInspectionPkId() {
		return inspectionPkId;
	}

	public void setInspectionPkId(BigDecimal inspectionPkId) {
		this.inspectionPkId = inspectionPkId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getTypePkId() {
		return typePkId;
	}

	public void setTypePkId(BigDecimal typePkId) {
		this.typePkId = typePkId;
	}

	public int getUsed() {
		return used;
	}

	public void setUsed(int used) {
		this.used = used;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public int getTimes() {
		return times;
	}

	public void setTimes(int times) {
		this.times = times;
	}

	public int getDayLength() {
		return dayLength;
	}

	public void setDayLength(int dayLength) {
		this.dayLength = dayLength;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getCost() {
		if (cost == null)
			cost = BigDecimal.ZERO;
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		if (done)
			used = 1;
		this.done = done;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isSelect() {
		return select;
	}

	public void setSelect(boolean select) {
		this.select = select;
	}

	public String getDisplayStr1() {
		if (Tool.LAST.equals(getStatus()))
			displayStr1 = "display: none; ";
		else
			displayStr1 = "";
		return displayStr1;
	}

	public void setDisplayStr1(String displayStr1) {
		this.displayStr1 = displayStr1;
	}

	public String getDisplayStr2() {
		if (!Tool.LAST.equals(getStatus()))
			displayStr2 = "display: none; ";
		else
			displayStr2 = "";
		return displayStr2;
	}

	public void setDisplayStr2(String displayStr2) {
		this.displayStr2 = displayStr2;
	}

	public int getDoneCount() {
		return doneCount;
	}

	public void setDoneCount(int doneCount) {
		this.doneCount = doneCount;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public Date getInspectionDate() {
		return inspectionDate;
	}

	public void setInspectionDate(Date inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	public String getDateString(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	public Date getLabTestDate() {
		if (labTestDate == null)
			labTestDate = new Date();
		return labTestDate;
	}

	public void setLabTestDate(Date labTestDate) {
		this.labTestDate = labTestDate;
	}

	public String getOcsListType() {
		ocsListType = Tool.INSPECTION;
		return ocsListType;
	}

	public void setOcsListType(String ocsListType) {
		this.ocsListType = ocsListType;
	}

	public String getOrgan() {
		return organ;
	}

	public void setOrgan(String organ) {
		this.organ = organ;
	}

	public BigDecimal getEmployeePkId() {
		return employeePkId;
	}

	public void setEmployeePkId(BigDecimal employeePkId) {
		this.employeePkId = employeePkId;
	}

	public BigDecimal getSubOrganizationPkId() {
		return subOrganizationPkId;
	}

	public void setSubOrganizationPkId(BigDecimal subOrganizationPkId) {
		this.subOrganizationPkId = subOrganizationPkId;
	}

	public Date getDate() {
		if (date == null)
			date = new Date();
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getSubOrganizationName() {
		return subOrganizationName;
	}

	public void setSubOrganizationName(String subOrganizationName) {
		this.subOrganizationName = subOrganizationName;
	}

	public Date getBeginDate() {
		if (beginDate == null)
			beginDate = new Date();
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public String getBeginDateStr() {
		beginDateStr = new SimpleDateFormat("yyyy-MM-dd").format(getBeginDate());
		return beginDateStr;
	}

	public void setBeginDateStr(String beginDateStr) {
		this.beginDateStr = beginDateStr;
	}

	public int getInpatientReason() {
		return inpatientReason;
	}

	public void setInpatientReason(int inpatientReason) {
		this.inpatientReason = inpatientReason;
	}

	public String getInpatientAdvice() {
		return inpatientAdvice;
	}

	public void setInpatientAdvice(String inpatientAdvice) {
		this.inpatientAdvice = inpatientAdvice;
	}

	public String getInpatientReasonStr() {
		return inpatientReasonStr;
	}

	public void setInpatientReasonStr(String inpatientReasonStr) {
		this.inpatientReasonStr = inpatientReasonStr;
	}

	public BigDecimal getNursingGroupPkId() {
		return nursingGroupPkId;
	}

	public void setNursingGroupPkId(BigDecimal nursingGroupPkId) {
		this.nursingGroupPkId = nursingGroupPkId;
	}

	public BigDecimal getNursingPkId() {
		return nursingPkId;
	}

	public void setNursingPkId(BigDecimal nursingPkId) {
		this.nursingPkId = nursingPkId;
	}

	public String getNursingStatusStyle() {
		nursingStatusStyle = "";
		if (nursingStatus == 1) {
			nursingStatusStyle = "background: black; color: white;";
		}
		if (nursingStatus == 2) {
			nursingStatusStyle = "background: green; color: white;";
		}
		if (nursingStatus == 3) {
			nursingStatusStyle = "background: red; color: white;";
		}
		return nursingStatusStyle;
	}

	public void setNursingStatusStyle(String nursingStatusStyle) {
		this.nursingStatusStyle = nursingStatusStyle;
	}

	public byte getNursingStatus() {
		return nursingStatus;
	}

	public void setNursingStatus(byte nursingStatus) {
		this.nursingStatus = nursingStatus;
	}

	public Date getDateTime() {
		if (dateTime == null)
			dateTime = new Date();
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public boolean isOut() {
		out = getOutOfHospital() == 1;
		return out;
	}

	public void setOut(boolean out) {
		setOutOfHospital(out ? 1 : 0);
		this.out = out;
	}

	public boolean isHasNursing() {
		return hasNursing;
	}

	public void setHasNursing(boolean hasNursing) {
		this.hasNursing = hasNursing;
	}

	public boolean isHasInjection() {
		return hasInjection;
	}

	public void setHasInjection(boolean hasInjection) {
		this.hasInjection = hasInjection;
	}

	public boolean isHasMedicine() {
		return hasMedicine;
	}

	public void setHasMedicine(boolean hasMedicine) {
		this.hasMedicine = hasMedicine;
	}

	public Date getEmployeeRequestDate() {
		if (employeeRequestDate == null)
			employeeRequestDate = new Date();
		return employeeRequestDate;
	}

	public void setEmployeeRequestDate(Date employeeRequestDate) {
		this.employeeRequestDate = employeeRequestDate;
	}

	public int getOutOfHospital() {
		return outOfHospital;
	}

	public void setOutOfHospital(int outOfHospital) {
		this.outOfHospital = outOfHospital;
	}

}