package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "WarningMedicineDose")
public class WarningMedicineDose implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name = "MedicinePkId")
	private BigDecimal medicinePkId;
	
	@Column(name = "MaxDose")
	private String maxDose;
	
	@Transient
	private boolean select;
	
	public WarningMedicineDose(){
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getMedicinePkId() {
		return medicinePkId;
	}

	public void setMedicinePkId(BigDecimal medicinePkId) {
		this.medicinePkId = medicinePkId;
	}

	public String getMaxDose() {
		return maxDose;
	}

	public void setMaxDose(String maxDose) {
		this.maxDose = maxDose;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public boolean isSelect() {
		return select;
	}
	
	public void setSelect(boolean select) {
		this.select = select;
	}
	
	
}
