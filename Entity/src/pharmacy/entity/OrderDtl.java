package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="OrderDtl")
public class OrderDtl implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", nullable = false, length = 18)
	private BigDecimal pkId;
	
	@Column(name = "HdrPkId")
	private BigDecimal hdrPkId;
	
	@Column(name = "ItemPkId")
	private BigDecimal itemPkId;
	
	@Column(name = "OrderQty")
	private BigDecimal orderQty;
	
	@Column(name = "Qty")
	private BigDecimal qty;
	
	@Column(name = "Description")
	private String description;

	private String status;
	
	private String name;
	
	public OrderDtl(){
		super();
	}
	
	public OrderDtl(BigDecimal pkId, String name){
		this.itemPkId = pkId;
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getHdrPkId() {
		return hdrPkId;
	}

	public void setHdrPkId(BigDecimal hdrPkId) {
		this.hdrPkId = hdrPkId;
	}

	public BigDecimal getItemPkId() {
		return itemPkId;
	}

	public void setItemPkId(BigDecimal itemPkId) {
		this.itemPkId = itemPkId;
	}

	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(BigDecimal orderQty) {
		this.orderQty = orderQty;
	}
	
	
}
