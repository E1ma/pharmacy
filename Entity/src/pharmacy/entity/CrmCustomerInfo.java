package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "CrmCustomerInfo")
public class CrmCustomerInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;

	//МЭДЭЭЛЭЛ
	@Column(name = "Info")
	private String info;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;

	@Column(name="CrmCustomerPkId")
	private BigDecimal crmCustomerPkId;
	
	@Column(name="CreatedBy")
	private BigDecimal createdBy;

	@Transient
	private String status;
	
	@Transient
	private String createdByName;
	
	public CrmCustomerInfo() {
		super();
	}
	
	//crmLogic -> getCustomerInfo()
	public CrmCustomerInfo(CrmCustomerInfo info, String createdByName) {
		super();
		this.pkId = info.getPkId();
		this.info = info.getInfo();
		this.createdDate = info.getCreatedDate();
		this.createdBy = info.getCreatedBy();
		this.createdByName = createdByName;
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getCrmCustomerPkId() {
		return crmCustomerPkId;
	}

	public void setCrmCustomerPkId(BigDecimal crmCustomerPkId) {
		this.crmCustomerPkId = crmCustomerPkId;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedByName() {
		return createdByName;
	}

	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
}