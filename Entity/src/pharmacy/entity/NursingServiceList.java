package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "NursingServiceList")
public class NursingServiceList implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name = "InspectionPkId")
	private BigDecimal inspectionPkId;
	
	@Column(name = "InspectionDtlPkId")
	private BigDecimal inspectionDtlPkId;
	
	@Column(name = "InpatientHdrPkId")
	private BigDecimal inpatientHdrPkId;
	
	/*
	 * TREATMENT
	 * SURGERY
	 * XRAY
	 * EXAMINATION
	 * INSPECTIONMEDICINE
	 * Injection
	 * 
	 * 
	 * */
	@Column(name = "Type")
	private String type;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Date")
	private Date date;
	
	//Ашиглах  огноо эм эмчилнээ  шинжилгээ гэх  мэт 
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UsedDate")
	private Date usedDate;
	
	//Хүлээж авсан
	@Column(name = "IsUsed")
	private byte isUsed;
	
	@Column(name = "Status")
	private byte status;
	
	@Column(name = "NursePkId")
	private BigDecimal nursePkId;
	
	/**
	 * 
	 * 0 - Өөрчдөгөхгүй
	 * 1 - Захиалсан
	 * 2 - Хэрэгжүүлсэн
	 * 3 - Зогсоосон
	 * 4 - Цуцалсан
	 * 5 - Татгалзсан
	 * */
	@Column(name = "ServiseStatus")
	private byte serviseStatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "StartDate")
	private Date startDate;
	
	@Column(name = "Description")
	private String description;
	
	/**
	 * 
	 * 0 - Өөрчдөгөхгүй
	 * 1 - Захиалсан
	 * 2 - Хэрэгжүүлсэн
	 * 3 - Зогсоосон
	 * 4 - Цуцалсан
	 * 5 - Татгалзсан
	 * */
	@Column(name = "M")
	private int m;
	
	/**
	 * 
	 * 0 - Өөрчдөгөхгүй
	 * 1 - Захиалсан
	 * 2 - Хэрэгжүүлсэн
	 * 3 - Зогсоосон
	 * 4 - Цуцалсан
	 * 5 - Татгалзсан
	 * */
	@Column(name = "D")
	private int d;
	
	/**
	 * 
	 * 0 - Өөрчдөгөхгүй
	 * 1 - Захиалсан
	 * 2 - Хэрэгжүүлсэн
	 * 3 - Зогсоосон
	 * 4 - Цуцалсан
	 * 5 - Татгалзсан
	 * */
	@Column(name = "E")
	private int e;
	
	/**
	 * 
	 * 0 - Өөрчдөгөхгүй
	 * 1 - Захиалсан
	 * 2 - Хэрэгжүүлсэн
	 * 3 - Зогсоосон
	 * 4 - Цуцалсан
	 * 5 - Татгалзсан
	 * */
	@Column(name = "N")
	private int n;
	
	@Column(name = "Confirm")
	private byte confirm;
	
	//Ахлах сувилагч хянасан
	@Column(name = "HeadNurse")
	private byte headNurse;
	
	//Сувилагч хэрэгжүүлэх  хэсгээс бүгд  хэрэгжсэн  огноо

	
	@Transient
	private int   nursingServiceOrderCount;
	
	@Transient
	private String statusTool;
	
	@Transient
	private String typeMn;
	
	public NursingServiceList(){
		super();
	}
	
	public NursingServiceList(long count){
		this.nursingServiceOrderCount= (int)count;
	}
	
	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getInspectionPkId() {
		return inspectionPkId;
	}

	public void setInspectionPkId(BigDecimal inspectionPkId) {
		this.inspectionPkId = inspectionPkId;
	}

	public BigDecimal getInspectionDtlPkId() {
		return inspectionDtlPkId;
	}

	public void setInspectionDtlPkId(BigDecimal inspectionDtlPkId) {
		this.inspectionDtlPkId = inspectionDtlPkId;
	}

	public BigDecimal getInpatientHdrPkId() {
		return inpatientHdrPkId;
	}

	public void setInpatientHdrPkId(BigDecimal inpatientHdrPkId) {
		this.inpatientHdrPkId = inpatientHdrPkId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public byte getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(byte isUsed) {
		this.isUsed = isUsed;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public BigDecimal getNursePkId() {
		return nursePkId;
	}

	public void setNursePkId(BigDecimal nursePkId) {
		this.nursePkId = nursePkId;
	}

	public byte getServiseStatus() {
		return serviseStatus;
	}

	public void setServiseStatus(byte serviseStatus) {
		this.serviseStatus = serviseStatus;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getM() {
		return m;
	}

	public void setM(int m) {
		this.m = m;
	}

	public int getD() {
		return d;
	}

	public void setD(int d) {
		this.d = d;
	}

	public int getE() {
		return e;
	}

	public void setE(int e) {
		this.e = e;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getUsedDate() {
		return usedDate;
	}

	public void setUsedDate(Date usedDate) {
		this.usedDate = usedDate;
	}

	public int getNursingServiceOrderCount() {
		return nursingServiceOrderCount;
	}

	public void setNursingServiceOrderCount(int nursingServiceOrderCount) {
		this.nursingServiceOrderCount = nursingServiceOrderCount;
	}

	public byte getConfirm() {
		return confirm;
	}

	public void setConfirm(byte confirm) {
		this.confirm = confirm;
	}

	public byte getHeadNurse() {
		return headNurse;
	}

	public void setHeadNurse(byte headNurse) {
		this.headNurse = headNurse;
	}

	public String getStatusTool() {
		return statusTool;
	}

	public void setStatusTool(String statusTool) {
		this.statusTool = statusTool;
	}

	public String getTypeMn() {
		return typeMn;
	}

	public void setTypeMn(String typeMn) {
		this.typeMn = typeMn;
	}

	
}
