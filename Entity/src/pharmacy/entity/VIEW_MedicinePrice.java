package pharmacy.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "VIEW_MedicinePrice")
public class VIEW_MedicinePrice {

	@Id
	@Column(name = "MedicinePkId", length = 18, nullable = false)
	private BigDecimal medicinePkId;

	@Column(name = "Price")
	private BigDecimal price;

	@Column(name = "InpatientPrice")
	private BigDecimal inpatientPrice;
	
	@Column(name = "InsurancePrice")
	private BigDecimal insurancePrice;
	
	@Column(name = "IsHasInsurance")
	private byte isHasInsurance;
	
	@Column(name = "Name")
	private String name;
	
	@Column(name = "Dose")
	private String dose;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BeginDate")
	private Date beginDate;
	
	public VIEW_MedicinePrice()
	{
		super();
	}

	public VIEW_MedicinePrice(BigDecimal medicinePkId, BigDecimal price, BigDecimal inpatientPrice,
			BigDecimal insurancePrice, Date beginDate) {
		super();
		this.medicinePkId = medicinePkId;
		this.price = price;
		this.inpatientPrice = inpatientPrice;
		this.insurancePrice = insurancePrice;
		this.beginDate = beginDate;
	}

	public BigDecimal getMedicinePkId() {
		return medicinePkId;
	}

	public void setMedicinePkId(BigDecimal medicinePkId) {
		this.medicinePkId = medicinePkId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getInpatientPrice() {
		return inpatientPrice;
	}

	public void setInpatientPrice(BigDecimal inpatientPrice) {
		this.inpatientPrice = inpatientPrice;
	}

	public BigDecimal getInsurancePrice() {
		return insurancePrice;
	}

	public void setInsurancePrice(BigDecimal insurancePrice) {
		this.insurancePrice = insurancePrice;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public String getDose() {
		return dose;
	}

	public void setDose(String dose) {
		this.dose = dose;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getIsHasInsurance() {
		return isHasInsurance;
	}

	public void setIsHasInsurance(byte isHasInsurance) {
		this.isHasInsurance = isHasInsurance;
	}
}