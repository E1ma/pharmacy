package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "NotUserTogether")
public class NotUserTogether implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", nullable = false, length = 18)
	private BigDecimal pkId;
	
	@Column(name = "MedicinePkId1")
	private BigDecimal medicinePkId1;
	
	@Column(name = "MedicinePkId2")
	private BigDecimal medicinePkId2;
	
	@Column(name = "Message")
	private String message;
	
	public NotUserTogether(){
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getMedicinePkId1() {
		return medicinePkId1;
	}

	public void setMedicinePkId1(BigDecimal medicinePkId1) {
		this.medicinePkId1 = medicinePkId1;
	}

	public BigDecimal getMedicinePkId2() {
		return medicinePkId2;
	}

	public void setMedicinePkId2(BigDecimal medicinePkId2) {
		this.medicinePkId2 = medicinePkId2;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
