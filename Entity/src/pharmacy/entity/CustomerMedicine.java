package pharmacy.entity;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "CustomerMedicine")
public class CustomerMedicine implements Cloneable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "PkId", nullable = false, length = 18)
	private BigDecimal pkId;
	
	@Column(name = "InspectionPkId")
	private BigDecimal inspectionPkId;
	
	@Column(name = "MedicinePkId")
	private BigDecimal medicinePkId;
	
	@Column(name = "EmployeePkId")
	private BigDecimal employeePkId;
	
	@Column(name = "CustomerPkId")
	private BigDecimal customerPkId;
	
	@Column(name = "RepeatType")
	private int repeatType;

	@Column(name = "RepeatCount")
	private int repeatCount;

	@Column(name = "Dose")
	private String dose;
	
	@Column(name = "Time")
	private int time;
	
	@Column(name = "Day")
	private int day;

	@Column(name = "ExpireDay")
	private int expireDay;
	
	@Column(name = "DispenseCount")
	private int dispenseCount;
	
	@Column(name = "M")
	private byte m;
	
	@Column(name = "D")
	private byte d;
	
	@Column(name = "E")
	private byte e;
	
	@Column(name = "N")
	private byte n;
	
	@Column(name="IsDispenseMedicne")
	private byte isDispenseMedicne;

	@Column(name = "MedicineDescription")
	private String medicineDescription;
	
	@Column(name = "BeginDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date beginDate;
	
	@Column(name = "Type")
	private String type;
	
	@Column(name = "MedicineType")
	private byte mType;
	
	@Column(name = "CreatedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "CreatedBy")
	private BigDecimal createdBy;

	@Column(name = "UpdatedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@Column(name = "UpdatedBy")
	private BigDecimal updatedBy;
	
	@Column(name = "UseMethod")
	private String useMethod;
	
	@Transient
	private BigDecimal inspectionDtlPkId;
	
	@Transient
	private String id;
	
	@Transient
	private int nursingStatus;
	
	@Transient
	private String name;
	
	@Transient
	private String iName;

	@Transient
	private String measurementName;
	
	@Transient
	private String status;
	
	@Transient
	private boolean selectM;
	
	@Transient
	private boolean selectD;
	
	@Transient
	private boolean selectE;
	
	@Transient
	private boolean selectN;
	
	@Transient
	private boolean select;
	
	@Transient
	private BigDecimal medicineTypePkId;
	
	@Transient
	private String drugDose;
	
	@Transient
	private String displayStr1;
	
	@Transient
	private BigDecimal measurementPkId;
	
	@Transient
	private String displayStr2;
	
	@Transient
	private String medicineType;
	
	@Transient
	private View_ConstantMedicineType constantMedicineType;
	
    @Transient
	private int calcDrugDose;
	
	@Transient
	private int calcDose;
	
	@Transient
	private String calcType;
	
	@Transient
	private int balanceNumber;
	
	@Transient
	private int inputNumber;
	
	@Transient
	private String subOrganizationName;
	
	@Transient 
	private String employeeName;
	
	@Transient
	private Medicine medicine;
	
	@Transient
	private String createdDateStr;
	
	@Transient
	private boolean dispenseMedicne;
	
	@Transient
	private String drinkType;
	
	@Transient
	private int total;
	
	@Transient
	private Date dateTime;
	
	@Transient
	private boolean out;
	
	@Transient
	private BigDecimal price;
	
	@Transient
	private String nursingStatusStyle;
	
	@Transient
	private int warningMaxDay;
	
	@Transient
	private boolean wMaxDay;
	
	@Transient
	private String wMaxMessage;
	
	@Transient
	private String patientType;
	
	@Transient
	private int isDivide;
	
	@Transient
	private int warningMedicine;
	
	@Transient
	private String warningDose;
	
	@Transient
	private String warningDoseMessage;
	
	@Transient
	private int warningDoseOver;
	
	public CustomerMedicine() {
		super();
	}
	
	public Object clone() throws CloneNotSupportedException{  
		return (CustomerMedicine)super.clone();  
    }
	
	//LogicCashier -> getInpatientCashByInspectionDtl
	public CustomerMedicine(String name, BigDecimal price){
		super();
		this.name = name;
		this.price = price;
	}
	
	public CustomerMedicine(BigDecimal medicinePkId, BigDecimal employeePkId, String id, String name) {
		this.medicinePkId = medicinePkId;
		this.employeePkId = employeePkId;
		this.id = id;
		this.name = name;
		this.repeatType = 0;
		this.repeatCount = 0;
		this.expireDay = 0;
		this.dose = "";
	}
	
	public CustomerMedicine(BigDecimal medicinePkId, String id, String name) {
		this.medicinePkId = medicinePkId;
		this.id = id;
		this.name = name;
		this.repeatType = 0;
		this.repeatCount = 0;
		this.expireDay = 0;
		this.dose = "";
	}
	
	public CustomerMedicine(BigDecimal medicinePkId, String id, String name, String iName) {
		this.medicinePkId = medicinePkId;
		this.id = id;
		this.name = name;
		this.iName = iName;
		this.repeatType = 0;
		this.repeatCount = 0;
		this.expireDay = 0;
		this.dose = "";
	}
	
	public CustomerMedicine(CustomerMedicine customerMedicine, Medicine medicine) {
		super();
		
		this.pkId = customerMedicine.getPkId();
		this.inspectionPkId = customerMedicine.getInspectionPkId();
		this.medicinePkId = customerMedicine.getMedicinePkId();
		this.employeePkId = customerMedicine.getEmployeePkId();
		this.repeatType = customerMedicine.getRepeatType();
		this.repeatCount = customerMedicine.getRepeatCount();
		this.dose = customerMedicine.getDose();
		this.time = customerMedicine.getTime();
		this.day = customerMedicine.getDay();
		this.expireDay = customerMedicine.getExpireDay();
		this.m = customerMedicine.getM();
		this.d = customerMedicine.getD();
		this.e = customerMedicine.getE();
		this.n = customerMedicine.getN();
		this.medicineDescription = customerMedicine.getMeasurementName();
		this.medicineDescription  = customerMedicine.getMedicineDescription();
		this.createdDate = customerMedicine.getCreatedDate();
		this.createdBy = customerMedicine.getCreatedBy();
		this.updatedDate = customerMedicine.getUpdatedDate();
		this.updatedBy = customerMedicine.getUpdatedBy();
		this.name = medicine.getName();
	}
	public CustomerMedicine(CustomerMedicine customerMedicine, Medicine medicine,View_ConstantMedicineType constantMedicineType) {
		super();
		
		this.pkId = customerMedicine.getPkId();
		this.inspectionPkId = customerMedicine.getInspectionPkId();
		this.medicinePkId = customerMedicine.getMedicinePkId();
		this.employeePkId = customerMedicine.getEmployeePkId();
		this.repeatType = customerMedicine.getRepeatType();
		this.repeatCount = customerMedicine.getRepeatCount();
		this.dose = customerMedicine.getDose();
		this.drugDose = medicine.getDrugDose();
		this.time = customerMedicine.getTime();
		this.day = customerMedicine.getDay();
		this.expireDay = customerMedicine.getExpireDay();
		this.m = customerMedicine.getM();
		this.d = customerMedicine.getD();
		this.e = customerMedicine.getE();
		this.n = customerMedicine.getN();
		this.medicineDescription = customerMedicine.getMeasurementName();
		this.medicineDescription  = customerMedicine.getMedicineDescription();
		this.createdDate = customerMedicine.getCreatedDate();
		this.createdBy = customerMedicine.getCreatedBy();
		this.updatedDate = customerMedicine.getUpdatedDate();
		this.updatedBy = customerMedicine.getUpdatedBy();
		this.constantMedicineType  = constantMedicineType;
		this.name = medicine.getName();
		this.iName  =  medicine.getiName();
	}
	
	public CustomerMedicine(CustomerMedicine customerMedicine, String id, String name){
		super();
		this.id = id;
		this.name = name;
		this.pkId = customerMedicine.getPkId();
		this.inspectionPkId = customerMedicine.getInspectionPkId();
		this.medicinePkId = customerMedicine.getMedicinePkId();
		this.employeePkId = customerMedicine.getEmployeePkId();
		this.repeatType = customerMedicine.getRepeatType();
		this.repeatCount = customerMedicine.getRepeatCount();
		this.dose = customerMedicine.getDose();
		this.time = customerMedicine.getTime();
		this.day = customerMedicine.getDay();
		this.expireDay = customerMedicine.getExpireDay();
		this.m = customerMedicine.getM();
		this.d = customerMedicine.getD();
		this.e = customerMedicine.getE();
		this.n = customerMedicine.getN();
		this.medicineDescription = customerMedicine.getMeasurementName();
		this.createdDate = customerMedicine.getCreatedDate();
		this.createdBy = customerMedicine.getCreatedBy();
		this.updatedDate = customerMedicine.getUpdatedDate();
		this.updatedBy = customerMedicine.getUpdatedBy();
		this.select = true;
	}
	
	public CustomerMedicine(CustomerMedicine customerMedicine, String id, String name, String iName, String drugDose, int calcDose, int calcDrugDose, String calcType, BigDecimal typePkId){
		super();
		this.id = id;
		this.name = name;
		this.iName = iName;
		this.pkId = customerMedicine.getPkId();
		this.inspectionPkId = customerMedicine.getInspectionPkId();
		this.medicinePkId = customerMedicine.getMedicinePkId();
		this.employeePkId = customerMedicine.getEmployeePkId();
		this.repeatType = customerMedicine.getRepeatType();
		this.repeatCount = customerMedicine.getRepeatCount();
		this.dose = customerMedicine.getDose();
		this.time = customerMedicine.getTime();
		this.day = customerMedicine.getDay();
		this.drugDose = drugDose;
		this.dose = customerMedicine.getDose();
		this.expireDay = customerMedicine.getExpireDay();
		this.m = customerMedicine.getM();
		this.d = customerMedicine.getD();
		this.e = customerMedicine.getE();
		this.n = customerMedicine.getN();
		this.medicineDescription = customerMedicine.getMedicineDescription();
		this.createdDate = customerMedicine.getCreatedDate();
		this.createdBy = customerMedicine.getCreatedBy();
		this.updatedDate = customerMedicine.getUpdatedDate();
		this.updatedBy = customerMedicine.getUpdatedBy();
		this.calcDose = calcDose;
		this.calcDrugDose = calcDrugDose;
		this.calcType = calcType;
		this.select = true;
		this.medicineTypePkId = typePkId;
	}
	
	public CustomerMedicine(CustomerMedicine customerMedicine, Medicine medicine, String measurementName) {
		super();
		this.pkId = customerMedicine.getPkId();
		this.inspectionPkId = customerMedicine.getInspectionPkId();
		this.medicinePkId = customerMedicine.getMedicinePkId();
		this.employeePkId = customerMedicine.getEmployeePkId();
		this.repeatType = customerMedicine.getRepeatType();
		this.repeatCount = customerMedicine.getRepeatCount();
		this.dose = customerMedicine.getDose();
		this.time = customerMedicine.getTime();
		this.day = customerMedicine.getDay();
		this.expireDay = customerMedicine.getExpireDay();
		this.m = customerMedicine.getM();
		this.d = customerMedicine.getD();
		this.e = customerMedicine.getE();
		this.n = customerMedicine.getN();
		this.medicineDescription = customerMedicine.getMeasurementName();
		this.createdDate = customerMedicine.getCreatedDate();
		this.createdBy = customerMedicine.getCreatedBy();
		this.updatedDate = customerMedicine.getUpdatedDate();
		this.updatedBy = customerMedicine.getUpdatedBy();
		this.name = medicine.getName();
		this.measurementName = measurementName;
	}
	
	public CustomerMedicine(CustomerMedicine customerMedicine, Medicine medicine, String measurementName, String subOrganizationName, String employeeName, String typeName) {
		super();
		this.pkId = customerMedicine.getPkId();
		this.inspectionPkId = customerMedicine.getInspectionPkId();
		this.medicinePkId = customerMedicine.getMedicinePkId();
		this.employeePkId = customerMedicine.getEmployeePkId();
		this.repeatType = customerMedicine.getRepeatType();
		this.repeatCount = customerMedicine.getRepeatCount();
		this.dose = customerMedicine.getDose();
		this.time = customerMedicine.getTime();
		this.day = customerMedicine.getDay();
		this.expireDay = customerMedicine.getExpireDay();
		this.m = customerMedicine.getM();
		this.d = customerMedicine.getD();
		this.e = customerMedicine.getE();
		this.n = customerMedicine.getN();
		this.medicineDescription = customerMedicine.getMeasurementName();
		this.createdDate = customerMedicine.getCreatedDate();
		this.createdBy = customerMedicine.getCreatedBy();
		this.updatedDate = customerMedicine.getUpdatedDate();
		this.updatedBy = customerMedicine.getUpdatedBy();
		this.name = medicine.getName();
		this.measurementName = measurementName;
		this.employeeName = employeeName;
		this.subOrganizationName = subOrganizationName;
		this.medicine = medicine;
		this.medicine.setTypeName(typeName);
		this.medicineDescription = customerMedicine.getMedicineDescription();
		
	}
	
	public CustomerMedicine(CustomerMedicine customerMedicine, Medicine medicine, String measurementName, String subOrganizationName, String employeeName, String typeName, String patientType) {
		super();
		this.pkId = customerMedicine.getPkId();
		this.inspectionPkId = customerMedicine.getInspectionPkId();
		this.medicinePkId = customerMedicine.getMedicinePkId();
		this.employeePkId = customerMedicine.getEmployeePkId();
		this.repeatType = customerMedicine.getRepeatType();
		this.repeatCount = customerMedicine.getRepeatCount();
		this.dose = customerMedicine.getDose();
		this.time = customerMedicine.getTime();
		this.day = customerMedicine.getDay();
		this.expireDay = customerMedicine.getExpireDay();
		this.m = customerMedicine.getM();
		this.d = customerMedicine.getD();
		this.e = customerMedicine.getE();
		this.n = customerMedicine.getN();
		this.medicineDescription = customerMedicine.getMeasurementName();
		this.createdDate = customerMedicine.getCreatedDate();
		this.createdBy = customerMedicine.getCreatedBy();
		this.updatedDate = customerMedicine.getUpdatedDate();
		this.updatedBy = customerMedicine.getUpdatedBy();
		this.name = medicine.getName();
		this.measurementName = measurementName;
		this.employeeName = employeeName;
		this.subOrganizationName = subOrganizationName;
		this.medicine = medicine;
		this.medicine.setTypeName(typeName);
		this.medicineDescription = customerMedicine.getMedicineDescription();
		this.patientType = patientType;
		
	}
	public CustomerMedicine(BigDecimal pkId, long dispenseCount){
		this.pkId = pkId;
		this.dispenseCount = new BigDecimal(dispenseCount).intValueExact();
	}
	
	public CustomerMedicine(CustomerMedicine customerMedicine, String name){
		super();
		this.pkId = customerMedicine.getPkId();
		this.day = customerMedicine.getDay();
		this.time = customerMedicine.getTime();
		this.dose = customerMedicine.getDose();
		this.drinkType = "";
		if (customerMedicine.getM() == 1)
			this.drinkType = this.drinkType + "өглөө ";
		if (customerMedicine.getD() == 1)
			this.drinkType = this.drinkType + "өдөр ";
		if (customerMedicine.getE() == 1)
			this.drinkType = this.drinkType + "орой ";
		if (customerMedicine.getN() == 1)
			this.drinkType = this.drinkType + "шөнө ";
		this.createdDate = customerMedicine.getCreatedDate();
		this.name = name;
	}
	

	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getInspectionPkId() {
		return inspectionPkId;
	}

	public void setInspectionPkId(BigDecimal inspectionPkId) {
		this.inspectionPkId = inspectionPkId;
	}

	public BigDecimal getMedicinePkId() {
		return medicinePkId;
	}

	public void setMedicinePkId(BigDecimal medicinePkId) {
		this.medicinePkId = medicinePkId;
	}

	public int getRepeatType() {
		return repeatType;
	}

	public void setRepeatType(int repeatType) {
		this.repeatType = repeatType;
	}

	public int getRepeatCount() {
		return repeatCount;
	}

	public void setRepeatCount(int repeatCount) {
		this.repeatCount = repeatCount;
	}

	public String getDose() {
		if(dose == null) dose = "";
		return dose;
	}

	public void setDose(String dose) {
		this.dose = dose;
	}

	public int getExpireDay() {
		return expireDay;
	}

	public void setExpireDay(int expireDay) {
		this.expireDay = expireDay;
	}

	public String getMedicineDescription() {
		return medicineDescription;
	}

	public void setMedicineDescription(String medicineDescription) {
		this.medicineDescription = medicineDescription;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public BigDecimal getEmployeePkId() {
		return employeePkId;
	}

	public void setEmployeePkId(BigDecimal employeePkId) {
		this.employeePkId = employeePkId;
	}

	public String getName() {
		if(name == null) name = "";
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getDateString (Date date)
	{
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	public String getMeasurementName() {
		return measurementName;
	}

	public void setMeasurementName(String measurementName) {
		this.measurementName = measurementName;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public byte getD() {
		return d;
	}
	
	public void setD(byte d) {
		this.d = d;
	}
	
	public byte getE() {
		return e;
	}
	
	public void setE(byte e) {
		this.e = e;
	}
	
	public byte getM() {
		return m;
	}
	
	public void setM(byte m) {
		this.m = m;
	}
	
	public byte getN() {
		return n;
	}
	
	public void setN(byte n) {
		this.n = n;
	}

	public boolean isSelectM() {
		selectM = getM() == 1;
		return selectM;
	}

	public void setSelectM(boolean selectM) {
		setM((byte)(selectM ? 1 : 0));
		this.selectM = selectM;
	}

	public boolean isSelectD() {
		selectD = getD() == 1;
		return selectD;
	}

	public void setSelectD(boolean selectD) {
		setD((byte)(selectD ? 1 : 0));
		this.selectD = selectD;
	}

	public boolean isSelectE() {
		selectE = getE() == 1;
		return selectE;
	}

	public void setSelectE(boolean selectE) {
		setE((byte)(selectE ? 1 : 0));
		this.selectE = selectE;
	}

	public boolean isSelectN() {
		selectN = getN() == 1;
		return selectN;
	}

	public void setSelectN(boolean selectN) {
		setN((byte)(selectN ? 1 : 0));
		this.selectN = selectN;
	}
	
	public boolean isSelect() {
		return select;
	}
	
	public void setSelect(boolean select) {
		this.select = select;
	}
	
	public String getDisplayStr1() {
		return displayStr1;
	}
	
	public void setDisplayStr1(String displayStr1) {
		this.displayStr1 = displayStr1;
	}
	
	public String getDisplayStr2() {
		return displayStr2;
	}
	
	public void setDisplayStr2(String displayStr2) {
		this.displayStr2 = displayStr2;
	}
	
	public int getTime() {
		return time;
	}
	
	public void setTime(int time) {
		this.time = time;
	}
	
	public int getDay() {
		return day;
	}
	
	public void setDay(int day) {
		this.day = day;
	}
	
	public String getiName() {
		return iName;
	}
	
	public void setiName(String iName) {
		this.iName = iName;
	}
	
	public String getDrugDose() {
		return drugDose;
	}
	
	public void setDrugDose(String drugDose) {
		this.drugDose = drugDose;
	}
	
	public BigDecimal getMeasurementPkId() {
		return measurementPkId;
	}
	
	public void setMeasurementPkId(BigDecimal measurementPkId) {
		this.measurementPkId = measurementPkId;
	}
	
	public String getMedicineType() {
		return medicineType;
	}
	
	public void setMedicineType(String medicineType) {
		this.medicineType = medicineType;
	}
	
	public void calcRepeatCountByTimeDay(){
		if(getCalcDose() > 0 && getCalcDrugDose() > 0) {
			int t = 0;
			if(time > 0 && day > 0) t = time*day;
			t = t*getCalcDose();
			repeatCount = 0;
			while(t > 0){
				repeatCount++;
				t -= getCalcDrugDose();
			}
		}else {
			if(time > 0 && day > 0) repeatCount = time*day;
		}
	}
	
	public void calc2(){
		this.time = 0;
		if(this.selectM) this.time++;
		if(this.selectD) this.time++;
		if(this.selectE) this.time++;
		if(this.selectN) this.time++;
		calcRepeatCountByTimeDay();
		//calcMDEN();
		medicineDescription = "";
		if(this.selectM) medicineDescription = "Өглөө, ";
		if(this.selectD) medicineDescription += "Өдөр, ";
		if(this.selectE) medicineDescription += "Орой, ";
		if(this.selectN) medicineDescription += "Шөнө, ";
		medicineDescription += time + " удаа";
	}
	
	public void calcMDEN(){
		setSelectM(false);
		setSelectD(false);
		setSelectE(false);
		setSelectN(false);
		medicineDescription = "";
		if(time == 1) {
			setSelectM(true);
			setSelectD(false);
			setSelectE(false);
			setSelectN(false);
			medicineDescription = "Өглөө ";
		}
		if(time == 2) {
			setSelectM(true);
			setSelectD(false);
			setSelectE(true);
			setSelectN(false);
			medicineDescription = "Өглөө, Орой ";
		}
		if(time == 3) {
			setSelectM(true);
			setSelectD(true);
			setSelectE(true);
			setSelectN(false);
			medicineDescription = "Өглөө, Өдөр, Орой ";
		}
		if(time >= 4) {
			setSelectM(true);
			setSelectD(true);
			setSelectE(true);
			setSelectN(true);
			medicineDescription = "Өглөө, Өдөр, Орой, Шөнө ";
		}
		medicineDescription+= time + " удаа";
	}

	public View_ConstantMedicineType getConstantMedicineType() {
		return constantMedicineType;
	}

	public void setConstantMedicineType(View_ConstantMedicineType constantMedicineType) {
		this.constantMedicineType = constantMedicineType;}
	public int getCalcDrugDose() {
		return calcDrugDose;
	}

	public void setCalcDrugDose(int calcDrugDose) {
		this.calcDrugDose = calcDrugDose;
	}

	public int getCalcDose() {
		return calcDose;
	}

	public void setCalcDose(int calcDose) {
		this.calcDose = calcDose;
	}

	public String getCalcType() {
		return calcType;
	}

	public void setCalcType(String calcType) {
		this.calcType = calcType;
	}
	
	public BigDecimal getMedicineTypePkId() {
		return medicineTypePkId;
	}
	
	public void setMedicineTypePkId(BigDecimal medicineTypePkId) {
		this.medicineTypePkId = medicineTypePkId;
	}

	public int getBalanceNumber() {
		return balanceNumber;
	}

	public void setBalanceNumber(int balanceNumber) {
		this.balanceNumber = balanceNumber;
	}

	public int getInputNumber() {
		return inputNumber;
	}

	public void setInputNumber(int inputNumber) {
		this.inputNumber = inputNumber;
	}
	
	public void calculateBalance(){
		if(getRepeatCount()>0){
			balanceNumber  = getRepeatCount()-getDispenseCount();
		}
	}

	public int getDispenseCount() {
		return dispenseCount;
	}

	public void setDispenseCount(int dispenseCount) {
		this.dispenseCount = dispenseCount;
	}
	
	public BigDecimal getCustomerPkId() {
		return customerPkId;
	}
	
	public void setCustomerPkId(BigDecimal customerPkId) {
		this.customerPkId = customerPkId;
	}

	public String getSubOrganizationName() {
		return subOrganizationName;
	}

	public void setSubOrganizationName(String subOrganizationName) {
		this.subOrganizationName = subOrganizationName;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public Medicine getMedicine() {
		return medicine;
	}
	public void setMedicine(Medicine medicine) {
		this.medicine = medicine;
	}
	
	public Date getBeginDate() {
		if(beginDate == null) beginDate = new Date();
		return beginDate;
	}
	
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public byte getmType() {
		return mType;
	}
	
	public void setmType(byte mType) {
		this.mType = mType;
	}

	public byte getIsDispenseMedicne() {
		return isDispenseMedicne;
	}

	public void setIsDispenseMedicne(byte isDispenseMedicne) {
		this.isDispenseMedicne = isDispenseMedicne;
	}

	public boolean isDispenseMedicne() {
		dispenseMedicne = getIsDispenseMedicne()==1;
		if(getRepeatCount()==0)
			setDispenseMedicne(true);
		return dispenseMedicne;
	}

	public void setDispenseMedicne(boolean dispenseMedicne) {
		setIsDispenseMedicne((byte) (dispenseMedicne ? 1:0));
		this.dispenseMedicne = dispenseMedicne;
	}

	public String getCreatedDateStr() {
		createdDateStr = new SimpleDateFormat("yyyy-MM-dd").format(getCreatedDate());
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public String getDrinkType() {
		return drinkType;
	}

	public void setDrinkType(String drinkType) {
		this.drinkType = drinkType;
	}

	public int getTotal() {
		total = day * time;
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
	public Date getDateTime() {
		if(dateTime == null) dateTime = new Date();
		return dateTime;
	}
	
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public boolean isOut() {
		return out;
	}

	public void setOut(boolean out) {
		this.out = out;
	}
	
	public int getNursingStatus() {
		return nursingStatus;
	}
	
	public void setNursingStatus(int nursingStatus) {
		this.nursingStatus = nursingStatus;
	}
	
	public BigDecimal getInspectionDtlPkId() {
		return inspectionDtlPkId;
	}
	
	public void setInspectionDtlPkId(BigDecimal inspectionDtlPkId) {
		this.inspectionDtlPkId = inspectionDtlPkId;
	}
	
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getNursingStatusStyle() {
		nursingStatusStyle = "";
		if(nursingStatus == 1) {
			nursingStatusStyle = "background: black; color: white;";
		}
		if(nursingStatus == 2) {
			nursingStatusStyle = "background: green; color: white;";
		}
		if(nursingStatus == 3) {
			nursingStatusStyle = "background: red; color: white;";
		}
		return nursingStatusStyle;
	}
	
	public void setNursingStatusStyle(String nursingStatusStyle) {
		this.nursingStatusStyle = nursingStatusStyle;
	}	
	
	public int getWarningMaxDay() {
		return warningMaxDay;
	}
	
	public void setWarningMaxDay(int warningMaxDay) {
		this.warningMaxDay = warningMaxDay;
	}
	
	public boolean iswMaxDay() {
		return wMaxDay;
	}
	
	public void setwMaxDay(boolean wMaxDay) {
		this.wMaxDay = wMaxDay;
	}
	
	public String getPatientType() {
		return patientType;
	}
	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public int getIsDivide() {
		return isDivide;
	}

	public void setIsDivide(int isDivide) {
		this.isDivide = isDivide;
	}

	public String getUseMethod() {
		return useMethod;
	}

	public void setUseMethod(String useMethod) {
		this.useMethod = useMethod;
	}
	
	public int getWarningMedicine() {
		return warningMedicine;
	}
	
	public void setWarningMedicine(int warningMedicine) {
		this.warningMedicine = warningMedicine;
	}
	
	public String getWarningDose() {
		if(warningDose == null) warningDose = "";
		return warningDose;
	}
	
	public void setWarningDose(String warningDose) {
		this.warningDose = warningDose;
	}
	
	public String getWarningDoseMessage() {
		if(warningDoseMessage == null) warningDoseMessage = "";
		return warningDoseMessage;
	}
	
	public void setWarningDoseMessage(String warningDoseMessage) {
		this.warningDoseMessage = warningDoseMessage;
	}
	
	public int getWarningDoseOver() {
		return warningDoseOver;
	}
	
	public void setWarningDoseOver(int warningDoseOver) {
		this.warningDoseOver = warningDoseOver;
	}
	
	public String getwMaxMessage() {
		return wMaxMessage;
	}
	
	public void setwMaxMessage(String wMaxMessage) {
		this.wMaxMessage = wMaxMessage;
	}
}