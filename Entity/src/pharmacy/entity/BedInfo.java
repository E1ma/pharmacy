package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "BedInfo")
public class BedInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name = "RoomPkId")
	private BigDecimal roomPkId;
	
	@Column(name = "BedNo")
	private byte bedNo;
	
	
	@Column(name = "BedNumber")
	private String bedNumber;
	
	@Transient
	private int gender;
	
	@Transient 
	private String genderStr;

	@Transient 
	private String firstName;
	
	@Transient 
	private String lastName;
	
	@Transient 
	private InpatientHdr hdr;
	
	@Transient
	private List<BedInfo> bedInfos;
	
	public BedInfo(){
		super();
	}
	
	public BedInfo(BigDecimal pkId, byte bedNo){
		super();
		this.pkId = pkId;
		this.bedNo = bedNo;
	}
	
	public BedInfo(BigDecimal pkId,String bedNo,int gender){
		super();
		this.pkId = pkId;
		this.bedNumber = bedNo;
		this.gender =gender;
	}
	
	
	public BedInfo(BigDecimal pkId,String bedNo,int gender,String firstname,String lastName,InpatientHdr  hdr){
		super();
		this.pkId = pkId;
		this.bedNumber = bedNo;
		this.gender =gender;
		this.firstName =firstname;
		this.lastName=lastName;
		this.hdr =hdr;
	}
	
	public BedInfo(BigDecimal pkId){
		super();
		this.pkId = pkId;
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getRoomPkId() {
		return roomPkId;
	}

	public void setRoomPkId(BigDecimal roomPkId) {
		this.roomPkId = roomPkId;
	}

	public byte getBedNo() {
		return bedNo;
	}

	public void setBedNo(byte bedNo) {
		this.bedNo = bedNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getGender() {
		
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public List<BedInfo> getBedInfos() {
		if(bedInfos==null)
			bedInfos = new  ArrayList<>();
		return bedInfos;
	}

	public void setBedInfos(List<BedInfo> bedInfos) {
		this.bedInfos = bedInfos;
	}

	public String getGenderStr() {
		if(getGender()==1){
			genderStr ="Эр";
		} else  if(getGender()==0)
			genderStr ="Эм";
		else genderStr =" Хоосон";
		return genderStr;
	}

	public void setGenderStr(String genderStr) {
		this.genderStr = genderStr;
	}

	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String bedInfoClass() {
		String ret="";
		if(this.gender==2) {
			ret="label label-primary";
		} else ret ="label label-danger";
		
		return ret;
	}

	public String getBedNumber() {
		return bedNumber;
	}

	public void setBedNumber(String bedNumber) {
		this.bedNumber = bedNumber;
	}

	public InpatientHdr getHdr() {
		return hdr;
	}

	public void setHdr(InpatientHdr hdr) {
		this.hdr = hdr;
	}
}
	
