package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="InjectionPrice")
public class InjectionPrice implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="PkId",length=18,nullable=false)
	private BigDecimal pkId;
	
	@Column(name="Price",length=18,nullable=false)
	private BigDecimal price;
	
	@Column(name="InjectionPkId",length=18,nullable=false)
	private BigDecimal injectionPkId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date  createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date  updatedDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BeginDate")
	private Date  beginDate;
	
	
	@Column(name="CreatedBy",length=18,nullable=false)
	private BigDecimal createdBy;
	
	@Column(name="UpdatedBy",length=18,nullable=false)
	private BigDecimal updatedBy;
	
	/* 1 Бол даатгалтай */
	@Column(name="IsHasInsurance")
	private byte isHasInsurance;
	
	@Column(name="InsurancePrice",length=18,nullable=false)
	private BigDecimal insurancePrice;
	
	@Column(name = "InpatientPrice")
	private BigDecimal inpatientPrice;
	
	public InjectionPrice(){
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getInjectionPkId() {
		return injectionPkId;
	}

	public void setInjectionPkId(BigDecimal injectionPkId) {
		this.injectionPkId = injectionPkId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}



	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public byte getIsHasInsurance() {
		return isHasInsurance;
	}

	public void setIsHasInsurance(byte isHasInsurance) {
		this.isHasInsurance = isHasInsurance;
	}

	public BigDecimal getInsurancePrice() {
		return insurancePrice;
	}

	public void setInsurancePrice(BigDecimal insurancePrice) {
		this.insurancePrice = insurancePrice;
	}

	public BigDecimal getInpatientPrice() {
		return inpatientPrice;
	}

	public void setInpatientPrice(BigDecimal inpatientPrice) {
		this.inpatientPrice = inpatientPrice;
	}	
}