package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "CrmTreatmentCondition")
public class CrmTreatmentCondition implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name="Name")
	private String name;
	
	@Column(name="ConditionType")
	private String conditionType;
	
	@Column(name = "IsActive")
	private int isActive;
	
	@Column(name="Description")
	private String description;
	
	@Column(name="Gender")
	private String gender;
	
	@Column(name="LifeCategory")
	private String lifeCategory;
	
	@Column(name="BloodType")
	private String bloodType;
	
	@Column(name="InsuranceInfo")
	private String insuranceInfo;
	
	@Column(name="FirstTreatment")
	private int firstTreatment;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;

	@Column(name = "CreatedBy")
	private BigDecimal createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;

	@Column(name = "UpdatedBy")
	private BigDecimal updatedBy;
	
	@Transient
	private String createdUser;
	
	@Transient
	private String activeStr;
	
	@Transient
	private String status;
	
	@Transient
	private String conditionTypeStr;
	
	@Transient
	private boolean firstTreat;
	
	@Transient
	private boolean activeBool;

	public CrmTreatmentCondition()
	{
		super();
	}
	
	//LogicCrm -> getCrmTreatmentList
	public CrmTreatmentCondition(BigDecimal pkId, String name, String conditionType, int isActive, String createdUser, Date createdDate,
			String description, BigDecimal createdBy, BigDecimal updatedBy, Date updatedDate, int firstTreatment, String insuranceInfo, 
			String bloodType, String lifeCategory, String gender)
	{
		super();
		this.name = name;
		this.conditionType = conditionType;
		this.isActive = isActive;
		this.createdUser = createdUser;
		this.createdDate = createdDate;
		this.description = description;
		this.pkId = pkId;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
		this.firstTreatment = firstTreatment;
		this.insuranceInfo = insuranceInfo;
		this.bloodType = bloodType;
		this.lifeCategory = lifeCategory;
		this.gender = gender;
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getConditionType() {
		return conditionType;
	}

	public void setConditionType(String conditionType) {
		this.conditionType = conditionType;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLifeCategory() {
		return lifeCategory;
	}

	public void setLifeCategory(String lifeCategory) {
		this.lifeCategory = lifeCategory;
	}

	public String getBloodType() {
		return bloodType;
	}

	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}

	public String getInsuranceInfo() {
		return insuranceInfo;
	}

	public void setInsuranceInfo(String insuranceInfo) {
		this.insuranceInfo = insuranceInfo;
	}

	public int getFirstTreatment() {
		return firstTreatment;
	}

	public void setFirstTreatment(int firstTreatment) {
		this.firstTreatment = firstTreatment;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getActiveStr() {
		activeStr = isActive == 1 ? "Тийм" : "Үгүй";
		return activeStr;
	}

	public void setActiveStr(String activeStr) {
		this.activeStr = activeStr;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isFirstTreat() {
		firstTreat = firstTreatment == 1;
		return firstTreat;
	}

	public void setFirstTreat(boolean firstTreat) {
		firstTreatment = firstTreat ? 1 : 0;
		this.firstTreat = firstTreat;
	}

	public String getConditionTypeStr() {
		if("TreatmentDay".equals(conditionType))
			conditionTypeStr = "Эмчилгээ хийсэн өдөр";
		if("InpatientDay".equals(conditionType))
			conditionTypeStr = "Эмнэлэгт хэвтэх өдөр";
		if("DischargeDay".equals(conditionType))
			conditionTypeStr = "Эмнэлгээс гарсан өдөр";
		if("InpatientOrderDay".equals(conditionType))
			conditionTypeStr = "Эмнэлэгт хэвтэх захилгасан өгсөн өдөр";
		if("ExamDay".equals(conditionType))
			conditionTypeStr = "Шинжилгээ хийсэн өдөр";
		if("DiagnoseDay".equals(conditionType))
			conditionTypeStr = "Онош тавьсан өдөр";
		if("SurgeryDay".equals(conditionType))
			conditionTypeStr = "Мэс заслын өдөр";	
		return conditionTypeStr;
	}

	public void setConditionTypeStr(String conditionTypeStr) {
		this.conditionTypeStr = conditionTypeStr;
	}

	public boolean isActiveBool() {
		activeBool = isActive == 1;
		return activeBool;
	}

	public void setActiveBool(boolean activeBool) {
		isActive = activeBool ? 1 : 0;
		this.activeBool = activeBool;
	}
}