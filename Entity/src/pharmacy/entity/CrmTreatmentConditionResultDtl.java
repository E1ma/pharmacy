package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "CrmTreatmentConditionResultDtl")
public class CrmTreatmentConditionResultDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name="ResultPkId")
	private BigDecimal resultPkId;
	
	@Column(name="CustomerPkId")
	private BigDecimal customerPkId;
	
	@Column(name="EmployeePkId")
	private BigDecimal employeePkId;
	
	@Column(name="OrderDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderDate;
	
	@Column(name="SendDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendDate;
	
	@Column(name="IsFirst")
	private String isFirst;
	
	@Column(name="OrderId")
	private String orderId;
	
	@Column(name="ExaminationPkId")
	private BigDecimal examinationPkId;
	
	@Column(name="SurgeryPkId")
	private BigDecimal surgeryPkId;
	
	@Transient
	private String customerName;
	
	@Transient
	private String cardNumber;
	
	@Transient
	private String employeeName;
	
	@Transient
	private String surgeryName;
	
	@Transient
	private String examinationName;
	
	@Transient
	private String subOrganizationName;
	
	public CrmTreatmentConditionResultDtl()
	{
		super();
	}
	
	//LogicCrm -> getResultDtlList
	public CrmTreatmentConditionResultDtl(BigDecimal pkId, BigDecimal resultPkId, BigDecimal customerPkId, String customerName, 
			String cardNumber, BigDecimal employeePkId, String employeeName, String subOrgaName, Date sendDate, String orderId, 
			String isFirst, Date orderDate)
	{
		super();
		this.pkId = pkId;
		this.resultPkId = resultPkId;
		this.customerPkId = customerPkId;
		this.customerName = customerName;
		this.cardNumber = cardNumber;
		this.employeePkId = employeePkId;
		this.employeeName = employeeName;
		this.subOrganizationName = subOrgaName;
		this.sendDate = sendDate;
		this.orderId = orderId;
		this.isFirst = isFirst;
		this.orderDate = orderDate;
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getResultPkId() {
		return resultPkId;
	}

	public void setResultPkId(BigDecimal resultPkId) {
		this.resultPkId = resultPkId;
	}

	public BigDecimal getCustomerPkId() {
		return customerPkId;
	}

	public void setCustomerPkId(BigDecimal customerPkId) {
		this.customerPkId = customerPkId;
	}

	public BigDecimal getEmployeePkId() {
		return employeePkId;
	}

	public void setEmployeePkId(BigDecimal employeePkId) {
		this.employeePkId = employeePkId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getIsFirst() {
		return isFirst;
	}

	public void setIsFirst(String isFirst) {
		this.isFirst = isFirst;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public BigDecimal getExaminationPkId() {
		return examinationPkId;
	}

	public void setExaminationPkId(BigDecimal examinationPkId) {
		this.examinationPkId = examinationPkId;
	}

	public BigDecimal getSurgeryPkId() {
		return surgeryPkId;
	}

	public void setSurgeryPkId(BigDecimal surgeryPkId) {
		this.surgeryPkId = surgeryPkId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getSubOrganizationName() {
		return subOrganizationName;
	}

	public void setSubOrganizationName(String subOrganizationName) {
		this.subOrganizationName = subOrganizationName;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public String getSurgeryName() {
		return surgeryName;
	}

	public void setSurgeryName(String surgeryName) {
		this.surgeryName = surgeryName;
	}

	public String getExaminationName() {
		return examinationName;
	}

	public void setExaminationName(String examinationName) {
		this.examinationName = examinationName;
	}
}