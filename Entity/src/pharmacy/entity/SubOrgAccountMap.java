package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "SubOrgAccountMap")
public class SubOrgAccountMap implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;

	@Column(name = "SubOrganizationPkId")
	private BigDecimal subOrganizationPkId;

	@Column(name = "AccountType")
	private String accountType;

	@Column(name = "AccountId")
	private String accountId;
	
	@Column(name = "AccountName")
	private String accountName;
	
	@Transient
	private String status;
	
	@Transient
	private boolean selected;
	
	@Transient
	private String subOrganizationName;

	public SubOrgAccountMap() {
		super();
	}
	
	public SubOrgAccountMap(BigDecimal pkId, BigDecimal subOrganizationPkId, String accountType,
			String accountId, String accountName, String subOrganizationName) {
		super();
		this.pkId = pkId;
		this.subOrganizationPkId = subOrganizationPkId;
		this.accountType = accountType;
		this.accountId = accountId;
		this.accountName = accountName;
		this.subOrganizationName = subOrganizationName;
	}
	
	//Tmp List
	public SubOrgAccountMap(String accountType, String accountId, String accountName) {
		super();
		this.accountType = accountType;
		this.accountId = accountId;
		this.accountName = accountName;
		this.selected = false;
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getSubOrganizationPkId() {
		return subOrganizationPkId;
	}

	public void setSubOrganizationPkId(BigDecimal subOrganizationPkId) {
		this.subOrganizationPkId = subOrganizationPkId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubOrganizationName() {
		return subOrganizationName;
	}

	public void setSubOrganizationName(String subOrganizationName) {
		this.subOrganizationName = subOrganizationName;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}