package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MedicineWarningMap")
public class MedicineWarningMap  implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name="MedicinePkId")
	private BigDecimal medicinePkId;
	
	@Column(name="MedicineWarningPkId")
	private BigDecimal medicineWarningPkId;
	
	public MedicineWarningMap(){
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getMedicinePkId() {
		return medicinePkId;
	}

	public void setMedicinePkId(BigDecimal medicinePkId) {
		this.medicinePkId = medicinePkId;
	}

	public BigDecimal getMedicineWarningPkId() {
		return medicineWarningPkId;
	}

	public void setMedicineWarningPkId(BigDecimal medicineWarningPkId) {
		this.medicineWarningPkId = medicineWarningPkId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
