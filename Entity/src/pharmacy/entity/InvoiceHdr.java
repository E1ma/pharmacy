package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="InvoiceHdr")
public class InvoiceHdr  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PkId",length=18,nullable=false)
	private BigDecimal pkId;
	
	@Column(name="CustomerPkId")
	private BigDecimal customerPkId;
	
	@Column(name="SubOrganizationPkId")
	private BigDecimal subOrganizationPkId;
	
	@Column(name="HdrPkId")
	private BigDecimal hdrPkId;
	
	@Column(name="EmployeePkId")
	private BigDecimal employeePkId;
	
	@Column(name="NursingPkId")
	private BigDecimal nursingPkId;
	
	@Column(name="CreatedBy")
	private BigDecimal createdBy;
	
	@Column(name="InvType")
	private String invType;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="InvDate")
	private Date invDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	public  InvoiceHdr() {
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getCustomerPkId() {
		return customerPkId;
	}

	public void setCustomerPkId(BigDecimal customerPkId) {
		this.customerPkId = customerPkId;
	}

	public BigDecimal getSubOrganizationPkId() {
		return subOrganizationPkId;
	}

	public void setSubOrganizationPkId(BigDecimal subOrganizationPkId) {
		this.subOrganizationPkId = subOrganizationPkId;
	}

	

	public BigDecimal getHdrPkId() {
		return hdrPkId;
	}

	public void setHdrPkId(BigDecimal hdrPkId) {
		this.hdrPkId = hdrPkId;
	}

	public BigDecimal getEmployeePkId() {
		return employeePkId;
	}

	public void setEmployeePkId(BigDecimal employeePkId) {
		this.employeePkId = employeePkId;
	}

	public BigDecimal getNursingPkId() {
		return nursingPkId;
	}

	public void setNursingPkId(BigDecimal nursingPkId) {
		this.nursingPkId = nursingPkId;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public String getInvType() {
		return invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	public Date getInvDate() {
		return invDate;
	}

	public void setInvDate(Date invDate) {
		this.invDate = invDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
