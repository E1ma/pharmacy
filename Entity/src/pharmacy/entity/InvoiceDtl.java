package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity
@Table(name="InvoiceDtl")
public class InvoiceDtl  implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PkId" , nullable=false,length=18)
	private BigDecimal pkId;
	
	@Column(name="HdrPkId" , nullable=false,length=18)
	private BigDecimal hdrPkId; 
	
	@Column(name="ItemPkId" , nullable=false,length=18)
	private BigDecimal itemPkId; 
	
	
	// emzuich  olgoson  too  
	@Column(name="Qty" , nullable=false,length=18)
	private BigDecimal qty; 
	
	
	// zahialsan too  
	@Column(name="OrderQty" , nullable=false,length=18)
	private BigDecimal orderQty; 
	
	@Column(name="PharmacistPkId" , nullable=false,length=18)
	private BigDecimal pharmacistPkId; 
	
	@Column(name="PrescribePkId" , nullable=false,length=18)
	private BigDecimal prescribePkId; 
	
	@Column(name="IsToDispense")
	private byte isToDispense; 
	
	@Column(name="IsPackage")
	private byte isPackage; 
	
	@Column(name="Description")
	private byte description; 
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="InvDate")
	private Date invDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	@Column(name="UpdatedBy" )
	private BigDecimal updatedBy;
	
	@Column(name="ItemDescription" )
	private String itemDescription;
	
	@Column(name="CreatedBy" , nullable=false,length=18)
	private BigDecimal createdBy;
	
	@Transient
	private String status;
	
	
	@Transient
	private boolean dispenseCheck;
	
	@Transient
	private boolean packageCheck;
	
	public InvoiceDtl() {
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getHdrPkId() {
		return hdrPkId;
	}

	public void setHdrPkId(BigDecimal hdrPkId) {
		this.hdrPkId = hdrPkId;
	}

	public BigDecimal getItemPkId() {
		return itemPkId;
	}

	public void setItemPkId(BigDecimal itemPkId) {
		this.itemPkId = itemPkId;
	}

	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public BigDecimal getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(BigDecimal orderQty) {
		this.orderQty = orderQty;
	}

	public Date getInvDate() {
		return invDate;
	}

	public void setInvDate(Date invDate) {
		this.invDate = invDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public byte getIsToDispense() {
		return isToDispense;
	}

	public void setIsToDispense(byte isToDispense) {
		this.isToDispense = isToDispense;
	}
	
	public boolean isDispenseCheck() {
		return isToDispense==1 ? true:false;
	}

	public void setDispenseCheck(boolean dispenseCheck) {
		this.isToDispense = (byte) (dispenseCheck==true ? 1:0);
	}

	public byte getDescription() {
		return description;
	}

	public void setDescription(byte description) {
		this.description = description;
	}

	public byte getIsPackage() {
		return isPackage;
	}

	public void setIsPackage(byte isPackage) {
		this.isPackage = isPackage;
	}

	public boolean isPackageCheck() {
		return isPackage==1 ? true:false;
	}

	public void setPackageCheck(boolean packageCheck) {
		this.isPackage = (byte)(packageCheck==true ? 1:0);
	}

	public BigDecimal getPharmacistPkId() {
		return pharmacistPkId;
	}

	public void setPharmacistPkId(BigDecimal pharmacistPkId) {
		this.pharmacistPkId = pharmacistPkId;
	}

	public BigDecimal getPrescribePkId() {
		return prescribePkId;
	}

	public void setPrescribePkId(BigDecimal prescribePkId) {
		this.prescribePkId = prescribePkId;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	
	
}
