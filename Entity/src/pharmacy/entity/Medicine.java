package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import pharmacy.businessentity.CustomerMedicineDeispense;

@Entity
@Table(name = "Medicine")
public class Medicine implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", nullable = false, length = 18)
	private BigDecimal pkId;

	@Column(name = "Id", unique = true)
	private String id;

	@Column(name = "Name")
	private String name;

	@Column(name = "IName")
	private String iName;

	@Column(name = "ATCPkId")
	private BigDecimal atcPkId;

	@Column(name = "TypePkId")
	private BigDecimal typePkId;

	@Column(name = "MeasurementPkId")
	private BigDecimal measurementPkId;

	@Column(name = "BioActive")
	private byte bioActive;
	/*1-A, 2-B, 3-C, 4-D, 5-X*/

	@Column(name = "MinAge")
	private int minAge;

	@Column(name = "MaxAge")
	private int maxAge;

	@Column(name = "WarningMessage")
	private String warningMessage;

	@Column(name = "DrugDose")
	private String drugDose;

	@Column(name = "DayDose")
	private String dayDose;

	@Column(name = "Dose")
	private String dose;

	@Column(name = "Description")
	private String description;

	@Column(name = "CalcDrugDose")
	private int calcDrugDose;

	@Column(name = "CalcDose")
	private int calcDose;

	@Column(name = "CalcType")
	private String calcType;

	@Column(name = "MedicineImpact")
	private int  medicineImpact;
	
	/* 0 Ã�Â±Ã�Â¾Ã�Â» Ã�Â¸Ã�Â´Ã‘ï¿½Ã�Â²Ã‘â€¦Ã‘â€šÃ‘ï¿½Ã�Â¹  1 Ã�Â±Ã�Â¾Ã�Â»  Ã�Â¸Ã�Â´Ã‘ï¿½Ã�Â²Ã‘â€¦Ã�Â³Ã’Â¯Ã�Â¹  */
	
	@Column(name = "ActiveMedicine")
	private byte  activeMedicine;
	
	@Column(name = "CreatedBy")
	private BigDecimal createdBy;

	@Column(name = "CreatedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "UpdatedBy")
	private BigDecimal updatedBy;

	@Column(name = "UpdatedDate")
	@Temporal(TemporalType.TIMESTAMP)
	Date updatedDate;
	
	@Column(name = "WarningPkId")
	private BigDecimal warningPkId;
	
	@Column(name = "IsDivide")
	private int isDivide;

	@Column(name = "MixName")
	private String mixName;
	
	@Column (name = "TraditionalBool")
	private int traditionalBool;
	
	@Column (name = "UsageType")
	private String usageType;
	
	@Column (name = "IsDiscountOut")
	private int isDiscountOut;
	
	@Column (name = "IsDiscountIn")
	private int isDiscountIn;
	
	@Column (name = "HasDR")
	private int hasDR;
	
	/* 1-Энгийн 2-Сэтгэц нөлөөт 3-Антибиотик*/
	@Column (name = "DrugPurpose")
	private int drugPurpose;
	
	
	@Transient
	private List<CustomerMedicineDeispense> pharmacyMedicineName;
	
	@Transient
	String status;

	@Transient
	String atcName;

	@Transient
	String typeName;

	@Transient
	String measurementName;

	@Transient
	String bioActiveName;

	@Transient
	boolean activeAllAge;

	@Transient
	String ageDimension;

	@Transient
	private BigDecimal price;

	@Transient
	private BigDecimal inpatientPrice;
	
	@Transient
	private Date usageDate;

	@Transient
	private boolean insurance;
	
	@Transient
	private boolean activeMedicineValue;
	
	@Transient
	private boolean traditionalBoolValue;
	
	@Transient
	private boolean divideMedicine;
	
	@Transient
	private boolean discountOut;
	
	@Transient
	private boolean discountIn;

	@Transient
	private BigDecimal insurancePrice;

	@Transient
	private byte   isHasInsurance;
	
	@Transient
	private String activeStatus;
	
	@Transient
	private String message;

	public Medicine() {
		super();
	}
	
	public Medicine(BigDecimal pkId, String name, String id, String iname){
		super();
		this.pkId = pkId;
		this.name = name;
		this.id = id;
		this.iName = iname;
	}
	
	public Medicine(BigDecimal pkId, String name, String id, String dose,String iName,
			BigDecimal price,String typeName,BigDecimal inpatientPrice) {
		super();
		this.pkId = pkId;
		this.name = name;
		this.id = id;
		this.dose = dose;
		this.iName = iName;
		this.price = price;
		this.typeName = typeName;
		this.inpatientPrice = inpatientPrice;
		
	}
	
	public Medicine(BigDecimal pkId, String id, String name, String iName,
			BigDecimal atcPkId, BigDecimal typePkId,
			BigDecimal measurementPkId, byte bioActive, int minAge, int maxAge,
			String warningMessage, String description, String atcName,
			String typeName, String measurementName) {
		super();
		this.pkId = pkId;
		this.id = id;
		this.name = name;
		this.iName = iName;
		this.atcPkId = atcPkId;
		this.typePkId = typePkId;
		this.measurementPkId = measurementPkId;
		this.bioActive = bioActive;
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.warningMessage = warningMessage;
		this.description = description;
		this.atcName = atcName;
		this.typeName = typeName;
		this.measurementName = measurementName;
		
	}
	
	public Medicine(BigDecimal pkId, String id, String name, String iName,
			BigDecimal atcPkId, BigDecimal typePkId,
			BigDecimal measurementPkId, byte bioActive, int minAge, int maxAge,
			String warningMessage, String description, String atcName,
			String typeName, String measurementName, String drugDose, String dayDose, String dose
			, int calcDose, int calcDrugDose, String calcType, BigDecimal price, int medicineImpact, Date usageDate, 
			byte  isHasInsurance, BigDecimal insprice, BigDecimal inpatientPrice, byte activeMedicine, int isDivide, String mixName,  int traditionalBool) {
		super();
		this.pkId = pkId;
		this.id = id;
		this.name = name;
		this.iName = iName;
		this.atcPkId = atcPkId;
		this.typePkId = typePkId;
		this.measurementPkId = measurementPkId;
		this.bioActive = bioActive;
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.warningMessage = warningMessage;
		this.description = description;
		this.atcName = atcName;
		this.typeName = typeName;
		this.measurementName = measurementName;
		this.drugDose = drugDose;
		this.dayDose = dayDose;
		this.dose = dose;
		this.calcDose = calcDose;
		this.calcDrugDose = calcDrugDose;
		this.calcType = calcType;
		this.price = price;
		this.medicineImpact = medicineImpact;
		this.usageDate = usageDate;
		this.isHasInsurance = isHasInsurance;
		this.insurancePrice = insprice;
		this.activeMedicine = activeMedicine;
		this.inpatientPrice = inpatientPrice;
		this.isDivide = isDivide;
		this.mixName = mixName;
		this.traditionalBool = traditionalBool;
	
	}
	
	public Medicine(BigDecimal pkId, String id, String name, String iName,
			BigDecimal atcPkId, BigDecimal typePkId,
			BigDecimal measurementPkId, byte bioActive, int minAge, int maxAge,
			String warningMessage, String description, String atcName,
			String typeName, String measurementName,BigDecimal price,BigDecimal inpatientPrice) {
		super();
		this.pkId = pkId;
		this.id = id;
		this.name = name;
		this.iName = iName;
		this.atcPkId = atcPkId;
		this.typePkId = typePkId;
		this.measurementPkId = measurementPkId;
		this.bioActive = bioActive;
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.warningMessage = warningMessage;
		this.description = description;
		this.atcName = atcName;
		this.typeName = typeName;
		this.measurementName = measurementName;
		this.price = price;
		this.inpatientPrice = inpatientPrice;
		
	}

	public Medicine(BigDecimal pkId, String id, String name, String iName,
			BigDecimal atcPkId, BigDecimal typePkId,
			BigDecimal measurementPkId, byte bioActive, int minAge, int maxAge,
			String warningMessage, String description, String atcName,
			String typeName, String measurementName, String drugDose, String dayDose, String dose
			, int calcDose, int calcDrugDose, String calcType) {
		super();
		this.pkId = pkId;
		this.id = id;
		this.name = name;
		this.iName = iName;
		this.atcPkId = atcPkId;
		this.typePkId = typePkId;
		this.measurementPkId = measurementPkId;
		this.bioActive = bioActive;
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.warningMessage = warningMessage;
		this.description = description;
		this.atcName = atcName;
		this.typeName = typeName;
		this.measurementName = measurementName;
		this.drugDose = drugDose;
		this.dayDose = dayDose;
		this.dose = dose;
		this.calcDose = calcDose;
		this.calcDrugDose = calcDrugDose;
		this.calcType = calcType;
		
	}
	
	//InfoLogic -> getMedicine
	public Medicine(BigDecimal pkId, String id, String name, String iName,
			BigDecimal atcPkId, BigDecimal typePkId,
			BigDecimal measurementPkId, byte bioActive, int minAge, int maxAge,
			String warningMessage, String description, String atcName,
			String typeName, String measurementName, String drugDose, String dayDose, String dose
			, int calcDose, int calcDrugDose, String calcType, BigDecimal price, int medicineImpact, Date usageDate, 
			byte  isHasInsurance, BigDecimal insprice, BigDecimal inpatientPrice, byte activeMedicine, int isDivide, String mixName,  
			int traditionalBool, int isDiscountOut, int isDiscountIn) {
		super();
		this.pkId = pkId;
		this.id = id;
		this.name = name;
		this.iName = iName;
		this.atcPkId = atcPkId;
		this.typePkId = typePkId;
		this.measurementPkId = measurementPkId;
		this.bioActive = bioActive;
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.warningMessage = warningMessage;
		this.description = description;
		this.atcName = atcName;
		this.typeName = typeName;
		this.measurementName = measurementName;
		this.drugDose = drugDose;
		this.dayDose = dayDose;
		this.dose = dose;
		this.calcDose = calcDose;
		this.calcDrugDose = calcDrugDose;
		this.calcType = calcType;
		this.price = price;
		this.medicineImpact = medicineImpact;
		this.usageDate = usageDate;
		this.isHasInsurance = isHasInsurance;
		this.insurancePrice = insprice;
		this.activeMedicine = activeMedicine;
		this.inpatientPrice = inpatientPrice;
		this.isDivide = isDivide;
		this.mixName = mixName;
		this.traditionalBool = traditionalBool;
		this.isDiscountOut = isDiscountOut;
		this.isDiscountIn = isDiscountIn;
	}
	
	public Medicine(BigDecimal pkId, String id, String name, String iName,
			BigDecimal atcPkId, BigDecimal typePkId,
			BigDecimal measurementPkId, byte bioActive, int minAge, int maxAge,
			String warningMessage, String description, String atcName,
			String typeName, String measurementName, String drugDose, String dayDose, String dose) {
		super();
		this.pkId = pkId;
		this.id = id;
		this.name = name;
		this.iName = iName;
		this.atcPkId = atcPkId;
		this.typePkId = typePkId;
		this.measurementPkId = measurementPkId;
		this.bioActive = bioActive;
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.warningMessage = warningMessage;
		this.description = description;
		this.atcName = atcName;
		this.typeName = typeName;
		this.measurementName = measurementName;
		this.drugDose = drugDose;
		this.dayDose = dayDose;
		this.dose = dose;
		
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getiName() {
		return iName;
	}

	public void setiName(String iName) {
		this.iName = iName;
	}

	public BigDecimal getAtcPkId() {
		return atcPkId;
	}

	public void setAtcPkId(BigDecimal atcPkId) {
		this.atcPkId = atcPkId;
	}

	public BigDecimal getTypePkId() {
		return typePkId;
	}

	public void setTypePkId(BigDecimal typePkId) {
		this.typePkId = typePkId;
	}

	public BigDecimal getMeasurementPkId() {
		return measurementPkId;
	}

	public void setMeasurementPkId(BigDecimal measurementPkId) {
		this.measurementPkId = measurementPkId;
	}

	public byte getBioActive() {
		return bioActive;
	}

	public void setBioActive(byte bioActive) {
		this.bioActive = bioActive;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public String getWarnigMessage() {
		return warningMessage;
	}

	public void setWarnigMessage(String warnigMessage) {
		this.warningMessage = warnigMessage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAtcName() {
		return atcName;
	}

	public void setAtcName(String atcName) {
		this.atcName = atcName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getMeasurementName() {
		return measurementName;
	}

	public void setMeasurementName(String measurementName) {
		this.measurementName = measurementName;
	}

	public String getBioActiveName() {
		if(getBioActive() == '1' || getBioActive() == 1)
			bioActiveName = "A";
		if(getBioActive() == '2' || getBioActive() == 2)
			bioActiveName = "B";
		if(getBioActive() == '3' || getBioActive() == 3)
			bioActiveName = "C";
		if(getBioActive() == '4' || getBioActive() == 4)
			bioActiveName = "D";
		if(getBioActive() == '5' || getBioActive() == 5)
			bioActiveName = "X";


		return bioActiveName;
	}

	public void setBioActiveName(String bioActiveName) {


		this.bioActiveName = bioActiveName;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public boolean isActiveAllAge() {
		if(getMaxAge() == 150 && getMinAge() == 0)
			return true;
		return activeAllAge;
	}

	public void setActiveAllAge(boolean activeAllAge) {
		this.activeAllAge = activeAllAge;
	}

	public String getWarningMessage() {
		return warningMessage;
	}

	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}

	public String getAgeDimension() {
		if(getMinAge()  == 0 && getMaxAge() == 150)
			ageDimension = "Ã�â€˜Ã’Â¯Ã‘â€¦ Ã�Â½Ã�Â°Ã‘ï¿½Ã�Â½Ã‘â€¹Ã‘â€¦Ã�Â°Ã�Â½Ã�Â´";
		if(getMaxAge() == 150 && getMinAge()>0)
			ageDimension = getMinAge() + " Ã�Â½Ã�Â°Ã‘ï¿½Ã�Â½Ã�Â°Ã�Â°Ã‘ï¿½ Ã�Â´Ã‘ï¿½Ã‘ï¿½Ã‘Ë†";
		if(getMinAge() == 0 && getMaxAge()<150)
			ageDimension = getMaxAge() + " Ã�Â½Ã�Â°Ã‘ï¿½Ã�Â½Ã�Â°Ã�Â°Ã‘ï¿½ Ã�Â´Ã�Â¾Ã�Â¾Ã‘Ë†";
		return ageDimension;
	}
	public void setAgeDimension(String ageDimension) {
		this.ageDimension = ageDimension;
	}

	public String getDayDose() {
		return dayDose;
	}

	public void setDayDose(String dayDose) {
		this.dayDose = dayDose;
	}

	public String getDrugDose() {
		return drugDose;
	}

	public void setDrugDose(String drugDose) {
		this.drugDose = drugDose;
	}

	public String getDose() {
		return dose;
	}

	public void setDose(String dose) {
		this.dose = dose;
	}

	public int getCalcDrugDose() {
		return calcDrugDose;
	}

	public void setCalcDrugDose(int calcDrugDose) {
		this.calcDrugDose = calcDrugDose;
	}

	public int getCalcDose() {
		return calcDose;
	}

	public void setCalcDose(int calcDose) {
		this.calcDose = calcDose;
	}

	public String getCalcType() {
		return calcType;
	}

	public void setCalcType(String calcType) {
		this.calcType = calcType;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getUsageDate() {
		if(usageDate == null)
			return new Date();
		return usageDate;
	}

	public void setUsageDate(Date usageDate) {
		this.usageDate = usageDate;
	}


	public BigDecimal getInsurancePrice() {
		return insurancePrice;
	}

	public void setInsurancePrice(BigDecimal insurancePrice) {
		this.insurancePrice = insurancePrice;
	}

	public int getMedicineImpact() {
		return medicineImpact;
	}

	public void setMedicineImpact(int medicineImpact) {
		this.medicineImpact = medicineImpact;
	}

	public byte getActiveMedicine() {
		return activeMedicine;
	}

	public void setActiveMedicine(byte activeMedicine) {
		this.activeMedicine = activeMedicine;
	}

	public boolean isActiveMedicineValue() {
		return this.activeMedicine == 1;
	}

	public void setActiveMedicineValue(boolean activeMedicineValue) {
		this.activeMedicine = (byte) (activeMedicineValue ? 1 : 0);
	}

	public byte getIsHasInsurance() {
		return isHasInsurance;
	}

	public void setIsHasInsurance(byte isHasInsurance) {
		this.isHasInsurance = isHasInsurance;
	}

	public boolean isInsurance() {
		return this.isHasInsurance == 1;
	}

	public void setInsurance(boolean insurance) {
		this.isHasInsurance = (byte) (insurance ? 1 : 0);
		if(isHasInsurance==0)
			setInsurancePrice(null);
	}

	public String getActiveStatus() {
		return this.activeMedicine == 1  ?  "Ã�Â¢Ã�Â°Ã‘ï¿½Ã�Â°Ã‘â‚¬Ã‘ï¿½Ã�Â°Ã�Â½" : "Ã�Â¢Ã�Â°Ã‘ï¿½Ã‘â‚¬Ã�Â°Ã�Â°Ã�Â³Ã’Â¯Ã�Â¹";
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}
	
	public BigDecimal getInpatientPrice() {
		return inpatientPrice;
	}

	public void setInpatientPrice(BigDecimal inpatientPrice) {
		this.inpatientPrice = inpatientPrice;
	}
	
	public BigDecimal getWarningPkId() {
		return warningPkId;
	}
	
	public void setWarningPkId(BigDecimal warningPkId) {
		this.warningPkId = warningPkId;
	}

	public int getIsDivide() {
		return isDivide;
	}

	public void setIsDivide(int isDivide) {
		this.isDivide = isDivide;
	}

	public boolean isDivideMedicine() {
		return this.isDivide == 1;
	}

	public void setDivideMedicine(boolean divideMedicine) {
		this.divideMedicine = divideMedicine;
		this.isDivide = divideMedicine ? 1 : 0;
	}
	
	public String getMixName() {
		return mixName;
	}

	public void setMixName(String mixName) {
		this.mixName = mixName;
	}

	
	public int getTraditionalBool() {
		return traditionalBool;
	}

	public void setTraditionalBool(byte traditionalBool) {
		this.traditionalBool = traditionalBool;
	}
	
	public boolean isTraditionalBoolValue() {
		return this.traditionalBool == 1;
	}

	public void setTraditionalValue(boolean traditionalBoolValue) {
		this.traditionalBool = (byte) (traditionalBoolValue ? 1 : 0);
	}
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public String getUsageType() {
		return usageType;
	}

	public void setUsageType(String usageType) {
		this.usageType = usageType;
	}

	public int getIsDiscountOut() {
		return isDiscountOut;
	}

	public void setIsDiscountOut(int isDiscountOut) {
		this.isDiscountOut = isDiscountOut;
	}

	public int getIsDiscountIn() {
		return isDiscountIn;
	}

	public void setIsDiscountIn(int isDiscountIn) {
		this.isDiscountIn = isDiscountIn;
	}

	public boolean isDiscountOut() {
		discountOut = isDiscountOut != 0;
		return discountOut;
	}

	public void setDiscountOut(boolean discountOut) {
		//this.isDiscountOut = discountOut ? 1 : 0;
		this.discountOut = discountOut;
	}

	public boolean isDiscountIn() {
		discountIn = isDiscountIn != 0;
		return discountIn;
	}

	public void setDiscountIn(boolean discountIn) {
		//this.isDiscountIn = discountIn ? 1 : 0;
		this.discountIn = discountIn;
	}

	public int getDrugPurpose() {
		return drugPurpose;
	}

	public void setDrugPurpose(int drugPurpose) {
		this.drugPurpose = drugPurpose;
	}

	public List<CustomerMedicineDeispense> getPharmacyMedicineName() {
		if(pharmacyMedicineName==null)
			pharmacyMedicineName =  new  ArrayList<>();
		return pharmacyMedicineName;
	}

	public void setPharmacyMedicineName(List<CustomerMedicineDeispense> pharmacyMedicineName) {
		this.pharmacyMedicineName = pharmacyMedicineName;
	}
	
	
	
}