package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="MedicinePrice")
public class MedicinePrice implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="PkId" ,length=18, nullable=false)
	private BigDecimal pkId;
	
	@Column(name="MedicinePkId")
	private BigDecimal  medicinePkId;

	@Column(name="IsHasInsurance")
	private byte   isHasInsurance;
	
	@Column(name="InsurancePrice")
	private BigDecimal  insurancePrice;
	
	@Column(name="Price")
	private BigDecimal  price;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BeginDate")
	private Date beginDate;

	@Column(name="CreatedBy")
	private BigDecimal createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreateDate")
	private Date createDate;
	
	@Column(name="UpdatedBy")
	private BigDecimal updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdateDate")
	private Date updateDate;
	
	@Column(name = "InpatientPrice")
	private BigDecimal inpatientPrice;
	
	@Transient
	private String iName;
	
	@Transient
	private String userName;
	
	public MedicinePrice(){
		super();
	}
	public MedicinePrice(BigDecimal medPkId,BigDecimal price,Date createDate,Date updateDate){
		this.medicinePkId=medPkId;
		this.price =  price;
		this.createDate = createDate;
		this.updateDate =updateDate;
	}
	
	public MedicinePrice(Date beginDate,Date updateDate,BigDecimal price,String iName,String username){
		this.beginDate = beginDate;
		this.updateDate =updateDate;
		this.price =  price;
		this.iName=iName;
		this.userName=username;
		
	}
	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getMedicinePkId() {
		return medicinePkId;
	}

	public void setMedicinePkId(BigDecimal medicinePkId) {
		this.medicinePkId = medicinePkId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getiName() {
		return iName;
	}
	public void setiName(String iName) {
		this.iName = iName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getDateConvert(Date d){
		SimpleDateFormat format =  new SimpleDateFormat("yyyy/MM/dd");
		return format.format(d);
	}

	public byte getIsHasInsurance() {
		return isHasInsurance;
	}
	public void setIsHasInsurance(byte isHasInsurance) {
		this.isHasInsurance = isHasInsurance;
	}
	
	public BigDecimal getInsurancePrice() {
		return insurancePrice;
	}
	public void setInsurancePrice(BigDecimal insurancePrice) {
		this.insurancePrice = insurancePrice;
	}
	public BigDecimal getInpatientPrice() {
		return inpatientPrice;
	}
	public void setInpatientPrice(BigDecimal inpatientPrice) {
		this.inpatientPrice = inpatientPrice;
	}	
}