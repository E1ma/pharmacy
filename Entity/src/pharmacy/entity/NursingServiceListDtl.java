package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "NursingServiceListDtl")
public class NursingServiceListDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PkId", length = 18, nullable = false)
	private BigDecimal pkId;
	
	@Column(name = "NursingServiceListPkId")
	private BigDecimal nursingServiceListPkId;
	
	@Column(name = "NursePkId")
	private BigDecimal nursePkId;
	
	/*
	 * 0 - 
	 * 1 - Өглөө
	 * 2 - Өдөр
	 * 3 - Орой
	 * 4 - Шөнө
	 * */
	@Column(name = "Time")
	private int time;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ServiceDate")
	private Date serviceDate;
	
	/*
	 * 0 - 
	 * 1 - Хэрэгжүүлсэн
	 * 2 - Зогсоосон
	 * 3 - Цуцалсан
	 * 4 - Татгалзсан
	 * */
	@Column(name = "ServiceStatus")
	private int serviceStatus;
	
	@Column(name = "Description")
	private String description;
	
	@Column(name = "Count")
	private int count;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "CreatedBy")
	private BigDecimal createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name = "UpdatedBy")
	private BigDecimal updatedBy;
	

	
	@Transient
	private String status;
	
	
	public NursingServiceListDtl(){
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getNursingServiceListPkId() {
		return nursingServiceListPkId;
	}

	public void setNursingServiceListPkId(BigDecimal nursingServiceListPkId) {
		this.nursingServiceListPkId = nursingServiceListPkId;
	}

	public BigDecimal getNursePkId() {
		return nursePkId;
	}

	public void setNursePkId(BigDecimal nursePkId) {
		this.nursePkId = nursePkId;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public Date getServiceDate() {
		if(serviceDate==null){
			serviceDate =  new Date();
		}
		return serviceDate;
	}
	
	@SuppressWarnings("deprecation")
	public void setServiceDate(Date serviceDate) {
		if(serviceDate==null){
			serviceDate =  new Date();
			Date date =  new Date();
			serviceDate.setYear(date.getYear());
			serviceDate.setMonth(date.getMonth());
			serviceDate.setDate(date.getDate());
		}
		Date date =  new Date();
		serviceDate.setYear(date.getYear());
		serviceDate.setMonth(date.getMonth());
		serviceDate.setDate(date.getDate());
		this.serviceDate = serviceDate;
	}

	public int getServiceStatus() {
		return serviceStatus;
	}

	public void setServiceStatus(int serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
