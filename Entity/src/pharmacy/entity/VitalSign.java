package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: VitalSign
 *
 */
@Entity
@Table(name="VitalSign")
public class VitalSign implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="PkId",nullable=false)
	private BigDecimal pkId;
	
	@Column(name="InpatientPkId")
	private BigDecimal inpatientPkId;
	
	@Column(name="CustomerPkId")
	private BigDecimal customerPkId;
	
	@Column(name="EmployeePkId")
	private BigDecimal employeePkId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="Date")
	private Date date;
	
	@Column(name="Systol")
	private BigDecimal systol;
	
	@Column(name="Diastol")
	private BigDecimal diastol;
	
	@Column(name="Temperature")
	private BigDecimal temperature;
	
	@Column(name="RespiratoryRate")
	private int respiratoryRate;
	
	@Column(name="OxygenSaturation")
	private int oxygenSaturation;
	
	@Column(name="Height")
	private BigDecimal height;
	
	@Column(name="Weigth")
	private BigDecimal weigth;
	
	@Column(name="BMI")
	private BigDecimal bmi;
	
	@Column(name="VisionRight")
	private BigDecimal visionRight;
	
	@Column(name="visionLeft")
	private BigDecimal visionLeft;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="CreatedBy")
	private BigDecimal createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	@Column(name="UpdatedBy")
	private BigDecimal updatedBy;
	
	@Column(name="Pulse")
	private BigDecimal pulse;
	
	@Column(name="VitalSignType")
	private String vitalSignType;
	
	@Transient
	private String status;
	
	@Transient
	private String dateStr;
	
	@Transient
	private  String nurseName;
	
	@Transient
	private List<VitalSign> vitalSignDates;
	
	public VitalSign() {
		super();
	}
	
	public VitalSign(Date date) {
		this.date=date;
	}
	
	public VitalSign(BigDecimal pkId , Date date , BigDecimal systol , BigDecimal diastol ,BigDecimal temperature ,int respiratoryRate, int oxygenSaturation , BigDecimal height, BigDecimal weigth, BigDecimal bmi, BigDecimal visionRight ,BigDecimal visionLeft ,Date createdDate , BigDecimal pulse, String nurseName){
		this.pkId=pkId;
		this.date=date;
		this.systol=systol;
		this.diastol=diastol;
		this.temperature=temperature;
		this.respiratoryRate=respiratoryRate;
		this.oxygenSaturation=oxygenSaturation;
		this.height=height;
		this.weigth=weigth;
		this.bmi=bmi;
		this.visionRight=visionRight;
		this.visionLeft=visionLeft;
		this.createdDate=createdDate;
		this.nurseName=nurseName;
		this.pulse=pulse;
	}
	
	/*   Pkid   */
	public BigDecimal getPkId() {
		return pkId;
	}
	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}
	
	
	/*   InspectionPkId   */
	public BigDecimal getInpatientPkId() {
		return inpatientPkId;
	}
	public void setInpatientPkId(BigDecimal inpatientPkId) {
		this.inpatientPkId = inpatientPkId;
	}
	
	
	/*   EmployeePkId   */
	public BigDecimal getEmployeePkId() {
		return employeePkId;
	}
	public void setEmployeePkId(BigDecimal employeePkId) {
		this.employeePkId = employeePkId;
	}
	
	
	/*   CustomerPkId   */
	public BigDecimal getCustomerPkId() {
		return customerPkId;
	}
	public void setCustomerPkId(BigDecimal customerPkId) {
		this.customerPkId = customerPkId;
	}
	
	
	/*   Date   */
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	/*   Sistol   */
	public BigDecimal getSystol() {
		return systol;
	}
	public void setSystol(BigDecimal sistol) {
		this.systol = sistol;
	}
	
	
	/*   Diastol   */
	public BigDecimal getDiastol() {
		return diastol;
	}
	public void setDiastol(BigDecimal diastol) {
		this.diastol = diastol;
	}
	
	
	/*   Temperature   */
	public BigDecimal getTemperature() {
		return temperature;
	}
	public void setTemperature(BigDecimal temperature) {
		this.temperature = temperature;
	}
	
	
	/*   RespiratoryRate   */
	public int getRespiratoryRate() {
		return respiratoryRate;
	}
	public void setRespiratoryRate(int respiratoryRate) {
		this.respiratoryRate = respiratoryRate;
	}
	
	
	/*   OxygenSaturation   */
	public int getOxygenSaturation() {
		return oxygenSaturation;
	}
	public void setOxygenSaturation(int oxygenSaturation) {
		this.oxygenSaturation = oxygenSaturation;
	}
	
	
	/*   height   */
	public BigDecimal getHeight() {
		return height;
	}
	public void setHeight(BigDecimal height) {
		this.height = height;
	}
	
	
	/*   Weigth   */
	public BigDecimal getWeigth() {
		return weigth;
	}
	public void setWeigth(BigDecimal weigth) {
		this.weigth = weigth;
	}
	
	
	/*   Bmi   */
	public BigDecimal getBmi() {
		return bmi;
	}
	public void setBmi(BigDecimal bmi) {
		this.bmi = bmi;
	}
	
	
	/*   VisionRight   */
	public BigDecimal getVisionRight() {
		return visionRight;
	}
	public void setVisionRight(BigDecimal visionRight) {
		this.visionRight = visionRight;
	}
	
	
	/*   VisionLeft   */
	public BigDecimal getVisionLeft() {
		return visionLeft;
	}
	public void setVisionLeft(BigDecimal visionLeft) {
		this.visionLeft = visionLeft;
	}
	
	
	/*   CreatedDate   */
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	
	/*   CreatedBy   */
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	
	
	/*   updatedDate   */
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	/*   UpdatedBy   */
	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	/*   Status   */
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	/*   Date  */
	public String getDateStr() {
		SimpleDateFormat temp = new SimpleDateFormat("yyyy-MM-dd");
		dateStr = temp.format(getDate());
		return dateStr;
	}
	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}
	
	
	/*   Pulse   */
	public BigDecimal getPulse() {
		return pulse;
	}
	public void setPulse(BigDecimal pulse) {
		this.pulse = pulse;
	}

	
	/*   Nursename   */
	public String getNurseName() {
		return nurseName;
	}
	public void setNurseName(String nurseName) {
		this.nurseName = nurseName;
	}
	public List<VitalSign> getVitalSignDates() {
		if(vitalSignDates==null)
			vitalSignDates = new  ArrayList<>();
		return vitalSignDates;
	}
	public void setVitalSignDates(List<VitalSign> vitalSignDates) {
		this.vitalSignDates = vitalSignDates;
	}

	public String getVitalSignType() {
		return vitalSignType;
	}

	public void setVitalSignType(String vitalSignType) {
		this.vitalSignType = vitalSignType;
	}

	
	
	
}
