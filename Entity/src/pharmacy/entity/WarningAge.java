package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

@Entity
@Table(name="WarningAge")
public class WarningAge implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="PkId",nullable=false)
	private BigDecimal pkId;
	
	@Column(name="Type")
	private String type;
	
	@Column(name="TypePkId")
	private BigDecimal typePkId;
	
	@Column(name="Age")
	private int age;
	
	@Column(name="Message")
	private String message;
	
	@Transient
	private boolean select;
	
	public WarningAge(){
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getTypePkId() {
		return typePkId;
	}

	public void setTypePkId(BigDecimal typePkId) {
		this.typePkId = typePkId;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public boolean isSelect() {
		return select;
	}
	
	public void setSelect(boolean select) {
		this.select = select;
	}
	
	
}
