package pharmacy.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="WarningMedicineGroup")
public class WarningMedicineGroup implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="PkId",nullable=false)
	private BigDecimal pkId;
	
	@Column(name="MedicinePkId")
	private BigDecimal medicinePkId;
	
	@Column(name="GroupId")
	private String groupId;
	
	public WarningMedicineGroup() {
		super();
	}

	public BigDecimal getPkId() {
		return pkId;
	}

	public void setPkId(BigDecimal pkId) {
		this.pkId = pkId;
	}

	public BigDecimal getMedicinePkId() {
		return medicinePkId;
	}

	public void setMedicinePkId(BigDecimal medicinePkId) {
		medicinePkId = medicinePkId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		groupId = groupId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
