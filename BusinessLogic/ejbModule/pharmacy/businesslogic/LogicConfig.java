package pharmacy.businesslogic;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.naming.InitialContext;

import pharmacy.businessentity.*;
import pharmacy.businesslogic.interfaces.*;
import pharmacy.entity.*;
import base.*;

@Stateless(name = "LogicConfig", mappedName = "pharmacy.businesslogic.LogicConfig")
public class LogicConfig extends base.BaseData implements IConfigLogic, IConfigLogicLocal {
	@Resource
	SessionContext sessionContext;

	public LogicConfig() {
		super("Pharmacy");
	}
	
	@Override
	public List<SubOrgAccountMap> getAllAccount() throws Exception{
		List<Object[]> list = getProcList("EXEC GetInfo @merchantId = '001', @password = 'merchant00!', @objectId = 'Account'");
		List<SubOrgAccountMap> listAccount = new ArrayList<SubOrgAccountMap>();
		for (int i = 0; i < list.size(); i++) {
			SubOrgAccountMap dtl = new SubOrgAccountMap();
			dtl.setAccountId(list.get(i)[1].toString());
			dtl.setAccountName(list.get(i)[2].toString());
			dtl.setAccountType(list.get(i)[6].toString());
			if(list.get(i)[6].toString().contains("INV") || list.get(i)[6].toString().contains("COST"))
				listAccount.add(dtl);
		}
		return listAccount;
	}
	
	@Override
	public List<Item> getAllItem() throws Exception{
		List<Object[]> list = getProcList("EXEC GetInfo @merchantId = '001', @password = 'merchant00!', @objectId = 'Item'");
		List<Item> listItem = new ArrayList<Item>();
		for (int i = 0; i < list.size(); i++) {
			Item dtl = new Item();
			dtl.setId(list.get(i)[1].toString());
			dtl.setName(list.get(i)[3].toString());
			listItem.add(dtl);
		}
		return listItem;
	}
	
	public List<Object[]> getProcList(String execQuery) throws Exception{
		
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}

		try {
			connection = DriverManager.getConnection("jdbc:sqlserver://YSERVER:1399;databaseName=EmnelegAccounting","merchantUser", "@Merch4ntSL#");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		PreparedStatement statement;
		
		statement = connection.prepareStatement("USE EmnelegAccounting");
		statement.execute();
		
		statement = connection.prepareStatement(execQuery);
		List<Object[]> resultMap = new ArrayList<Object[]>();
		
		boolean results = statement.execute();
	   
	    while (results) {
	    	ResultSet rs = statement.getResultSet();
	    	ResultSetMetaData rsmd = rs.getMetaData();
	    	int columnsNumber = rsmd.getColumnCount();
	    	while (rs.next()) {
	    		List<String> strList = new ArrayList<String>();
	    		 for (int i = 1; i <= columnsNumber; i++) {
	    			 String columnValue = rs.getString(i);
	    			 //strList.add(rsmd.getColumnName(i)); boolean results = statement.execute();
	    			 strList.add(columnValue);
	    		 }
	    		 resultMap.add(strList.toArray());
	    	}
    		rs.close();
			results = statement.getMoreResults();
	    } 
		statement.close();
		return resultMap;
	}

	@Override
	public List<SubOrganization> getSubOrganizations() throws Exception{
		List<SubOrganization> listSubOrg = getAll(SubOrganization.class);
		return listSubOrg;
	}
	
	@Override
	public List<SubOrgAccountMap> getListData() throws Exception{
		String jpql = "SELECT new pharmacy.entity.SubOrgAccountMap(A.pkId, A.subOrganizationPkId, A.accountType,"
				+ "A.accountId, A.accountName, B.name) FROM SubOrgAccountMap A "
				+ "INNER JOIN SubOrganization B ON A.subOrganizationPkId = B.pkId ";
		return getByQuery(SubOrgAccountMap.class, jpql, null);
	}
	
	@Override
	public void saveAccountConfig(SubOrgAccountMap map, List<SubOrgAccountMap> listAccountMap) throws Exception
	{
		if(map.getStatus().equals(Tool.MODIFIED) || map.getStatus().equals(Tool.DELETE))
		{
			if(map.getStatus().equals(Tool.MODIFIED))
				deleteByAnyField(SubOrgAccountMap.class, "subOrganizationPkId", map.getSubOrganizationPkId());
			else
				deleteByAnyField(SubOrgAccountMap.class, "pkId", map.getPkId());
		}
		if(map.getStatus().equals(Tool.ADDED) || map.getStatus().equals(Tool.MODIFIED))
		{
			setEntityManager(null);
			BigDecimal newPkId = Tools.newPkId();
			for (SubOrgAccountMap tmp : listAccountMap) {
				if(tmp.isSelected())
				{
					newPkId = newPkId.add(BigDecimal.ONE);
					tmp.setSubOrganizationPkId(map.getSubOrganizationPkId());
					tmp.setPkId(newPkId);
					insert(tmp);
				}
			}
		}
	}
}