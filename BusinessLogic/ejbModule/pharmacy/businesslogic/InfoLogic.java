package pharmacy.businesslogic;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import base.*;
import pharmacy.entity.*;
import pharmacy.businesslogic.interfaces.IInfoLogicLocal;
import pharmacy.businessentity.Tool;
import pharmacy.businessentity.CustomerMedicineDeispense;
import pharmacy.businessentity.LoggedUser;

@Stateless(name = "InfoLogic", mappedName = "pharmacy.businesslogic.InfoLogic")
public class InfoLogic extends base.BaseData
implements pharmacy.businesslogic.interfaces.IInfoLogic, IInfoLogicLocal {

	@Resource
	SessionContext sessionContext;

	public InfoLogic() {
		super("Pharmacy");
	}

	public void setDataBaseInfo() throws Exception {
		// dataBaseName = "pharmacy"
	}

	@Override
	public List<Item> getItemList() throws Exception{
		String jpql = "SELECT new pharmacy.entity.Item(A.pkId, A.id, A.name, A.entityPrice, A.priceUsageDate, A.measurementPkId, B.name) FROM Item A "
				+ "INNER JOIN Measurement B ON A.measurementPkId = B.pkId ";
		return getByQuery(Item.class, jpql, null);
	}

	@Override
	public List<MedicinePrice> getMedicinePrice(BigDecimal pkId) throws Exception {
		// TODO Auto-generated method stub
		CustomHashMap parameters = new CustomHashMap();
		StringBuilder jpql = new StringBuilder();
		parameters.put("medPkId", pkId);
		jpql.append("SELECT  NEW pharmacy.entity.MedicinePrice(A.beginDate,A.updateDate,A.price,B.iName,C.name)  ");
		jpql.append("FROM MedicinePrice  A  ");
		jpql.append("INNER JOIN  Medicine B  ON A.medicinePkId = B.pkId ");
		jpql.append("INNER JOIN Users  C  ON A.updatedBy  = C.pkId ");
		jpql.append("WHERE  A.medicinePkId = :medPkId ");
		return getByQuery(MedicinePrice.class, jpql.toString(), parameters);
	}

	@Override
	public List<Customer> getCustomers(int first, int count, String sortField, String sortType,
			Map<String, String> filters) throws Exception {
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();

		jpql.append("SELECT a ");
		jpql.append("FROM Customer a ");

		if (!filters.isEmpty()) {
			jpql.append("WHERE ");
			for (Map.Entry<String, String> filter : filters.entrySet()) {
				parameters.put(filter.getKey(), "%" + filter.getValue() + "%");
				filter.getValue();
				jpql.append("a." + filter.getKey() + " LIKE :" + filter.getKey() + " AND ");
			}
			jpql.delete(jpql.length() - 4, jpql.length());
		}
		if (sortField != null && !sortField.isEmpty() && sortType != null && !sortType.isEmpty()) {
			jpql.append("ORDER BY a." + sortField + " " + sortType + " ");
		}
		return getByQuery(Customer.class, jpql.toString(), parameters, first, count);
	}

	@Override
	public long getCustomerCount(Map<String, String> filters) throws Exception {
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();

		jpql.append("SELECT COUNT(a.pkId) ");
		jpql.append("FROM Customer a ");

		if (!filters.isEmpty()) {
			jpql.append("WHERE ");
			for (Map.Entry<String, String> filter : filters.entrySet()) {
				parameters.put(filter.getKey(), "%" + filter.getValue() + "%");
				filter.getValue();
				jpql.append("a." + filter.getKey() + " LIKE :" + filter.getKey() + " AND ");
			}
			jpql.delete(jpql.length() - 4, jpql.length());
		}

		return (Long) getByQuerySingle(jpql.toString(), parameters);
	}

	@Override
	public List<SubOrganization> getListSubOrganization() throws Exception {

		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT DISTINCT a FROM SubOrganization a ");
		jpql.append("INNER JOIN Employee b on a.pkId = b.subOrganizationPkId ");

		List<SubOrganization> list = new ArrayList<SubOrganization>();

		list = getByQuery(SubOrganization.class, jpql.toString(), null);
		return list;
	}

	@Override
	public List<Menu> getMenus() throws Exception {

		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT a FROM Menu a ");

		List<Menu> list = getByQuery(Menu.class, jpql.toString(), null);
		return list;
	}


	@Override
	public List<Menu> getMenusFilter(LoggedUser loggedUser) throws Exception {

		StringBuilder jpql = new StringBuilder();
		BigDecimal bigDecimal = loggedUser.getUser().getPkId();
		CustomHashMap parameters = new CustomHashMap();

		parameters.put("bigDecimal", bigDecimal);
		jpql = new StringBuilder();
		jpql.append("SELECT b.menuPkId FROM UserRoleMap a ");
		jpql.append("INNER JOIN RoleMenuMap b ON a.rolePkId = b.rolePkId ");
		jpql.append("WHERE a.userPkId = :bigDecimal ");

		List<BigDecimal> bigDecimals = getByQuery(BigDecimal.class, jpql.toString(), parameters);
		parameters.put("list", bigDecimals);

		jpql = new StringBuilder();
		jpql.append("SELECT a FROM Menu a ");
		jpql.append("WHERE a.pkId NOT IN :list ");

		List<Menu> list = getByQuery(Menu.class, jpql.toString(), parameters);
		return list;
	}

	@Override
	public List<Menu> getMenus(LoggedUser loggedUser) throws Exception {

		StringBuilder jpql = new StringBuilder();
		BigDecimal userPkId = loggedUser.getUser().getPkId();
		CustomHashMap parameters = new CustomHashMap();

		parameters.put("bigDecimal", userPkId);
		jpql = new StringBuilder();
		jpql.append("SELECT new pharmacy.entity.Menu(b.menuPkId, c.sort) FROM UserRoleMap a ");
		jpql.append("INNER JOIN RoleMenuMap b ON a.rolePkId = b.rolePkId ");
		jpql.append("INNER JOIN Menu c ON b.menuPkId = c.pkId ");
		jpql.append("WHERE a.userPkId = :bigDecimal ");

		List<Menu> listPkId = getByQuery(Menu.class, jpql.toString(), parameters);
		List<BigDecimal> listMenuPkId = new ArrayList<BigDecimal>();
		List<Integer> listSort = new ArrayList<Integer>();
		for (Menu menu : listPkId) {
			listMenuPkId.add(menu.getPkId());
			if(!listSort.contains(menu.getSort()))
				listSort.add(menu.getSort());
		}
		parameters.put("list", listMenuPkId);
		parameters.put("sort", listSort);

		jpql = new StringBuilder();
		jpql.append("SELECT a FROM Menu a ");
		jpql.append("WHERE a.pkId IN ( ");
		jpql.append("SELECT MIN(b.pkId) FROM Menu b ");
		jpql.append("GROUP BY b.sort ");
		jpql.append(") AND a.pkId IN :list ");
		jpql.append("ORDER BY a.sort ");

		List<Menu> list = getByQuery(Menu.class, jpql.toString(), parameters);

		jpql = new StringBuilder();
		jpql.append("SELECT a FROM Menu a ");
		jpql.append("WHERE a.sort IN :sort ");
		List<Menu> subList = getByQuery(Menu.class, jpql.toString(), parameters);
		for (Menu menu : list)
			menu.setListSubMenu(new ArrayList<Menu>());
		for (Menu menu : list) {
			for (Menu tmp : subList) {
				if(menu.getSort() == tmp.getSort() && menu.getPkId().compareTo(tmp.getPkId()) != 0)
					menu.getListSubMenu().add(tmp);
			}
		}

		return list;
	}

	@Override
	public List<Menu> getListMenus(LoggedUser loggedUser) throws Exception {
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();

		parameters.put("userPkId", loggedUser.getUser().getPkId());
		jpql.append("SELECT DISTINCT a FROM Menu a ");
		jpql.append("INNER JOIN RoleMenuMap b ON b.menuPkId = a.pkId ");
		jpql.append("INNER JOIN UserRoleMap c ON c.rolePkId = b.rolePkId ");
		jpql.append("WHERE c.userPkId = :userPkId ");

		return getByQuery(Menu.class, jpql.toString(), parameters);
	}

	@Override
	public SystemConfig getLicense() throws Exception {

		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("name", "License");

		jpql.append("SELECT a FROM SystemConfig a WHERE a.name = :name ");
		List<SystemConfig> list = getByQuery(SystemConfig.class, jpql.toString(), parameters);
		if (list.size() == 0)
			return null;
		else
			return list.get(0);
	}

	@Override
	public void saveLicense(SystemConfig config) throws Exception {

		if (Tool.ADDED.equals(config.getStatus())) {
			config.setPkId(Tools.newPkId());
			insert(config);
		} else if (Tool.MODIFIED.equals(config.getStatus())) {
			update(config);
		} else if (Tool.DELETE.equals(config.getStatus())) {
			delete(config);
		}
	}

	@Override
	public List<Employee> getEmployeeInfo() throws Exception
	{
		String jpql = "SELECT new pharmacy.entity.Employee(A.pkId, A.firstName, B.name) FROM Employee A "
				+ "INNER JOIN SubOrganization B ON A.subOrganizationPkId = B.pkId ";
		return getByQuery(Employee.class, jpql, null);
	}


	@Override
	public List<Medicine> getMedicine(BigDecimal atcPkId, LoggedUser lu, String filterKey) throws Exception {
		List<Medicine> list = new ArrayList<Medicine>();
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("atcPkId", atcPkId);
		parameters.put("filter", "%" + filterKey + "%");
		jpql.append(
				"SELECT NEW pharmacy.entity.Medicine(a.pkId, a.id, a.name, a.iName, a.atcPkId, a.typePkId, a.measurementPkId, a.bioActive, a.minAge, a.maxAge, a.warningMessage, a.description, b.nameMn, c.name, d.name, a.drugDose, a.dayDose, a.dose, a.calcDose, a.calcDrugDose, a.calcType,f.price,a.medicineImpact,f.beginDate,f.isHasInsurance,f.insurancePrice,f.inpatientPrice,a.activeMedicine, a.isDivide ,a.mixName, a.traditionalBool) ");
		jpql.append("FROM Medicine a ");
		jpql.append("LEFT JOIN View_ConstantATC b ON b.pkId = a.atcPkId ");
		jpql.append("LEFT JOIN View_ConstantMedicineType c ON c.pkId = a.typePkId ");
		jpql.append("LEFT JOIN Measurement d ON d.pkId = a.measurementPkId ");
		jpql.append("LEFT JOIN VIEW_MedicinePrice f  ON a.pkId = f.medicinePkId ");
		jpql.append("WHERE (a.name LIKE :filter OR a.iName like :filter ) ");
		jpql.append(" ");
		jpql.append(" ");
		if (atcPkId.compareTo(BigDecimal.ZERO) != 0) {
			jpql.append("AND a.atcPkId =:atcPkId ");
		}
		list = getByQuery(Medicine.class, jpql.toString(), parameters);
		return list;
	}

	public void saveMedicine(Medicine medicine, boolean notUsedTogetherMedicineListCheck, List<Medicine> notUsedTogetherMedicineList, boolean pregnantWarningCheck, PregnantWarning pregnantWarning, boolean warningAgeCheck, WarningAge warningAge, boolean warningMedicineMaxDayCheck, WarningMedicineMaxDay warningMedicineMaxDay, boolean warningMedicineDoseCheck, WarningMedicineDose warningMedicineDose, boolean notUseGroupCheck, List<String> notUseGroup, LoggedUser lu, Date usageDate) throws Exception{
		BigDecimal pkId = Tools.newPkId();
		if (medicine.getStatus().equals(Tool.ADDED)) {
			medicine.setCreatedBy(lu.getUser().getPkId());
			medicine.setCreatedDate(new Date());
			medicine.setUpdatedBy(lu.getUser().getPkId());
			medicine.setUpdatedDate(new Date());
			medicine.setPkId(Tools.newPkId());
			insert(medicine);
			MedicinePrice medPrice = new MedicinePrice();
			medPrice.setPkId(Tools.newPkId());
			medPrice.setMedicinePkId(medicine.getPkId());
			medPrice.setCreateDate(new Date());
			medPrice.setUpdateDate(new Date());
			medPrice.setPrice(medicine.getPrice());
			medPrice.setInpatientPrice(medicine.getInpatientPrice());
			medPrice.setInsurancePrice(medicine.getInsurancePrice());
			medPrice.setBeginDate(medicine.getUsageDate());
			medPrice.setCreatedBy(lu.getUser().getPkId());
			medPrice.setUpdatedBy(lu.getUser().getPkId());
			medPrice.setIsHasInsurance(medicine.getIsHasInsurance());
			insert(medPrice);

		} else if (medicine.getStatus().equals(Tool.MODIFIED)) {
			medicine.setUpdatedBy(lu.getUser().getPkId());
			medicine.setUpdatedDate(new Date());
			if(medicine.getUsageDate().compareTo(usageDate) != 0)
			{
				MedicinePrice medPrice = new MedicinePrice();
				medPrice.setPkId(Tools.newPkId());
				medPrice.setMedicinePkId(medicine.getPkId());
				medPrice.setCreateDate(new Date());
				medPrice.setUpdateDate(new Date());
				medPrice.setPrice(medicine.getPrice());
				medPrice.setInpatientPrice(medicine.getInpatientPrice());
				medPrice.setIsHasInsurance(medicine.getIsHasInsurance());
				medPrice.setCreatedBy(lu.getUser().getPkId());
				medPrice.setUpdatedBy(lu.getUser().getPkId());
				medPrice.setBeginDate(medicine.getUsageDate());
				medPrice.setInsurancePrice(medicine.getInsurancePrice());
				insert(medPrice);
			}
			update(medicine);

			deleteByAnyField(PregnantWarning.class, "typePkId", medicine.getPkId());
			deleteByAnyField(WarningAge.class, "typePkId", medicine.getPkId());
			deleteByAnyField(WarningMedicineDose.class, "medicinePkId", medicine.getPkId());
			deleteByAnyField(WarningMedicineMaxDay.class, "medicinePkId", medicine.getPkId());
			deleteByAnyField(NotUserTogether.class, "medicinePkId1", medicine.getPkId());
			deleteByAnyField(NotUserTogether.class, "medicinePkId2", medicine.getPkId());
			deleteByAnyField(WarningMedicineGroup.class, "medicinePkId", medicine.getPkId());
		}

		else if (medicine.getStatus().equals(Tool.DELETE)) {
			deleteByAnyField(MedicinePrice.class, "medicinePkId", medicine.getPkId());
			deleteByPkId(Medicine.class, medicine.getPkId());
		}

		else if (medicine.getStatus().equals(Tool.UNCHANGED)) {

		}

		if(warningMedicineDoseCheck) {
			pkId = pkId.add(BigDecimal.ONE);
			warningMedicineDose.setPkId(pkId);
			warningMedicineDose.setMedicinePkId(medicine.getPkId());
			insert(warningMedicineDose);
		}
		if(warningMedicineMaxDayCheck) {
			pkId = pkId.add(BigDecimal.ONE);
			warningMedicineMaxDay.setPkId(pkId);
			warningMedicineMaxDay.setMedicinePkId(medicine.getPkId());
			insert(warningMedicineMaxDay);
		}
		if(warningAgeCheck) {
			pkId = pkId.add(BigDecimal.ONE);
			warningAge.setPkId(pkId);
			warningAge.setTypePkId(medicine.getPkId());
			insert(warningAge);
		}
		if(pregnantWarningCheck) {
			pkId = pkId.add(BigDecimal.ONE);
			pregnantWarning.setPkId(pkId);
			pregnantWarning.setType("Medicine");
			pregnantWarning.setTypePkId(medicine.getPkId());
			insert(pregnantWarning);
		}
		if(notUseGroupCheck) {
			for (String string : notUseGroup) {
				pkId = pkId.add(BigDecimal.ONE);
				WarningMedicineGroup group = new WarningMedicineGroup();
				group.setPkId(pkId);
				group.setMedicinePkId(medicine.getPkId());
				group.setGroupId(string);
				insert(group);
			}
		}
		if(notUsedTogetherMedicineListCheck) {
			for (Medicine medicine2 : notUsedTogetherMedicineList) {
				NotUserTogether notUserTogether = new NotUserTogether();
				pkId = pkId.add(BigDecimal.ONE);
				notUserTogether.setPkId(pkId);
				notUserTogether.setMedicinePkId1(medicine.getPkId());
				notUserTogether.setMedicinePkId2(medicine2.getPkId());
				notUserTogether.setMessage(medicine2.getMessage());
				insert(notUserTogether);
			}
		}
	}

	@Override
	public void saveMedicine(Medicine medicine, LoggedUser lu) throws Exception {
		if (medicine.getStatus().equals(Tool.ADDED)) {
			medicine.setCreatedBy(lu.getUser().getPkId());
			medicine.setCreatedDate(new Date());
			medicine.setUpdatedBy(lu.getUser().getPkId());
			medicine.setUpdatedDate(new Date());
			medicine.setPkId(Tools.newPkId());
			insert(medicine);
			MedicinePrice medPrice = new MedicinePrice();
			medPrice.setPkId(Tools.newPkId());
			medPrice.setMedicinePkId(medicine.getPkId());
			medPrice.setCreateDate(new Date());
			medPrice.setUpdateDate(new Date());
			medPrice.setPrice(medicine.getPrice());
			medPrice.setInpatientPrice(medicine.getInpatientPrice());
			medPrice.setInsurancePrice(medicine.getInsurancePrice());
			medPrice.setBeginDate(medicine.getUsageDate());
			medPrice.setCreatedBy(lu.getUser().getPkId());
			medPrice.setUpdatedBy(lu.getUser().getPkId());
			medPrice.setIsHasInsurance(medicine.getIsHasInsurance());
			insert(medPrice);

		} else if (medicine.getStatus().equals(Tool.MODIFIED)) {
			medicine.setUpdatedBy(lu.getUser().getPkId());
			medicine.setUpdatedDate(new Date());
			MedicinePrice medPrice = new MedicinePrice();
			medPrice.setPkId(Tools.newPkId());
			medPrice.setMedicinePkId(medicine.getPkId());
			medPrice.setCreateDate(new Date());
			medPrice.setUpdateDate(new Date());
			medPrice.setPrice(medicine.getPrice());
			medPrice.setInpatientPrice(medicine.getInpatientPrice());
			medPrice.setIsHasInsurance(medicine.getIsHasInsurance());
			medPrice.setCreatedBy(lu.getUser().getPkId());
			medPrice.setUpdatedBy(lu.getUser().getPkId());
			medPrice.setBeginDate(medicine.getUsageDate());
			medPrice.setInsurancePrice(medicine.getInsurancePrice());
			insert(medPrice);
			update(medicine);

		}

		else if (medicine.getStatus().equals(Tool.DELETE)) {
			deleteByAnyField(MedicinePrice.class, "medicinePkId", medicine.getPkId());
			deleteByPkId(Medicine.class, medicine.getPkId());
		}

		else if (medicine.getStatus().equals(Tool.UNCHANGED)) {

		}

	}

	@Override
	public void saveMedicineByList(List<Medicine> listMedicine) throws Exception{
		insert(listMedicine);
	}

	@Override
	public List<Medicine> getMedicineByListId(List<String> listId) throws Exception{

		CustomHashMap parameters = new CustomHashMap();
		parameters.put("ids", listId);
		return getByQuery(Medicine.class, "SELECT A FROM Medicine A WHERE A.id IN :ids", parameters);
	}


	public List<Medicine> getListMedicineByGroup(String groupId) throws Exception{
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("groupId", groupId);

		jpql.append("SELECT a FROM Medicine a ");
		jpql.append("INNER JOIN WarningMedicineGroup b ON a.pkId = b.medicinePkId ");
		jpql.append("WHERE b.groupId = :groupId ");

		List<Medicine> list = getByQuery(Medicine.class, jpql.toString(), parameters);
		return list;		
	}

	public List<String> getNotUseGroup() throws Exception{
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT DISTINCT a.groupId FROM WarningMedicineGroup a ");
		List<String> list = getByQuery(String.class, jpql.toString(), null);
		return list;
	}

	public List<String> getNotUseGroup(BigDecimal medicinePkId) throws Exception{
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("medicinePkId", medicinePkId);

		jpql.append("SELECT DISTINCT a.groupId FROM WarningMedicineGroup a WHERE a.medicinePkId = :medicinePkId");
		List<String> list = getByQuery(String.class, jpql.toString(), parameters);
		return list;
	}

	public List<Medicine> getNotUsedTogetherMedicineList(BigDecimal medicinePkId) throws Exception{
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("medicinePkId", medicinePkId);

		jpql.append("SELECT a FROM Medicine a ");
		jpql.append("INNER JOIN NotUserTogether b ON a.pkId = b.medicinePkId1 ");
		jpql.append("WHERE b.medicinePkId2 = :medicinePkId ");
		List<Medicine> list = getByQuery(Medicine.class, jpql.toString(), parameters);
		List<Medicine> list2 = new ArrayList<Medicine>();
		for (Medicine medicine : list) {
			parameters.put("medPkId", medicine.getPkId());
			jpql = new StringBuilder();
			jpql.append("SELECT a FROM NotUserTogether a ");
			jpql.append("WHERE a.medicinePkId1 = :medPkId ");
			jpql.append("AND a.medicinePkId2 = :medicinePkId ");
			List<NotUserTogether> notUserTogethers = getByQuery(NotUserTogether.class, jpql.toString(), parameters);
			if(notUserTogethers.size() > 0) medicine.setMessage(notUserTogethers.get(0).getMessage());
		}

		jpql = new StringBuilder();
		jpql.append("SELECT a FROM Medicine a ");
		jpql.append("INNER JOIN NotUserTogether b ON a.pkId = b.medicinePkId2 ");
		jpql.append("WHERE b.medicinePkId1 = :medicinePkId ");
		list2.addAll(getByQuery(Medicine.class, jpql.toString(), parameters));
		for (Medicine medicine : list2) {
			parameters.put("medPkId", medicine.getPkId());
			jpql = new StringBuilder();
			jpql.append("SELECT a FROM NotUserTogether a ");
			jpql.append("WHERE a.medicinePkId2 = :medPkId ");
			jpql.append("AND a.medicinePkId1 = :medicinePkId ");
			List<NotUserTogether> notUserTogethers = getByQuery(NotUserTogether.class, jpql.toString(), parameters);
			if(notUserTogethers.size() > 0) medicine.setMessage(notUserTogethers.get(0).getMessage());
		}
		list.addAll(list2);
		return list;
	}

	public PregnantWarning getPregnantWarning(BigDecimal medicinePkId) throws Exception{
		List<PregnantWarning> list = getByAnyField(PregnantWarning.class, "typePkId", medicinePkId);
		if(list.size() > 0) return list.get(0);
		return null;
	}

	public WarningAge getWarningAge(BigDecimal medicinePkId) throws Exception{
		List<WarningAge> list = getByAnyField(WarningAge.class, "typePkId", medicinePkId);
		if(list.size() > 0) return list.get(0);
		return null;
	}

	public WarningMedicineMaxDay getWarningMedicineMaxDay(BigDecimal medicinePkId) throws Exception{
		List<WarningMedicineMaxDay> list = getByAnyField(WarningMedicineMaxDay.class, "medicinePkId", medicinePkId);
		if(list.size() > 0) return list.get(0);
		return null;
	}

	public WarningMedicineDose getWarningMedicineDose(BigDecimal medicinePkId) throws Exception{
		List<WarningMedicineDose> list = getByAnyField(WarningMedicineDose.class, "medicinePkId", medicinePkId);
		if(list.size() > 0) return list.get(0);
		return null;
	}
	//Medicine List-д дуудаж байгаа листийн function
	@Override
	public List<Medicine> getMedicineForList(String usageType, LoggedUser lu, String filterKey, byte filterActiveMed, int month, boolean hasDR, int filterDrugPurpose) throws Exception {
		List<Medicine> list = new ArrayList<Medicine>();
		StringBuilder jpql = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -month);
		Date date = cal.getTime();

		parameters.put("usageType", usageType);
		parameters.put("filter", "%" + filterKey + "%");
		parameters.put("activeMedicine", filterActiveMed);
		parameters.put("date", date);
		parameters.put("filterDrugPurpose", filterDrugPurpose);

		jpql.append(
				"SELECT NEW pharmacy.entity.Medicine(a.pkId, a.id, a.name, a.iName, a.atcPkId, a.typePkId, "
						+ "a.measurementPkId, a.bioActive, a.minAge, a.maxAge, a.warningMessage, a.description, b.nameMn, "
						+ "c.name, d.name, a.drugDose, a.dayDose, a.dose, a.calcDose, a.calcDrugDose, a.calcType, f.price,"
						+ "a.medicineImpact, f.beginDate, f.isHasInsurance, f.insurancePrice, f.inpatientPrice, a.activeMedicine, "
						+ "a.isDivide, a.mixName, a.traditionalBool, a.isDiscountOut, a.isDiscountIn) ");
		jpql.append("FROM Medicine a ");
		jpql.append("LEFT JOIN View_ConstantATC b ON b.pkId = a.atcPkId ");
		jpql.append("LEFT JOIN View_ConstantMedicineType c ON c.pkId = a.typePkId ");
		jpql.append("LEFT JOIN Measurement d ON d.pkId = a.measurementPkId ");
		jpql.append("LEFT JOIN VIEW_MedicinePrice f ON a.pkId = f.medicinePkId ");
		jpql.append("WHERE (a.name LIKE :filter OR a.iName LIKE :filter OR a.id LIKE :filter) ");

		if(month > 0){
			jpql.append("AND a.createdDate > :date ORDER BY a.createdDate DESC ");
		}

		if(hasDR == true){
			jpql.append("AND a.hasDR = 1");
		}

		if(!usageType.equals("All")){
			jpql.append("AND a.usageType =:usageType");
		}

		if(filterActiveMed == 0){
			jpql.append("AND a.activeMedicine = 0");
		}

		if(filterDrugPurpose == 0){
			jpql.append(" ");
		}else{
			jpql.append("AND a.drugPurpose =:filterDrugPurpose");
		}

		list = getByQuery(Medicine.class, jpql.toString(), parameters);
		return list;
	}

	@Override
	public List<View_ConstantATC> getAtcs() throws Exception {
		// TODO Auto-generated method stub
		return getAll(View_ConstantATC.class);
	}


	@Override
	public List<View_ConstantMedicineType> getMedicineTypes() throws Exception {
		// TODO Auto-generated method stub
		return getAll(View_ConstantMedicineType.class);
	}

	@Override
	public List<Measurement> getMeasurements() throws Exception {
		// TODO Auto-generated method stub
		return getAll(Measurement.class);
	}

	@Override
	public List<Injection> getInjection(BigDecimal pkId, LoggedUser lu, String filterKey) throws Exception {
		CustomHashMap parameters = new CustomHashMap();
		StringBuilder jpql = new StringBuilder();
		parameters.put("pkId", pkId);
		parameters.put("filterKey", "%" + filterKey + "%");
		jpql.append("SELECT NEW pharmacy.entity.Injection(A.pkId,A.name,A.iName,A.id,A.atcPkId,A.typeInsjection,A.measurement,A.warningMessage,A.description,A.drugDose,A.dose,A.ageAtive,A.createdDate,A.updatedDate,A.active,A.allAge,A.ageDivision,A.injectionImpact,A.bioActive,B.price,B.insurancePrice,B.isHasInsurance ,B.beginDate,B.inpatientPrice)  ");
		jpql.append("FROM  Injection A  ");
		jpql.append("LEFT JOIN  InjectionPrice  B ON A.pkId = B.injectionPkId ");
		jpql.append("WHERE (A.name LIKE :filterKey OR   A.iName  LIKE :filterKey ) ");
		if (pkId.compareTo(BigDecimal.ZERO) != 0)
			jpql.append("AND  A.atcPkId=:pkId  ");
		jpql.append("ORDER BY A.createdDate  DESC ");
		return getByQuery(Injection.class, jpql.toString(), parameters);
	}

	@Override
	public void saveInjection(LoggedUser lu, Injection injection) throws Exception {
		System.out.println(injection.getStatus());
		if (Tool.ADDED.equals(injection.getStatus())) {
			injection.setPkId(Tools.newPkId());
			injection.setCreatedBy(lu.getEmployeePkId());
			injection.setCreatedDate(new Date());
			insert(injection);
			InjectionPrice price = new InjectionPrice();
			price.setPkId(Tools.newPkId());
			price.setInjectionPkId(injection.getPkId());
			price.setCreatedBy(lu.getEmployeePkId());
			price.setBeginDate(injection.getBeginDate());
			price.setInsurancePrice(injection.getInsurancePrice());
			price.setIsHasInsurance(injection.getIsHasInsurance());
			price.setPrice(injection.getPrice());
			price.setInpatientPrice(injection.getInpatientPrice());
			price.setCreatedDate(new Date());
			price.setCreatedBy(lu.getEmployeePkId());
			insert(price);
		} else if (Tool.MODIFIED.equals(injection.getStatus())) {
			injection.setUpdatedDate(new Date());
			injection.setUpdatedBy(lu.getEmployeePkId());
			deleteByAnyField(InjectionPrice.class, "injectionPkId", injection.getPkId());
			InjectionPrice price = new InjectionPrice();
			price.setPkId(Tools.newPkId());
			price.setInjectionPkId(injection.getPkId());
			price.setCreatedBy(lu.getEmployeePkId());
			price.setBeginDate(injection.getBeginDate());
			price.setInsurancePrice(injection.getInsurancePrice());
			price.setIsHasInsurance(injection.getIsHasInsurance());
			price.setPrice(injection.getPrice());
			price.setInpatientPrice(injection.getInpatientPrice());
			price.setUpdatedBy(lu.getEmployeePkId());
			price.setUpdatedDate(new Date());
			insert(price);

			update(injection);
		} else if (Tool.DELETE.equals(injection.getStatus())) {
			deleteByAnyField(InjectionPrice.class, "injectionPkId", injection.getPkId());
			deleteByPkId(Injection.class, injection.getPkId());
		}
	}

	@Override
	public List<Object[]> getConnectionPharmacy(String storedProcedure) throws Exception {
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			connection = DriverManager.getConnection("jdbc:sqlserver://10.10.3.5\\SL;databaseName=EmnelegAccounting","merchantUser", "@Merch4ntSL#");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		PreparedStatement statement;

		statement = connection.prepareStatement("USE EmnelegAccounting");
		statement.execute();

		statement = connection.prepareStatement(storedProcedure);
		List<Object[]> resultMap = new ArrayList<Object[]>();

		boolean results = statement.execute();

		while (results) {
			ResultSet rs = statement.getResultSet();
			while (rs.next()) {
				List<String> strList = new ArrayList<String>();
				String columnValue = rs.getString("Id");
				String columnValue1 = rs.getString("PkId");
				String columnValue2 = rs.getString("Name");
				strList.add(columnValue);
				strList.add(columnValue1);
				strList.add(columnValue2);
				resultMap.add(strList.toArray());
			}
			rs.close();
			results = statement.getMoreResults();
		} 
		statement.close();
		return resultMap;
	}
	
	@Override
	public List<CustomerMedicineDeispense> getItemWharehouse(BigDecimal pkId) throws Exception {
		List<CustomerMedicineDeispense>  customerDispenses = new ArrayList<CustomerMedicineDeispense>();
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			connection = DriverManager.getConnection("jdbc:sqlserver://10.10.3.5\\SL;databaseName=EmnelegAccounting","merchantUser", "@Merch4ntSL#");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		PreparedStatement statement;
		
		statement = connection.prepareStatement("USE EmnelegAccounting");
		statement =connection.prepareStatement("SELECT  * FROM InventoryWarehouseBalance  "
				+ "WHERE ItemPkId = " + pkId);
		statement.execute();
		boolean results = statement.execute();
		
	    while (results) {
	    	
	    	ResultSet rs = statement.getResultSet();
	    	while (rs.next()) {
	    		CustomerMedicineDeispense deispense = new CustomerMedicineDeispense();
	    		deispense.setWarehousePkId(new BigDecimal(rs.getString("WarehousePkId")));
	    		customerDispenses.add(deispense);
	    	}
	    	rs.close();
			results = statement.getMoreResults();
	    }
	    statement.close();	
		return customerDispenses;
	}


}