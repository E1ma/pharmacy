package pharmacy.businesslogic;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import pharmacy.businessentity.*;
import pharmacy.businesslogic.interfaces.IPharmacyLogic;
import pharmacy.businesslogic.interfaces.IPharmacyLogicLocal;
import pharmacy.entity.*;
import base.*;

@Stateless(name = "LogicPharmacy", mappedName = "pharmacy.businesslogic.LogicPharmacy")
public class LogicPharmacy extends base.BaseData implements IPharmacyLogic, IPharmacyLogicLocal {
	@Resource
	SessionContext sessionContext;

	public LogicPharmacy() {
		super("Pharmacy");
	}

	@Override
	public List<CustomerMedicineDeispense> getCustomerMedicineOcs(BigDecimal typeSelect) throws Exception {

		CustomHashMap parameters = new CustomHashMap();
		parameters.put("patientType", Tool.INPATIENTDTLSTATUS_IN);

		StringBuilder jpql = new StringBuilder();
		jpql  =  new StringBuilder();
		parameters.put("invType", Tool.INPATIENTDTLSTATUS_Medicine);
		jpql.append("SELECT DISTINCT NEW   pharmacy.businessentity.CustomerMedicineDeispense(c,a.pkId,a.hdrPkId,"
				+ "d.roomNo,f.name,b.beginDate,m.bedNumber,SUM(g.orderQty),SUM(g.qty)) ");
		jpql.append("FROM InvoiceHdr a ");
		jpql.append("INNER JOIN  InpatientHdr b  ON a.hdrPkId=b.pkId ");
		jpql.append("INNER JOIN Customer c ON  b.customerPkId=c.pkId ");
		jpql.append("INNER JOIN  Room d  ON  d.pkId =b.roomPkId ");
		jpql.append("INNER JOIN  BedInfo m  ON  m.pkId =b.bedInfoPkId ");
		jpql.append("INNER JOIN  SubOrganization f ON f.pkId=b.subOrganizationPkId ");
		jpql.append("INNER JOIN InvoiceDtl g  ON g.hdrPkId= a.pkId ");
		jpql.append("WHERE a.invType =:invType AND b.status<>0  AND b.moveOut<>2  ");
		if(BigDecimal.ONE.compareTo(typeSelect)==0)
			jpql.append("AND g.isToDispense=1 ");
		jpql.append("GROUP BY c,a.pkId,a.hdrPkId ,d.roomNo,f.name,b.beginDate ,m.bedNumber");

		return getByQuery(CustomerMedicineDeispense.class, jpql.toString(), parameters);
	}

	@Override
	public List<CustomerMedicineDeispense> getEmployees() throws Exception {
		// TODO Auto-generated method stub
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT NEW pharmacy.businessentity.CustomerMedicineDeispense(A,B)  FROM Employee A ");
		jpql.append("INNER JOIN SubOrganization  B  ON A.subOrganizationPkId  = B.pkId ");
		return getByQuery(CustomerMedicineDeispense.class, jpql.toString(), null);
	}

	@Override
	public List<Diagnose> getCusromerDiagnoseList(BigDecimal customerPkId) throws Exception {
		CustomHashMap parameters = new CustomHashMap();
		StringBuilder jpql = new StringBuilder();
		parameters.put("customerPkId", customerPkId);
		jpql.append("SELECT DISTINCT NEW pharmacy.entity.CustomerDiagnose(c.id,c.nameEn,a.type,a.date)  ");
		jpql.append("FROM CustomerDiagnose a ");
		jpql.append("INNER Inspection b ON  a.inspectionPkId = b.pkId ");
		jpql.append("INNER JOIN Diagnose c  ON a.diagnosePkId =  c.pkId ");
		jpql.append("WHERE b.customerPkId  = :customerPkId ");
		jpql.append("ORDER BY a.date DESC ");
		return getByQuery(Diagnose.class, jpql.toString(), parameters);
	}

	@Override
	public List<CustomerMedicineDeispense> getPharmacyDates(BigDecimal selectedCustomerPkId) throws Exception {
		List<CustomerMedicineDeispense> deispenses = new ArrayList<CustomerMedicineDeispense>();
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("patientType", Tool.INPATIENTDTLSTATUS_IN);
		parameters.put("selectedCustomerPkId", selectedCustomerPkId);

		StringBuilder jpql =  new StringBuilder();

		jpql.append("SELECT NEW pharmacy.businessentity.CustomerMedicineDeispense(a.createdDate,b.employeePkId,c.subOrganizationPkId,c.firstName,d.name ) "
				+ " FROM CustomerMedicine a ");
		jpql.append("INNER JOIN Inspection b On  a.inspectionPkId=b.pkId ");
		jpql.append("INNER JOIN Employee c ON b.employeePkId=c.pkId ");
		jpql.append("INNER JOIN SubOrganization d ON c.subOrganizationPkId=d.pkId ");
		jpql.append("WHERE b.patientType= :patientType  AND  b.customerPkId=:selectedCustomerPkId ");

		List<CustomerMedicineDeispense>  dates =  getByQuery(CustomerMedicineDeispense.class,jpql.toString(),parameters);
		Date beginDate = dates.get(0).getCreateDate();
		Date endDate =  dates.get(dates.size()-1).getCreateDate();
		Date bDate =  beginDate;
		int index=-1;
		int index1 = dates.size()-1;
		CustomerMedicineDeispense endCus  =  new CustomerMedicineDeispense(bDate,dates.get(index1).getEmployeePkId(),dates.get(index1).getSubPkId()
				,dates.get(index1).getEmployeeName(),dates.get(index1).getSubName());
		deispenses.add(endCus);
		System.out.println(dates.size());
		while (bDate.before(endDate)) {
			index++;
			if(index==dates.size()){
				index = index-1;
			}
			java.util.Calendar calendar =  java.util.Calendar.getInstance();
			calendar.setTime(bDate);
			calendar.add(java.util.Calendar.DATE, 1);
			bDate = calendar.getTime();
			CustomerMedicineDeispense cusDate = new CustomerMedicineDeispense(bDate,dates.get(index).getEmployeePkId(),dates.get(index).getSubPkId(),
					dates.get(index).getEmployeeName(),dates.get(index).getSubName());
			deispenses.add(cusDate);
		}
		return deispenses;
	}

	@Override
	public List<SubOrganization> getSubOrganizations() throws Exception {
		// TODO Auto-generated method stub
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT  A FROM SubOrganization  A");
		return getByQuery(SubOrganization.class, jpql.toString(), null);
	}

	@Override
	public void saveCustomerMedicine(LoggedUser loggedInfo, List<CustomerMedicineDeispense> customerMedicineDeispenses) throws Exception{
		for (CustomerMedicineDeispense customerMedicineDeispense : customerMedicineDeispenses) {
			for(CustomerMedicineDeispense  cd : customerMedicineDeispense.getListCustomerDipenses()){
				if(cd.getCountMedicineCustomerOrder()>0){
					CustomerMedicineDtl  dtl  =  new CustomerMedicineDtl();
					dtl.setPkId(Tools.newPkId());
					dtl.setCreatedBy(loggedInfo.getEmployeePkId());
					dtl.setCreatedDate( new Date());
					dtl.setUpdatedBy(loggedInfo.getEmployeePkId());
					dtl.setDispenseCount(cd.getCountMedicineCustomerOrder());
					dtl.setUpdatedDate(new Date());
					dtl.setCustomerMedicinePkId(cd.getCustomerMedicine().getPkId());
					dtl.setEmployeePkId(loggedInfo.getEmployeePkId());
					dtl.setDate(new Date());
					insert(dtl);
				}
				update(cd.getCustomerMedicine());
			}
		}

	}

	@Override
	public List<CustomerMedicineDeispense> getCustomerMedicineCt(BigDecimal customerPkId) throws Exception {
		// TODO Auto-generated method stub
		CustomHashMap parameters = new CustomHashMap();
		StringBuilder jpql = new StringBuilder();
		parameters.put("customerPkId", customerPkId);
		jpql.append("SELECT NEW pharmacy.businessentity.CustomerMedicineDeispense(A.createdDate)  ");
		jpql.append("FROM  CustomerMedicine  A   ");
		jpql.append("LEFT JOIN Inspection B  ON A.inspectionPkId =  B.pkId   ");
		jpql.append("LEFT JOIN Medicine  C  ON A.medicinePkId = C.pkId   ");
		jpql.append("WHERE B.customerPkId  = :customerPkId   ");
		jpql.append("GROUP BY A.createdDate  ");

		List<CustomerMedicineDeispense> deispenses = getByQuery(CustomerMedicineDeispense.class, jpql.toString(),
				parameters);
		for (CustomerMedicineDeispense cd : deispenses) {
			parameters.put("createDate", cd.getCreateDate());
			jpql = new StringBuilder();
			jpql.append(
					"SELECT NEW pharmacy.businessentity.CustomerMedicineDeispense(A.pkId,C.name,A.dose,A.medicineDescription,A.m,A.d,A.e)  ");
			jpql.append("FROM  CustomerMedicine  A   ");
			jpql.append("LEFT JOIN Inspection B  ON A.inspectionPkId =  B.pkId   ");
			jpql.append("LEFT JOIN Medicine  C  ON A.medicinePkId = C.pkId   ");
			jpql.append("WHERE  A.createdDate = :createDate  ");
			jpql.append("GROUP BY   A.pkId,C.name,A.dose,A.medicineDescription,A.m,A.d,A.e  ");
			cd.setListCreateDate(getByQuery(CustomerMedicineDeispense.class, jpql.toString(), parameters));
		}
		return deispenses;
	}

	@Override
	public List<CustomerMedicineDeispense> getCustomerMedicineDate(BigDecimal customerPkId, List<Date> date)
			throws Exception {
		// TODO Auto-generated method stub
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("customerPkId", customerPkId);
		parameters.put("createDate", date);

		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT NEW pharmacy.businessentity.CustomerMedicineDeispense(A.createdDate,A.m,A.d,A.e)  ");
		jpql.append("FROM  CustomerMedicine  A   ");
		jpql.append("LEFT JOIN Inspection B  ON A.inspectionPkId =  B.pkId   ");
		jpql.append("LEFT JOIN Medicine  C  ON A.medicinePkId = C.pkId   ");
		jpql.append("WHERE   B.customerPkId  = :customerPkId  AND  A.createdDate IN  :createDate  ");
		return getByQuery(CustomerMedicineDeispense.class, jpql.toString(), parameters);
	}

	@Override
	public List<CustomerMedicine> getCustomerMedicine(BigDecimal customerPkId, Date beginDate, Date endDate,
			BigDecimal employeePkId, String filterKey) throws Exception {
		StringBuilder jpql = new StringBuilder();
		beginDate.setHours(00);
		beginDate.setMinutes(00);
		endDate.setHours(23);
		endDate.setMinutes(59);
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("customerPkId", customerPkId);
		parameters.put("beginDate", beginDate);

		parameters.put("endDate", endDate);
		parameters.put("employeePkId", employeePkId);
		parameters.put("filterKey", "%" + filterKey + "%");
		jpql.append("SELECT NEW pharmacy.entity.CustomerMedicine (a, b, c.name, f.name, e.firstName, m.name, g.patientType ) ");
		jpql.append("FROM CustomerMedicine a ");
		jpql.append("INNER JOIN Medicine b ON a.medicinePkId = b.pkId ");
		jpql.append("INNER JOIN Measurement c ON c.pkId = b.measurementPkId ");
		jpql.append("INNER JOIN View_ConstantMedicineType m ON m.pkId = b.typePkId ");
		jpql.append("INNER JOIN Users d ON d.pkId = a.createdBy ");
		jpql.append("INNER JOIN Employee e ON e.userPkId = d.pkId ");
		jpql.append("INNER JOIN SubOrganization f ON f.pkId = e.subOrganizationPkId ");
		jpql.append("INNER JOIN Inspection g ON g.pkId = a.inspectionPkId ");
		jpql.append("WHERE g.customerPkId = :customerPkId ");
		if (employeePkId != null && BigDecimal.ZERO.compareTo(employeePkId) != 0)
			jpql.append("AND a.updatedBy = :employeePkId ");
		if (filterKey != null && !"".equals(filterKey))
			jpql.append("AND b.iName LIKE :filterKey ");
		jpql.append("AND a.createdDate BETWEEN :beginDate AND :endDate ");
		List<CustomerMedicine> list = getByQuery(CustomerMedicine.class, jpql.toString(), parameters);
		StringBuilder jpql1 = new StringBuilder();	
		jpql1.append("SELECT NEW pharmacy.entity.CustomerMedicine (a.pkId, SUM(b.dispenseCount)) ");
		jpql1.append("FROM CustomerMedicine a ");
		jpql1.append("INNER JOIN CustomerMedicineDtl b on a.pkId = b.customerMedicinePkId ");
		jpql1.append("INNER JOIN Inspection c ON c.pkId = a.inspectionPkId ");
		jpql1.append("WHERE c.customerPkId = :customerPkId ");
		jpql1.append("GROUP BY a.pkId ");
		List<CustomerMedicine> list1 = getByQuery(CustomerMedicine.class, jpql1.toString(), parameters);
		for(CustomerMedicine cm:list)
		{
			for(CustomerMedicine cm1:list1)
				if(cm.getPkId().compareTo(cm1.getPkId())==0)
				{
					cm.setDispenseCount(cm1.getDispenseCount());
				}
		}

		return list;

	}
	@Override
	public List<CustomerMedicineDeispense> getPharmacyDispense(Date date,BigDecimal selectedCustomerPkId, BigDecimal employeePkId,
			BigDecimal subPkId, int val) throws Exception {
		Date beginDate  = (Date) Tool.deepClone(date);
		beginDate.setHours(0);
		beginDate.setMinutes(0);
		beginDate.setSeconds(0);

		Date endDate  = (Date) Tool.deepClone(date);
		endDate.setHours(23);
		endDate.setMinutes(59);
		endDate.setSeconds(59);
		CustomHashMap parameters = new CustomHashMap();

		parameters.put("beginDate", beginDate);
		parameters.put("endDate", endDate);
		parameters.put("selectedCustomerPkId", selectedCustomerPkId);
		parameters.put("employeePkId", employeePkId);
		parameters.put("subPkId", subPkId);
		parameters.put("patientType", Tool.INPATIENTDTLSTATUS_IN);

		StringBuilder jpql = new StringBuilder();
		jpql.append(
				"SELECT NEW pharmacy.businessentity.CustomerMedicineDeispense(A,f.name,f.iName) FROM  CustomerMedicine  A  ");
		jpql.append("INNER JOIN 	Inspection I  ON  A.inspectionPkId = I.pkId ");
		jpql.append("INNER JOIN  Employee C  ON I.employeePkId = C.pkId ");
		jpql.append("INNER JOIN Medicine f ON A.medicinePkId=f.pkId ");
		jpql.append("WHERE I.customerPkId = :selectedCustomerPkId  AND I.employeePkId=:employeePkId AND C.subOrganizationPkId=:subPkId ");
		jpql.append("AND I.patientType = :patientType  AND  A.createdDate> :beginDate AND A.createdDate< :endDate  ");
		return getByQuery(CustomerMedicineDeispense.class,jpql.toString(),parameters);
	}

	@Override
	public CustomerMedicineDeispense getCustomerMedicineDtl(BigDecimal pkId) throws Exception {
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("customerMedicinePkId", pkId);
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT  NEW  pharmacy.businessentity.CustomerMedicineDeispense(a.customerMedicinePkId,SUM(a.dispenseCount)) ");
		jpql.append("FROM CustomerMedicineDtl a ");
		jpql.append("WHERE a.customerMedicinePkId =:customerMedicinePkId ");
		jpql.append("GROUP BY  a.customerMedicinePkId");
		List<CustomerMedicineDeispense> lst  =  getByQuery(CustomerMedicineDeispense.class,jpql.toString(),parameters);
		if(lst.size()>0)
			return lst.get(0);
		return new CustomerMedicineDeispense();
	}

	@Override
	public List<ExaminationRequestCompleted> getExaminationRequestCompletedByFilter(List<BigDecimal> subOrgaPkId,
			List<BigDecimal> employeePkId, BigDecimal customerPkId) throws Exception {
		StringBuilder jpql = new StringBuilder();
		StringBuilder jpql1 = new StringBuilder();
		StringBuilder jpql2 = new StringBuilder();
		StringBuilder jpql3 = new StringBuilder();
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("customerPkId", customerPkId);

		jpql.append(
				"SELECT NEW hospital.entity.ExaminationRequestCompleted(A, B.name, C.firstName) FROM ExaminationRequestCompleted A ");
		jpql.append("LEFT JOIN Examination B ON A.examinationPkId = B.pkId ");
		jpql.append("LEFT JOIN Employee C ON A.employeePkId = C.pkId ");
		jpql.append("WHERE A.customerPkId = :customerPkId ");

		jpql1.append(
				"SELECT NEW hospital.entity.ExaminationRequestCompleted(A, B.name, C.firstName) FROM ExaminationRequest A ");
		jpql1.append("LEFT JOIN Examination B ON A.examinationPkId = B.pkId ");
		jpql1.append("LEFT JOIN Employee C ON A.employeePkId = C.pkId ");
		jpql1.append("WHERE A.customerPkId = :customerPkId ");

		jpql2.append(
				"SELECT NEW hospital.entity.ExaminationRequestCompleted(A, B.name, C.firstName) FROM ExaminationRequestActive A ");
		jpql2.append("LEFT JOIN Examination B ON A.examinationPkId = B.pkId ");
		jpql2.append("LEFT JOIN Employee C ON A.employeePkId = C.pkId ");
		jpql2.append("WHERE A.customerPkId = :customerPkId ");

		jpql3.append(
				"SELECT NEW hospital.entity.ExaminationRequestCompleted(A, B.name, C.firstName) FROM ExaminationRequestTempSave A ");
		jpql3.append("LEFT JOIN Examination B ON A.examinationPkId = B.pkId ");
		jpql3.append("LEFT JOIN Employee C ON A.employeePkId = C.pkId ");
		jpql3.append("WHERE A.customerPkId = :customerPkId ");

		if (employeePkId != null && employeePkId.size() > 0) {
			parameters.put("employeePkId", employeePkId);
			jpql.append(" AND C.pkId IN :employeePkId ");
			jpql1.append(" AND C.pkId IN :employeePkId ");
			jpql2.append(" AND C.pkId IN :employeePkId ");
			jpql3.append(" AND C.pkId IN :employeePkId ");
		}
		if (subOrgaPkId != null && subOrgaPkId.size() > 0) {
			parameters.put("subOrgaPkId", subOrgaPkId);
			jpql.append(" AND C.subOrganizationPkId IN :subOrgaPkId ");
			jpql1.append(" AND C.subOrganizationPkId IN :subOrgaPkId ");
			jpql2.append(" AND C.subOrganizationPkId IN :subOrgaPkId ");
			jpql3.append(" AND C.subOrganizationPkId IN :subOrgaPkId ");
		}

		List<ExaminationRequestCompleted> completeds = getByQuery(ExaminationRequestCompleted.class, jpql.toString(),
				parameters);
		completeds.addAll(getByQuery(ExaminationRequestCompleted.class, jpql1.toString(), parameters));
		completeds.addAll(getByQuery(ExaminationRequestCompleted.class, jpql2.toString(), parameters));
		completeds.addAll(getByQuery(ExaminationRequestCompleted.class, jpql3.toString(), parameters));
		return completeds;
	}

	@Override
	public List<OutcomeInfo> getOutcomeInfoList(Date outcomeDate, BigDecimal employeePkId, String filterStr) throws Exception{
		List<OutcomeInfo> listInfo = new ArrayList<OutcomeInfo>();
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("outcomeDate", outcomeDate);
		parameters.put("employeePkId", employeePkId);
		parameters.put("filterStr", filterStr);

		String jpql = "SELECT NEW hospital.businessentity.OutcomeInfo() "
				+ "FROM ";
		return getByQuery(OutcomeInfo.class, jpql, parameters);
	}

	@Override
	public List<CustomerMedicineDeispense> getPharmacyDispenseLst(BigDecimal pkId,BigDecimal type,Date date,Date endDate) throws Exception {
		
		Date bDate = (Date) Tool.deepClone(date);
		bDate.setHours(0);
		bDate.setMinutes(0);
		bDate.setSeconds(0);
		
		Date eDate = (Date) Tool.deepClone(endDate);
		eDate.setHours(23);
		eDate.setMinutes(59);
		eDate.setSeconds(59);
		
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("pkId", pkId);
		parameters.put("medicine", Tool.INPATIENTDTLSTATUS_Medicine);
		parameters.put("injection", Tool.INPATIENTDTLSTATUS_INJECTION);
		parameters.put("bDate", bDate);
		parameters.put("eDate", eDate);
		
		
		StringBuilder jpql  =  new StringBuilder();

		jpql.append("SELECT DISTINCT NEW pharmacy.businessentity.CustomerMedicineDeispense(a,b.id,b.name,b.type,SUM(a.qty)) ");
		jpql.append("FROM InvoiceDtl a ");
		jpql.append("INNER JOIN Item b  ON  a.itemPkId=b.pkId ");
		jpql.append("WHERE a.hdrPkId = :pkId  AND (b.type = :medicine OR b.type = :injection) ");
//		jpql.append("AND a.invDate BETWEEN :bDate AND :eDate ");
		jpql.append("GROUP BY a, b.id,b.name,b.type ");

		return getByQuery(CustomerMedicineDeispense.class, jpql.toString(),parameters);
	}
	@Override
	public List<CustomerMedicineDeispense> getPharmacyDispenseDates(BigDecimal hdrPkId,BigDecimal medPkId,Date date) throws Exception {
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("medPkId", medPkId);
		parameters.put("pkId", hdrPkId);
		Date beginDate  = (Date) Tool.deepClone(date);
		beginDate.setHours(0);
		beginDate.setMinutes(0);
		beginDate.setSeconds(0);

		Date endDate  = (Date) Tool.deepClone(date);
		endDate.setHours(23);
		endDate.setMinutes(59);
		endDate.setSeconds(59);

		parameters.put("bDate", beginDate);
		parameters.put("eDate", endDate);
		parameters.put("medicine", Tool.INPATIENTDTLSTATUS_Medicine);
		System.out.println(beginDate  +" data " +endDate);
		StringBuilder jpql  =  new StringBuilder();

		if(medPkId.compareTo(BigDecimal.ZERO)==0) {
			jpql.append("SELECT DISTINCT  NEW pharmacy.businessentity.CustomerMedicineDeispense(c.firstName,b.id,b.name,a) ");
			jpql.append("FROM InvoiceDtl a ");
			jpql.append("LEFT JOIN Item b  ON  a.itemPkId=b.pkId ");
			jpql.append("LEFT JOIN Employee c ON c.pkId=a.updatedBy ");
			jpql.append("WHERE   a.invDate  BETWEEN  :bDate  AND :eDate ");
			jpql.append("AND b.type = :medicine ");
		} else {
			jpql.append("SELECT DISTINCT NEW pharmacy.businessentity.CustomerMedicineDeispense(a) ");
			jpql.append("FROM InvoiceDtl a ");
			jpql.append("INNER JOIN Item b  ON  a.itemPkId=b.pkId ");
			jpql.append("WHERE a.itemPkId = :medPkId ");
			jpql.append("AND b.type = :medicine ");
		}
		jpql.append(" AND a.hdrPkId = :pkId ");
		return getByQuery(CustomerMedicineDeispense.class, jpql.toString(),parameters);
	}


	@Override
	public void updatePharmacy(InvoiceDtl dtl, LoggedUser user) throws Exception {

		Date date = new Date();
		if(Tool.MODIFIED.equals(dtl.getStatus())) {
			dtl.setIsToDispense((byte) 1);
			dtl.setQty(dtl.getOrderQty());
			dtl.setUpdatedDate(date);
			dtl.setUpdatedBy(user.getEmployeePkId());
			update(dtl);
		} else if(Tool.ADDED.equals(dtl.getStatus())) {
			dtl.setUpdatedDate(date);
			dtl.setUpdatedBy(user.getEmployeePkId());
			update(dtl);
		}
	}

	@Override
	public List<InspectionForm> getPharmacyEmr(BigDecimal hdrPkId,String type) throws Exception {
		System.out.println(hdrPkId);
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("pkId", hdrPkId);

		StringBuilder jpql  =  new StringBuilder();

		parameters.put("type", type);
		jpql.append("SELECT NEW  pharmacy.entity.InspectionForm(d.firstName,f.name,c,b.date,b.typePkId) ");
		jpql.append("FROM  InpatientHdr a ");
		jpql.append("INNER JOIN InpatientDtl b  ON a.pkId=b.inpatientHdrPkId ");
		jpql.append("INNER JOIN InspectionForm c  ON  c.inspectionPkId=b.typePkId ");
		jpql.append("INNER JOIN  Employee d  ON d.pkId=b.employeePkId ");
		jpql.append("INNER JOIN SubOrganization f ON f.pkId=d.subOrganizationPkId ");
		jpql.append("WHERE b.inpatientHdrPkId=:pkId AND b.type=:type ");
		return  getByQuery(InspectionForm.class, jpql.toString(),parameters);
	}

	@Override
	public Role getRoleUser(LoggedUser user) throws Exception {

		CustomHashMap parameters = new CustomHashMap();
		parameters.put("pkId", user.getUser().getPkId());
		parameters.put("name", "%" + "Эм зүйч" +"%");
		StringBuilder jpql  =  new StringBuilder();
		jpql.append("SELECT  a FROM Role a ");
		jpql.append("INNER JOIN  UserRoleMap b ON a.pkId =b.rolePkId ");
		jpql.append("WHERE b.userPkId = :pkId AND a.name LIKE :name  ");
		List<Role> roles = getByQuery(Role.class, jpql.toString(),parameters);
		if(roles.size()>0)
			return roles.get(0); 
		return new Role();
	}

	@Override
	public List<VitalSign> getVitalSign(BigDecimal pkId) throws Exception {
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("pkId", pkId);

		StringBuilder jpql  =  new StringBuilder();
		jpql.append("SELECT  a FROM VitalSign a ");
		jpql.append("INNER JOIN  Employee b ON a.employeePkId=b.pkId ");
		jpql.append("WHERE a.inpatientPkId = :pkId ");
		jpql.append("ORDER BY a.createdDate ASC ");

		return getByQuery(VitalSign.class,jpql.toString(), parameters) ;
	}

	@Override
	public List<CustomerMedicineDeispense> getPharmacyBeforeOrder(BigDecimal pkId) throws Exception {
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("pkId", pkId);
		parameters.put("medicine", Tool.INPATIENTDTLSTATUS_Medicine);
		StringBuilder jpql  =  new StringBuilder();
		jpql.append("SELECT  NEW pharmacy.businessentity.CustomerMedicineDeispense(a.startDate) ");
		jpql.append("FROM NursingServiceList  a ");
		jpql.append("INNER JOIN  InspectionDtl b  ON  a.inspectionDtlPkId =b.pkId ");
		jpql.append("WHERE b.type = :medicine AND a.inpatientHdrPkId =:pkId ");
		List<CustomerMedicineDeispense> deispenses =  getByQuery(CustomerMedicineDeispense.class,jpql.toString(),parameters);

		for (CustomerMedicineDeispense customerMedicineDeispense : deispenses) {
			Date  beginDate = (Date)  Tool.deepClone(customerMedicineDeispense.getCreateDate());
			beginDate.setHours(0);
			beginDate.setMinutes(0);
			beginDate.setSeconds(0);
			Date  endDate = (Date)  Tool.deepClone(customerMedicineDeispense.getCreateDate());
			endDate.setHours(23);
			endDate.setMinutes(59);
			endDate.setSeconds(59);

			parameters.put("bDate", beginDate);
			parameters.put("eDate", endDate);
			jpql  = new StringBuilder();

			jpql.append("SELECT  NEW pharmacy.businessentity.CustomerMedicineDeispense(c.dose,c.medicineDescription,f.m,f.d,f.e,f.confirm,d.name,d.id,f.pkId) ");
			jpql.append("FROM NursingServiceList  a ");
			jpql.append("INNER JOIN  InspectionDtl b  ON  a.inspectionDtlPkId =b.pkId ");
			jpql.append("INNER JOIN CustomerMedicine c  ON  c.pkId=b.typePkId ");
			jpql.append("INNER JOIN Medicine d ON  d.pkId=c.medicinePkId ");
			jpql.append("INNER JOIN NursingServiceList f ON f.inspectionDtlPkId=b.pkId ");
			jpql.append("WHERE b.type =:medicine AND a.inpatientHdrPkId =:pkId ");
			jpql.append("AND a.startDate  BETWEEN :bDate  AND :eDate ");

			customerMedicineDeispense.setListCustomerDipenses(getByQuery(CustomerMedicineDeispense.class, jpql.toString(),parameters));
		}

		return deispenses;
	}

	@Override
	public CustomerMedicineDeispense getNursingFilter(BigDecimal servicePkId, int time) throws Exception {
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("pkId",servicePkId);
		parameters.put("time",time);

		StringBuilder jpql  =  new StringBuilder();
		jpql.append("SELECT  NEW pharmacy.businessentity.CustomerMedicineDeispense(a.serviceDate,a.description,b.firstName) ");
		jpql.append("FROM  NursingServiceListDtl  a ");
		jpql.append("INNER JOIN Employee b ON a.nursePkId=b.pkId ");
		jpql.append("WHERE a.nursingServiceListPkId= :pkId AND a.time =:time ");
		List<CustomerMedicineDeispense> customerMedicineDeispenses =  getByQuery(CustomerMedicineDeispense.class,jpql.toString(),parameters);
		if(customerMedicineDeispenses.size()>0)
			return customerMedicineDeispenses.get(0);
		else return new CustomerMedicineDeispense();
	}
	@Override
	public List<CustomerMedicineDeispense> getPresriptionMedicines(BigDecimal invPkId,Date bDate,Date eDate) throws Exception {
		
		CustomHashMap parameters = new CustomHashMap();
		Date beginDate =  (Date) Tool.deepClone(bDate);
		beginDate.setHours(0);
		beginDate.setMinutes(0);
		beginDate.setSeconds(0);
		
		Date endDate =  (Date) Tool.deepClone(eDate);
		endDate.setHours(23);
		endDate.setMinutes(59);
		endDate.setSeconds(59);
		
		parameters.put("pkId",invPkId);
		parameters.put("med","MEDICINE");
		parameters.put("injection","INJECTION");
		
		parameters.put("beginDate",beginDate);
		parameters.put("endDate",endDate);
		
		
		StringBuilder jpql  =  new StringBuilder();
		jpql.append("SELECT  NEW pharmacy.businessentity.CustomerMedicineDeispense(c.firstName,b.id,b.name,b.type,b.dose,a) ");
		jpql.append("FROM InvoiceDtl a ");
		jpql.append("INNER JOIN Item b ON a.itemPkId =b.pkId ");
		jpql.append("INNER JOIN Employee c  ON c.pkId=a.pharmacistPkId ");
		jpql.append("WHERE a.hdrPkId =:pkId AND a.isToDispense =1 ");
		jpql.append("AND (b.type=:med OR b.type= :injection) ");
		jpql.append("AND  a.invDate BETWEEN  :beginDate  AND :endDate ");
//		AND  (a.isPackage <> 1 OR a.isPackage  IS NULL)
		return getByQuery(CustomerMedicineDeispense.class, jpql.toString(),parameters);
	}
	
	@Override
	public List<CustomerMedicineDeispense> getCustomerItem(BigDecimal invHdrPkId) throws Exception {
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("pkId",invHdrPkId);
		parameters.put("type","MEDICINE");
		
		StringBuilder jpql  =  new StringBuilder();
		jpql.append("SELECT DISTINCT NEW pharmacy.businessentity.CustomerMedicineDeispense(a.itemPkId,a.invDate,d.cardNumber,"
				+ " a.qty,a.orderQty,b.id ) ");
		jpql.append("FROM InvoiceDtl a ");
		jpql.append("INNER JOIN Item b ON a.itemPkId =b.pkId  ");
		jpql.append("INNER JOIN InvoiceHdr c ON a.hdrPkId=c.pkId ");
		jpql.append("INNER JOIN Customer d  ON d.pkId=c.customerPkId ");
		jpql.append("WHERE a.hdrPkId =:pkId AND a.isToDispense =1 ");
		jpql.append("AND b.type=:type  AND a.isPackage=1 ");

		return getByQuery(CustomerMedicineDeispense.class, jpql.toString(),parameters);
			
	}
	
	@Override
	public void sendSaveItem(List<CustomerMedicineDeispense> customerMedicineDeispenses) throws Exception {
		try {
			String data ="EXEC SaveDocumentAPI @documentType = 'GoodsIssue', @xml = N' <Document> ";
			for (CustomerMedicineDeispense customerMedicineDeispense : customerMedicineDeispenses) {
				data+="<Line>" + 
						"<DocumentId>1</DocumentId>" + 
						"<DocumentPkId>0</DocumentPkId> " + 
						"<DocumentNumber></DocumentNumber> " + 
						"<DocumentDate>2018-10-16 11:37:19</DocumentDate>" + 
						"<DocumentDesc>0</DocumentDesc>" + 
						"<WarehousePkId>1803091549132091025</WarehousePkId>" + 
						"<CustomerPkId>"+customerMedicineDeispense.getCustomerPkId() +"</CustomerPkId>" +
						"<ItemPkId>"+ customerMedicineDeispense.getItemPkId() +"</ItemPkId>"+ 
						"<Qty>"+ customerMedicineDeispense.getMedicineQty()+"</Qty>" + 
						"<UnitCost>0</UnitCost>" + 
						"<OppAccountPkId>32</OppAccountPkId>" + 
						"</Line>";
			}
			data+="</Document>' , @userId = 'Admin'";
			connectionPharmacy(data);	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void connectionPharmacy(String execQuery) throws Exception {
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			connection = DriverManager.getConnection("jdbc:sqlserver://10.10.3.5\\SL;databaseName=EmnelegAccounting","merchantUser", "@Merch4ntSL#");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		PreparedStatement statement;
		statement = connection.prepareStatement("USE EmnelegAccounting");
		statement.execute();
		statement = connection.prepareStatement(execQuery);
		statement.execute();
	}
	@Override
	public List<CustomerMedicineDeispense> getMedicinePackagePrint(BigDecimal pkId) throws Exception {
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("pkId",pkId);
		parameters.put("type","MEDICINE");
		
		StringBuilder jpql  =  new StringBuilder();
		jpql.append("SELECT DISTINCT NEW pharmacy.businessentity.CustomerMedicineDeispense(b.id,b.name) ");
		jpql.append("FROM InvoiceDtl a ");
		jpql.append("INNER JOIN Item b ON a.itemPkId =b.pkId  ");
		jpql.append("INNER JOIN InvoiceHdr c ON a.hdrPkId=c.pkId ");
		jpql.append("WHERE a.hdrPkId =:pkId AND a.isToDispense =1 ");
		jpql.append("AND b.type=:type  AND a.isPackage=1 ");
		return getByQuery(CustomerMedicineDeispense.class, jpql.toString(),parameters);
	}
	
	@Override
	public List<CustomerMedicineDeispense> getPackagePrintDates(BigDecimal invPkId) throws Exception {
		CustomHashMap parameters = new CustomHashMap();
		parameters.put("pkId",invPkId);
		parameters.put("type","MEDICINE");
		parameters.put("injection","INJECTION");
	
		StringBuilder jpql  =  new StringBuilder();
		jpql.append("SELECT DISTINCT NEW pharmacy.businessentity.CustomerMedicineDeispense(a.invDate) ");
		jpql.append("FROM InvoiceDtl a ");
		jpql.append("INNER JOIN Item b ON a.itemPkId =b.pkId  ");
		jpql.append("WHERE a.hdrPkId =:pkId AND a.isToDispense =1 ");
		jpql.append("AND (b.type=:type OR b.type=:injection)  AND a.isPackage=1 ");
		return getByQuery(CustomerMedicineDeispense.class, jpql.toString(),parameters);
	}
	
	@Override
	public List<CustomerMedicineDeispense> getItemAll() throws Exception {
		
		List<Object[]> list = getConnectionPharmacy("EXEC GetInfo @merchantId = '001', @password = 'merchant00!', @objectId = 'Item'");
		List<CustomerMedicineDeispense> deispenses =  new ArrayList<CustomerMedicineDeispense>();
		for (int i=0;i<list.size();i++) {
			CustomerMedicineDeispense deispense = new CustomerMedicineDeispense();
			deispense.setMedicinePkId(new BigDecimal(list.get(i)[0].toString()));
			deispense.setMedicineId(list.get(i)[1].toString());
			deispenses.add(deispense);
		}
		
		return deispenses;
	}
	
	@Override
	public List<CustomerMedicineDeispense> getCustomerList() throws Exception {
		List<Object[]> list = getConnectionPharmacy("EXEC GetInfo @merchantId = '001', @password = 'merchant00!', @objectId = 'Customer'");
		List<CustomerMedicineDeispense> deispenses =  new ArrayList<CustomerMedicineDeispense>();
		
		for (int i=0;i<list.size();i++) {
			
			CustomerMedicineDeispense deispense = new CustomerMedicineDeispense();
			deispense.setCustomerPkId(new BigDecimal(list.get(i)[0].toString()));
			deispense.setCustomerCardNumber(list.get(i)[1].toString());
			deispenses.add(deispense);
	
		}
		
		return deispenses;
	}
	
	@Override
	public List<Object[]> getConnectionPharmacy(String storedProcedure) throws Exception {
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			connection = DriverManager.getConnection("jdbc:sqlserver://10.10.3.5\\SL;databaseName=EmnelegAccounting","merchantUser", "@Merch4ntSL#");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		PreparedStatement statement;
		
		statement = connection.prepareStatement("USE EmnelegAccounting");
		statement.execute();
		
		statement = connection.prepareStatement(storedProcedure);
		List<Object[]> resultMap = new ArrayList<Object[]>();
		
		boolean results = statement.execute();
	   
	    while (results) {
	    	ResultSet rs = statement.getResultSet();
	    	ResultSetMetaData rsmd = rs.getMetaData();
	    	int columnsNumber = rsmd.getColumnCount();
	    	while (rs.next()) {
	    		List<String> strList = new ArrayList<String>();
	    		 for (int i = 1; i <= columnsNumber; i++) {
	    			 String columnValue = rs.getString(i);
	    			 //strList.add(rsmd.getColumnName(i)); boolean results = statement.execute();
	    			 strList.add(columnValue);
	    		 }
	    		 resultMap.add(strList.toArray());
	    	}
    		rs.close();
			results = statement.getMoreResults();
	    } 
		statement.close();
		return resultMap;
	}
	
	@Override
	public CustomerMedicineDeispense getWarehouse(BigDecimal itemPkId) throws Exception {
		// TODO Auto-generated method stub
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			connection = DriverManager.getConnection("jdbc:sqlserver://10.10.3.5\\SL;databaseName=EmnelegAccounting","merchantUser", "@Merch4ntSL#");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		PreparedStatement statement;
		
		statement = connection.prepareStatement("USE EmnelegAccounting");
		statement =connection.prepareStatement("SELECT  * FROM InventoryWarehouseBalance  "
				+ "WHERE WarehousePkId=1803091549132091025 AND ItemPkId = " + itemPkId);
		statement.execute();
		boolean results = statement.execute();
		CustomerMedicineDeispense deispense = new CustomerMedicineDeispense();
	    while (results) {
	    	ResultSet rs = statement.getResultSet();
	    	while (rs.next()) {
	    		deispense.setOrderBalance(new BigDecimal(rs.getString("OnHand")));
	    	}
	    	rs.close();
			results = statement.getMoreResults();
	    }
	    statement.close();	
		return deispense ;
	}

	@Override
	public List<PharmacyImformation> getOrderPharmacy(Date date,Date eDate) throws Exception {
		
		Date bDate =  (Date) Tool.deepClone(date);
		bDate.setHours(0);
		bDate.setMinutes(0);
		bDate.setSeconds(0);
	
		Date endDate =  (Date) Tool.deepClone(eDate);
		endDate.setHours(23);
		endDate.setMinutes(59);
		endDate.setSeconds(59);
		
		CustomHashMap parameters  =  new CustomHashMap();
		parameters.put("injection", "INJECTION");
		parameters.put("medicine", "MEDICINE");
		parameters.put("bDate", bDate);
		parameters.put("endDate", endDate);

		StringBuilder jpql =  new StringBuilder();
		jpql.append("SELECT NEW pharmacy.businessentity.PharmacyImformation(c.type,c.name,c.id,SUM(b.orderQty)) ");
		jpql.append("FROM InvoiceHdr a ");
		jpql.append("INNER JOIN InvoiceDtl b ON a.pkId = b.hdrPkId ");
		jpql.append("INNER JOIN Item c ON c.pkId = b.itemPkId ");
		jpql.append("WHERE (c.type = :injection OR c.type = :medicine ) ");
		jpql.append("AND b.invDate  BETWEEN :bDate AND :endDate ");
		jpql.append("GROUP BY c.type,c.name,c.id ");
		
		return getByQuery(PharmacyImformation.class, jpql.toString(),parameters);
	}
	
	@Override
	public PharmacyImformation getOrderPharmacyDispense(String id, String name, BigDecimal type,Date date,Date eDate)
			throws Exception {
		
		Date bDate =  (Date) Tool.deepClone(date);
		bDate.setHours(0);
		bDate.setMinutes(0);
		bDate.setSeconds(0);
	
		Date endDate =  (Date) Tool.deepClone(eDate);
		endDate.setHours(23);
		endDate.setMinutes(59);
		endDate.setSeconds(59);
		
		CustomHashMap parameters  =  new CustomHashMap();
		parameters.put("injection", "INJECTION");
		parameters.put("medicine", "MEDICINE");
		parameters.put("id", id);
		parameters.put("name", name);
		
		parameters.put("bDate", bDate);
		parameters.put("endDate", endDate);
		
		// emzuich  em  olgoson  too 
		StringBuilder jpql =  new StringBuilder();
		if(type.compareTo(BigDecimal.ZERO)==0) {
			jpql.append("SELECT NEW pharmacy.businessentity.PharmacyImformation(SUM(b.qty)) ");
			jpql.append("FROM  InvoiceDtl b ");
			jpql.append("INNER JOIN Item c ON c.pkId = b.itemPkId ");
			jpql.append("WHERE (c.type = :injection OR c.type = :medicine ) ");
			jpql.append("AND b.isToDispense = 1  AND (c.name =:name OR c.id= :id) ");
			jpql.append("AND b.invDate  BETWEEN :bDate AND :endDate ");
		} else if(type.compareTo(BigDecimal.ONE)==0) {
			jpql =  new StringBuilder();
			jpql.append("SELECT NEW pharmacy.businessentity.PharmacyImformation(SUM(b.qty)) ");
			jpql.append("FROM  InvoiceDtl b ");
			jpql.append("INNER JOIN Item c ON c.pkId = b.itemPkId ");
			jpql.append("WHERE (c.type = :injection OR c.type = :medicine ) ");
			jpql.append("AND b.isPackage = 1  AND (c.name =:name OR c.id= :id) ");
			jpql.append("AND b.invDate  BETWEEN :bDate AND :endDate ");
		}
		List<PharmacyImformation> pharmacy1 = getByQuery(PharmacyImformation.class, jpql.toString(),parameters);
		if(pharmacy1.size()>0)
			return pharmacy1.get(0);
		return new PharmacyImformation();
	}
	
	
	@Override
	public List<PharmacyImformation> getOrderSubOrganization(String id, String name,Date date,Date eDate) throws Exception {
		
		Date bDate =  (Date) Tool.deepClone(date);
		bDate.setHours(0);
		bDate.setMinutes(0);
		bDate.setSeconds(0);
	
		Date endDate =  (Date) Tool.deepClone(eDate);
		endDate.setHours(23);
		endDate.setMinutes(59);
		endDate.setSeconds(59);
		
		CustomHashMap parameters  =  new CustomHashMap();
		parameters.put("id", id);
		parameters.put("name", name);
		parameters.put("bDate", bDate);
		parameters.put("endDate", endDate);
		
		StringBuilder jpql  =  new StringBuilder();
		jpql.append("SELECT  NEW pharmacy.businessentity.PharmacyImformation(c.pkId,c.name,SUM(b.qty)) ");
		jpql.append("FROM InvoiceHdr a ");
		jpql.append("INNER JOIN InvoiceDtl b ON  a.pkId=b.hdrPkId ");
		jpql.append("INNER JOIN  SubOrganization c ON  c.pkId=a.subOrganizationPkId ");
		jpql.append("INNER JOIN Item k ON k.pkId = b.itemPkId ");
		jpql.append("WHERE k.name =:name AND k.id =:id ");
		jpql.append("AND b.isPackage =1 ");
		jpql.append("AND b.invDate  BETWEEN :bDate AND :endDate ");
		jpql.append("GROUP BY c.name,c.pkId ");
		
		List<PharmacyImformation>  imformations =  getByQuery(PharmacyImformation.class, jpql.toString(),parameters);
		for (PharmacyImformation pharmacyImformation : imformations) {
			parameters.put("subPkId", pharmacyImformation.getSubPkId());
			jpql  =  new StringBuilder();
			jpql.append("SELECT  NEW pharmacy.businessentity.PharmacyImformation(a.invDate,b,"
					+ "g.firstName,v.firstName, SUM(c.orderQty)) ");
			jpql.append("FROM InvoiceHdr a ");
			jpql.append("INNER JOIN Customer b ON a.customerPkId=b.pkId ");
			jpql.append("INNER JOIN InvoiceDtl c ON c.hdrPkId=a.pkId ");
			jpql.append("INNER JOIN Item d ON d.pkId = c.itemPkId ");
			jpql.append("INNER JOIN Employee g  ON g.pkId = a.employeePkId ");
			jpql.append("INNER JOIN Employee v  ON v.pkId = c.pharmacistPkId ");
			jpql.append("WHERE  d.name =:name AND d.id= :id ");
			jpql.append("AND a.subOrganizationPkId =:subPkId ");
			jpql.append("AND c.invDate  BETWEEN :bDate AND :endDate ");
			jpql.append("GROUP BY a.invDate,g.firstName,"
					+ "v.firstName,b ");
			List<PharmacyImformation> pharmacyImformations  =  getByQuery(PharmacyImformation.class, jpql.toString(),parameters);
			
			for (PharmacyImformation pharmacyImformation2 : pharmacyImformations) {
				pharmacyImformation.setFirstName(pharmacyImformation2.getFirstName());
				pharmacyImformation.setCustomerAge(pharmacyImformation2.getCustomerAge());
				pharmacyImformation.setCustomerGender(pharmacyImformation2.getCustomerGender());
			}
		}
		
		return imformations;
	}
	
	
	@Override
	public List<CustomerMedicineDeispense> getMedicineSubOrganizationReport(Date bDate,Date eDate) throws Exception {
		
		Date beginDate =  (Date) Tool.deepClone(bDate);
		beginDate.setHours(0);
		beginDate.setMinutes(0);
		beginDate.setSeconds(0);
		
		Date endDate =  (Date) Tool.deepClone(eDate);
		endDate.setHours(23);
		endDate.setMinutes(59);
		endDate.setSeconds(59);
		
		
		CustomHashMap  parameters =  new CustomHashMap();
		parameters.put("bDate", beginDate);
		parameters.put("eDate", endDate);
		
		StringBuilder jpql = new StringBuilder();
		
		jpql.append("SELECT NEW pharmacy.businessentity.CustomerMedicineDeispense(b.pkId,b.name,COUNT(a.pkId)) ");
		jpql.append("FROM InvoiceHdr a ");
		jpql.append("INNER JOIN InpatientHdr c ON a.inpatientHdrPkId = c.pkId ");
		jpql.append("INNER JOIN SubOrganization b ON a.subOrganizationPkId = b.pkId ");
		jpql.append("WHERE c.status=2  ");
//		jpql.append("AND a.beginDate BETWEEN :bDate AND :eDate ");
		jpql.append("GROUP BY b.name,b.pkId ");
		
		List<CustomerMedicineDeispense> deispenses =getByQuery(CustomerMedicineDeispense.class,jpql.toString(), parameters);
		for (CustomerMedicineDeispense customerMedicineDeispense : deispenses) {
			parameters.put("subPkId", customerMedicineDeispense.getSubPkId());
			jpql = new StringBuilder();
			jpql.append("SELECT NEW pharmacy.businessentity.CustomerMedicineDeispense(SUM(c.qty)) ");
			jpql.append("FROM InvoiceHdr a ");
			jpql.append("INNER JOIN InvoiceDtl c ON a.pkId = c.hdrPkId ");
			jpql.append("INNER JOIN InpatientHdr k ON a.inpatientHdrPkId = k.pkId ");
			jpql.append("WHERE c.isPackage =1 AND a.subOrganizationPkId = :subPkId ");
//			jpql.append("AND k.beginDate BETWEEN :bDate AND :eDate ");

			List<CustomerMedicineDeispense> list =  getByQuery(CustomerMedicineDeispense.class,jpql.toString(), parameters);
			if(list.size()>0){
				customerMedicineDeispense.setOrderBalance(list.get(0).getOrderBalance());
//				int b  = list.get(0).getCustomerCountDiespense();
//				double k  =  Double.parseDouble(list.get(0).getOrderBalance().toString());
//				double sum   = k/b;
//				NumberFormat format =  new DecimalFormat("#0.00");
//				customerMedicineDeispense.setAverageNumber(format.format(sum).toString());
			}
		}
		
		return deispenses;
	}
	
	@Override
	public List<PharmacyImformation> getItemReport(Date date,Date endDate) throws Exception {
		
		Date bDate = (Date) Tool.deepClone(date);
		bDate.setHours(0);
		bDate.setMinutes(0);
		bDate.setSeconds(0);
		
		Date eDate = (Date) Tool.deepClone(endDate);
		eDate.setHours(23);
		eDate.setMinutes(59);
		eDate.setSeconds(59);
		
		CustomHashMap parameters =  new CustomHashMap();
		parameters.put("medicine", Tool.MEDICINE);
		parameters.put("injection", Tool.INJECTION);

		parameters.put("beginDate", bDate);
		parameters.put("endDate", eDate);
		
		StringBuilder jpql =  new StringBuilder();
		jpql.append("SELECT NEW pharmacy.businessentity.PharmacyImformation(b.pkId,b.name) ");
		jpql.append("FROM InvoiceHdr a ");
		jpql.append("INNER JOIN SubOrganization b  ON a.subOrganizationPkId=b.pkId ");
		jpql.append("WHERE a.invDate BETWEEN :beginDate AND :endDate ");
		jpql.append("GROUP BY b.pkId,b.name");
		List<PharmacyImformation> subOrganizations  = getByQuery(PharmacyImformation.class,jpql.toString(),parameters);
		for (PharmacyImformation subOrganization : subOrganizations) {
			parameters.put("subPkId", subOrganization.getSubPkId());
			jpql = new StringBuilder();
			jpql.append("SELECT NEW pharmacy.businessentity.PharmacyImformation(c.name,c.id,d.firstName,b.isPackage,SUM(b.qty)) ");
			jpql.append("FROM InvoiceHdr a ");
			jpql.append("INNER JOIN InvoiceDtl b ON a.pkId = b.hdrPkId ");
			jpql.append("INNER JOIN Item c ON c.pkId = b.itemPkId ");
			jpql.append("INNER JOIN Employee d ON d.pkId = a.employeePkId ");
			jpql.append("WHERE (c.type=:medicine OR c.type=:injection) AND a.subOrganizationPkId = :subPkId ");
			jpql.append("AND b.isToDispense=1 ");
			jpql.append("AND b.invDate BETWEEN :beginDate AND :endDate ");
			jpql.append("GROUP BY c.name,c.id,d.firstName,b.isPackage ");
			subOrganization.setPharmacyImformations(getByQuery(PharmacyImformation.class,jpql.toString(),parameters));
		}
		return subOrganizations;
	}
}