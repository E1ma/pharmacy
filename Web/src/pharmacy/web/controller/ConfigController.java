package pharmacy.web.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;

import pharmacy.businessentity.*;
import pharmacy.businesslogic.interfaces.IConfigLogicLocal;
import pharmacy.entity.*;

@SessionScoped
@ManagedBean(name = "configController")
public class ConfigController implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{applicationController}")
	private ApplicationController applicationController;

	@ManagedProperty(value = "#{userController}")
	private UserSessionController userSessionController;
	
	@EJB(beanName = "LogicConfig")
	IConfigLogicLocal configLogic;

	public ConfigController() {
		
	}
	
	public ApplicationController getApplicationController() {
		return applicationController;
	}

	public void setApplicationController(ApplicationController applicationController) {
		this.applicationController = applicationController;
	}

	public UserSessionController getUserSessionController() {
		return userSessionController;
	}

	public void setUserSessionController(UserSessionController userSessionController) {
		this.userSessionController = userSessionController;
	}

	private List<SubOrgAccountMap> listAccountMap;
	private SubOrgAccountMap currentAccountMap;
	private List<SubOrganization> listSubOrg;
	private List<SubOrgAccountMap> tmpListMap;
	
	public void getListData()
	{
		try
		{
			listAccountMap = configLogic.getListData();
			RequestContext context = RequestContext.getCurrentInstance();
			context.update("form:listAccountMap");
		}
		catch(Exception ex)
		{
			userSessionController.showErrorMessage(ex.getMessage());
		}
	}
	
	public void saveConfig()
	{
		try
		{
			if(currentAccountMap.getStatus().equals(Tool.ADDED))
			{
				boolean warningAccount = false;
				String accountWarningMsg = "Дараахь данснууд тохируулагдсан учир өөр данс тохируулна уу. Үүнд: ";
				List<String> listAccountId = new ArrayList<String>();
				for (SubOrgAccountMap tmp : listAccountMap) {
					if(tmp.getSubOrganizationPkId().compareTo(currentAccountMap.getSubOrganizationPkId()) == 0)
						listAccountId.add(tmp.getAccountId());
				}
				for (SubOrgAccountMap tmp : tmpListMap) {
					for (String accountId : listAccountId) {
						if(accountId.equals(tmp.getAccountId()))
						{
							accountWarningMsg += tmp.getAccountId() + ", ";
							warningAccount = true;
						}
					}
				}
				boolean checkAccounts = true;
				for (SubOrgAccountMap tmp : tmpListMap) {
					if(tmp.isSelected())
						checkAccounts = false;
				}
				if(checkAccounts)
				{
					if(warningAccount)
						userSessionController.showWarningMessage(accountWarningMsg);
					else
						userSessionController.showWarningMessage("Заавал 1 данс сонгоно уу");
					return;
				}
			}
			configLogic.saveAccountConfig(currentAccountMap, tmpListMap);
			userSessionController.showSuccessMessage("Амжилттай хадгаллаа");
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('accountMapDialog').hide()");
			getListData();
		}
		catch(Exception ex)
		{
			userSessionController.showErrorMessage(ex.getMessage());
		}
	}

	public void addAccountConfig()
	{
		currentAccountMap = new SubOrgAccountMap();
		currentAccountMap.setStatus(Tool.ADDED);
		tmpListMap = null;
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('accountMapDialog').show()");
		context.update("form:accountMapDialog");
		getListData();
	}
	
	public void modifiedAccountMap(SubOrgAccountMap map)
	{
		currentAccountMap = map;
		currentAccountMap.setStatus(Tool.MODIFIED);
		List<String> listAccountId = new ArrayList<String>();
		for (SubOrgAccountMap tmp : listAccountMap) {
			if(tmp.getSubOrganizationPkId().compareTo(map.getSubOrganizationPkId()) == 0)
				listAccountId.add(tmp.getAccountId());
		}
		for (SubOrgAccountMap tmp : tmpListMap) {
			for (String accountId : listAccountId) {
				if(accountId.equals(tmp.getAccountId()))
					tmp.setSelected(true);	
			}
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('accountMapDialog').show()");
		context.update("form:accountMapDialog");
		getListData();
	}
	
	public void deleteAccountMap(SubOrgAccountMap map)
	{
		try
		{
			map.setStatus(Tool.DELETE);
			configLogic.saveAccountConfig(map, null);
			getListData();
		}
		catch(Exception ex)
		{
			userSessionController.showErrorMessage(ex.getMessage());
		}
	}

	public List<SubOrgAccountMap> getListAccountMap() {
		if(listAccountMap == null)
			getListData();
		return listAccountMap;
	}

	public void setListAccountMap(List<SubOrgAccountMap> listAccountMap) {
		this.listAccountMap = listAccountMap;
	}

	public SubOrgAccountMap getCurrentAccountMap() {
		if(currentAccountMap == null)
			currentAccountMap = new SubOrgAccountMap();
		return currentAccountMap;
	}

	public void setCurrentAccountMap(SubOrgAccountMap currentAccountMap) {
		this.currentAccountMap = currentAccountMap;
	}

	public List<SubOrganization> getListSubOrg() {
		try {
			if(listSubOrg == null)
				listSubOrg = configLogic.getSubOrganizations();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listSubOrg;
	}

	public void setListSubOrg(List<SubOrganization> listSubOrg) {
		this.listSubOrg = listSubOrg;
	}

	public List<SubOrgAccountMap> getTmpListMap() throws Exception {
		if(tmpListMap == null)
		{
			tmpListMap = configLogic.getAllAccount();
		}
		return tmpListMap;
	}

	public void setTmpListMap(List<SubOrgAccountMap> tmpListMap) {
		this.tmpListMap = tmpListMap;
	}
}