package pharmacy.web.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;

import org.primefaces.model.TreeNode;

import pharmacy.businessentity.*;
import pharmacy.businesslogic.interfaces.IPharmacyLogicLocal;
import pharmacy.entity.*;

@SessionScoped
@ManagedBean(name = "pharmacyController")
public class PharmacyController implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{applicationController}")
	private ApplicationController applicationController;

	@ManagedProperty(value = "#{userController}")
	private UserSessionController userSessionController;

	@EJB(beanName = "LogicPharmacy")
	IPharmacyLogicLocal pharmacyLogic;

	private CustomerMedicineDeispense currentMedicineDeispense;

	private BigDecimal selectedEmployeePkId;
	private BigDecimal employeePkId;
	private BigDecimal subOrganizationPkId;
	private BigDecimal typeSelectMenu;
	private BigDecimal outEmployeePkId;

	private Date outcomeDate;
	private Date beginDate;
	private Date endDate;
	private Date reportBeginDate;
	private Date reportEndDate;
	
	private Date presBeginDate;
	private Date presEndDate;
	
	
	private Date reportMedBeginDate;
	private Date reportMedEndDate;
	
	

	private String customerFilterKey = "";
	private String outcomeFilterKey;
	private String medicineActing;

	private Customer  currentCustomer;
	private TreeNode treeNode;
	private TreeNode 	pharmacistTreeNode;
	private TreeNode 	pharmacistBeforeTreeNode;
	private CustomerMedicineDeispense currentDeispense;
	private CustomerMedicineDeispense selectedDeispense;

	private List<CustomerMedicineDeispense> customerMedicineDeispenses;
	private List<CustomerMedicineDeispense> customerMedicineLsts;
	private List<CustomerMedicineDeispense> prescriptionMedicines;
	private List<CustomerMedicineDeispense> pharmasistDeispenses;
	private List<CustomerMedicineDeispense> prescriptionDeispenses;
	private List<CustomerMedicineDeispense> listDispense;
	private List<CustomerMedicineDeispense> employeeList;
	private List<Diagnose> selectCustomerDiagnoseList;
	private List<CustomerMedicineDeispense> listCustomerMedicinePharmacy;
	private List<SubOrganization> listSubOrganization;
	private List<CustomerExamination> customerExaminations;
	private List<ExaminationRequestCompleted> examinationRequestCompleteds;
	private List<ExaminationResults> examinationResult;
	private List<ExaminationRequestCompleted> currentCustomerExaminations;
	private List<CustomerMedicine> customerMedicines;
	private List<CustomerMedicineDeispense> listCustomerMedicineCt;
	private List<CustomerMedicineDeispense> listCustomerMedicineDate;
	private List<CustomerMedicineDeispense> lstMedicineDates;
	private List<CustomerMedicineDeispense> lstMedicineReports;
	private List<OutcomeInfo> outcomeList;
	private List<InspectionDetail> inspectionDetails;
	private List<VitalSign> enrVitalSigns;
	private List<CustomerMedicineDeispense> medicinePackgePrints;
	private List<CustomerMedicineDeispense> packagePrintDates;
	private List<PharmacyImformation> orderPharmacyList;
	private List<PharmacyImformation> pharmacyImformations;

	private boolean selectCheckBoxMedicine;
	private boolean pharmacyDispensePage=false;

	private int selectDateValue;
	private int totalInputValue;
	private int lastCount;
	private int showTypeOutcome;

	private boolean showRole;

	@PostConstruct
	public void init() {
		refreshMedicineOcsList();
	}
	
	
	public void reportAccept(){
		try {
			pharmacyImformations  = pharmacyLogic.getItemReport(getReportBeginDate(),getReportEndDate());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void  staticInmformationData() {
		try {
			RequestContext context =  RequestContext.getCurrentInstance();
			orderPharmacyList = pharmacyLogic.getOrderPharmacy(getReportBeginDate(),getReportEndDate());
			for (PharmacyImformation pImformation : orderPharmacyList) {
				PharmacyImformation  imformation  =  pharmacyLogic.getOrderPharmacyDispense(pImformation.getItemId(), pImformation.getItemName(), BigDecimal.ZERO,getReportBeginDate(),getReportEndDate());
				if(imformation.getItemNumber()!=null)
					pImformation.setItemNumber(imformation.getItemNumber());
				else pImformation.setItemNumber(BigDecimal.ZERO);
				
				// savlah  too 
				PharmacyImformation  packegeImformation  =  pharmacyLogic.getOrderPharmacyDispense(pImformation.getItemId(), pImformation.getItemName(), BigDecimal.ONE,getReportBeginDate(),getReportEndDate());
				if(packegeImformation.getItemNumber()!=null)
					pImformation.setItemPackage(imformation.getItemNumber());
				else pImformation.setItemPackage(BigDecimal.ZERO);
				
				// tasag  blegleh 
				pImformation.setSubOrganizations(pharmacyLogic.getOrderSubOrganization(pImformation.getItemId(), pImformation.getItemName(),getReportBeginDate(),getReportEndDate()));
			}
			
			context.update("form:pharmacyGroupId");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadDataReport(){
		try {
			lstMedicineReports = pharmacyLogic.getMedicineSubOrganizationReport(getReportMedBeginDate(),getReportMedEndDate());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void getOutcomeListData() {
		try
		{
			outcomeList = pharmacyLogic.getOutcomeInfoList(outcomeDate, outEmployeePkId, outcomeFilterKey);
		}
		catch(Exception e) {
			e.printStackTrace();
			userSessionController.showErrorMessage(e.getMessage());
		}
	}

	public void prescriptionData() {
		try {
			prescriptionDeispenses = pharmacyLogic.getCustomerMedicineOcs(BigDecimal.ONE);
		} catch (Exception e) {
			e.printStackTrace();
			getUserSessionController().setErrorMessage(e.getMessage());
		}
	}

	public void pharmacyRefresh(){
		try {
			RequestContext context = RequestContext.getCurrentInstance();
			listSubOrganization = pharmacyLogic.getSubOrganizations();
			context.update("form:pharmaId");
			context.update("form:pharmacyMedId");
		} catch (Exception e) {
			e.printStackTrace();
			userSessionController.showErrorMessage(e.getMessage());
		}
	}

	public String prescriptionModified(CustomerMedicineDeispense deispense) {
		try {
			RequestContext context  =  RequestContext.getCurrentInstance();
			currentCustomer  = new Customer();
			currentDeispense  = new CustomerMedicineDeispense();
			setCurrentCustomer(deispense.getCustomer());
			setCurrentDeispense(deispense);
			prescriptionMedicines  =  pharmacyLogic.getPresriptionMedicines(deispense.getInvPkId(),getPresBeginDate(),getPresEndDate());
			context.update("form:customerlstId");
		} catch (Exception e) {
			e.printStackTrace();
			getUserSessionController().showErrorMessage(e.getMessage());
		}
		return "presciption_list";
	}
	
	public void prescriptionLoad() {
		try {
			prescriptionMedicines  =  pharmacyLogic.getPresriptionMedicines(getCurrentDeispense().getInvPkId(),getPresBeginDate(),getPresEndDate());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

		
	// smart logic ruu  yvuulah function
	public void sendToAccountSave() {
		try {

			List<CustomerMedicineDeispense> pkIds  =  new ArrayList<>();
			List<CustomerMedicineDeispense> customerItem =  pharmacyLogic.getItemAll();
			List<CustomerMedicineDeispense> customers =  pharmacyLogic.getCustomerList();
			if(prescriptionMedicines.size()>0) {
				for (CustomerMedicineDeispense customerMedicineDeispense : prescriptionMedicines) {

					if(customerMedicineDeispense.getInvoiceDtl().getIsPackage()==1) {
						// ItemPkId
						CustomerMedicineDeispense  medicineDeispense =  new CustomerMedicineDeispense();
						for (CustomerMedicineDeispense deispense : customerItem) {
							if(customerMedicineDeispense.getMedicineId().equals(deispense.getMedicineId())) {
								medicineDeispense.setItemPkId(deispense.getMedicinePkId());
								medicineDeispense.setMedicineQty(customerMedicineDeispense.getInvoiceDtl().getQty());
							}
						}
						
						//CustomerPkId
						for (CustomerMedicineDeispense customer : customers) {
							if(getCurrentCustomer().getCardNumber().equals(customer.getCustomerCardNumber())) {
								medicineDeispense.setCustomerPkId(customer.getCustomerPkId());
							}
						}
						
						pkIds.add(medicineDeispense);
					}
				}
				if(pkIds.size()>0) {
					//tur argalav
					pharmacyLogic.sendSaveItem(pkIds);
					getUserSessionController().showSuccessMessage("Амжилттай хадгалалаа");
				}  else getUserSessionController().showErrorMessage("Хадгалах боломжгүй байна");
					
			} else getUserSessionController().showErrorMessage("Хадгалах боломжгүй байна");
			
		} catch (Exception e) {
			e.printStackTrace();
			getUserSessionController().showErrorMessage(e.getMessage());
		}
	}
	
	
	public void refreshMedicineOcsList() {
		try {
			getCustomerMedicineLsts().clear();
			RequestContext context = RequestContext.getCurrentInstance();
			employeeList = pharmacyLogic.getEmployees();
			customerMedicineDeispenses = pharmacyLogic.getCustomerMedicineOcs(BigDecimal.ZERO);
			for (CustomerMedicineDeispense deispense : customerMedicineDeispenses) {
				if(deispense.getMedicineQty()==null)
					deispense.setMedicineQty(BigDecimal.ZERO);

				CustomerMedicineDeispense medicineDeispense  =  new CustomerMedicineDeispense();
				medicineDeispense.setCustomer(deispense.getCustomer());
				medicineDeispense.setRoomNo(deispense.getRoomNo());
				medicineDeispense.setBeginDate(deispense.getBeginDate());
				medicineDeispense.setSubName(deispense.getSubName());
				medicineDeispense.setInvPkId(deispense.getInvPkId());
				medicineDeispense.setInpatientHdrPkId(deispense.getInpatientHdrPkId());
				medicineDeispense.setOrderQtySub(deispense.getOrderQty().subtract(deispense.getMedicineQty()));
				getCustomerMedicineLsts().add(medicineDeispense);
			}
		} catch (Exception e) {
			userSessionController.setErrorMessage(e.getMessage());
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	public void medicinePrint() {
		try {
			RequestContext  context =  RequestContext.getCurrentInstance();
			medicinePackgePrints =  new ArrayList<>();
			for (CustomerMedicineDeispense deispense: prescriptionMedicines) {
				if(deispense.getInvoiceDtl().getIsPackage()==1) {
					medicinePackgePrints.add(deispense);
				}
			}
			getMedicinePackgePrints();
			context.update("form:pharmacyPrintId");
			context.update("form:medicinePanelId");
		} catch (Exception e) {
			getUserSessionController().showErrorMessage(e.getMessage());
			e.printStackTrace();
		}
	}

	public void dispensePharmacy(InvoiceDtl  dtl) {
		try {
			RequestContext context =  RequestContext.getCurrentInstance();
			dtl.setStatus(Tool.ADDED);
			dtl.setPharmacistPkId(getUserSessionController().getLoggedInfo().getEmployeePkId());
			dtl.setIsToDispense((byte) 1 );
			if(dtl.getQty().compareTo(dtl.getOrderQty())>0) {
				getUserSessionController().showErrorMessage("Утга их байна");
				dtl.setDispenseCheck(false);
			} else {
				if(dtl.getOrderQty().compareTo(dtl.getQty())==0)
					dtl.setDispenseCheck(true);
				pharmacyLogic.updatePharmacy(dtl, getUserSessionController().getLoggedInfo());
				context.update("@(.dispensePanelClass)");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void customerMedicineCtPage() {
		try {
			List<Date> date = new ArrayList<>();
			listCustomerMedicineCt = pharmacyLogic
					.getCustomerMedicineCt(getCurrentCustomer().getPkId());
			for (CustomerMedicineDeispense cd : listCustomerMedicineCt) {
				System.out.println(cd.getCreateDate() + "  , ");
				date.add(cd.getCreateDate());
				for (CustomerMedicineDeispense cm : cd.getListCreateDate()) {
					System.out.println(
							cm.getMedicineM() + " -  " + cm.getMedicineD() + "  - " + cm.getMedicineE() + " -  ");
				}
			}
			listCustomerMedicineDate = pharmacyLogic
					.getCustomerMedicineDate(getCurrentCustomer().getPkId(), date);
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void dateDispense() {
		try {
			RequestContext context  =  RequestContext.getCurrentInstance();
			if(getSelectedDeispense().getInvPkId()!=null) {
				listDispense = pharmacyLogic.getPharmacyDispenseLst(getSelectedDeispense().getInvPkId(),BigDecimal.ZERO,getBeginDate(),getEndDate());
//				context.update("form:pharmacyDeispenseId");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String  modifiedMedicineDispense(CustomerMedicineDeispense deispense) {
		String ret = "";
		RequestContext context =  RequestContext.getCurrentInstance();
		try {
			if(deispense.getCustomer().getPkId()!=null) {
				currentCustomer  =  new Customer();
				setCurrentCustomer(deispense.getCustomer());
				selectedDeispense = deispense;
				// erm
				List<InspectionForm> inspectionFormFirstDtl = pharmacyLogic.getPharmacyEmr(deispense.getInpatientHdrPkId(),Tool.INPATIENTDTLSTATUS_FIRSTINSPECTION);
				List<InspectionForm> inspectionFormDailyDtl = pharmacyLogic.getPharmacyEmr(deispense.getInpatientHdrPkId(),Tool.INPATIENTDTLSTATUS_DAILYINSPECTION);

				if(inspectionFormFirstDtl.size()>0) {
					if(inspectionFormFirstDtl.get(0).getTypeDtlPkId()!=null) {
						InspectionDetail  dtl =  new InspectionDetail();
						dtl.setInspectionPkId(inspectionFormFirstDtl.get(0).getTypeDtlPkId());
						getInspectionDetails().add(dtl);
					}
					// emr json  parser

					JSONParser jsonParser = new JSONParser();
					if (getInspectionDetails().size() > 0) {
						for (InspectionDetail dtl : getInspectionDetails()) {
							for (InspectionForm form : inspectionFormFirstDtl) {
								if (dtl.getInspectionPkId().compareTo(form.getInspectionPkId()) == 0) {
									dtl.setInspectionPkId(form.getInspectionPkId());
									JSONObject object = new JSONObject();
									object.put("t", form.getInspectionType());
									object.put("v", (JSONArray) jsonParser.parse(form.getInspectionPain()));
									dtl.getPain().add(object);
									dtl.setEmployeeName(form.getEmployeeName());
									dtl.setSubName(form.getSubName());
									dtl.setDate(form.getDate());
									JSONObject checkUpObject = new JSONObject();
									checkUpObject.put("t", form.getInspectionType());
									checkUpObject.put("v", (JSONArray) jsonParser.parse(form.getInspectionCheckup()));
									dtl.getCheckup().add(checkUpObject);

									JSONObject medicalHistoryObject = new JSONObject();
									medicalHistoryObject.put("t", form.getInspectionType());
									medicalHistoryObject.put("v",
											(JSONArray) jsonParser.parse(form.getInspectionMedicalHistory()));
									dtl.getQ().add(medicalHistoryObject);
								}
							}
						}
					}
				}

				// em zuich
				getPharmasistDeispenses().clear();
				List<CustomerMedicineDeispense> customerItem =  pharmacyLogic.getItemAll();
				listDispense = pharmacyLogic.getPharmacyDispenseLst(deispense.getInvPkId(),BigDecimal.ZERO,getBeginDate(),getEndDate());
				for (CustomerMedicineDeispense customerMedicineDeispense : listDispense) {
					if(customerMedicineDeispense.getInvoiceDtl().getQty()==null)
						customerMedicineDeispense.getInvoiceDtl().setQty(BigDecimal.ZERO);
					if(customerMedicineDeispense.getInvoiceDtl().getOrderQty().compareTo(customerMedicineDeispense.getInvoiceDtl().getQty())>0 
							|| customerMedicineDeispense.getInvoiceDtl().getIsToDispense()!=1) {
						CustomerMedicineDeispense  deispense1  =  new CustomerMedicineDeispense();
						deispense1.setMedicineName(customerMedicineDeispense.getMedicineName());
						for (CustomerMedicineDeispense customerMedicineDeispense2 : customerItem) {
							if(customerMedicineDeispense2.getMedicineId().equals(customerMedicineDeispense.getMedicineId())) {
								CustomerMedicineDeispense mDeispense =  pharmacyLogic.getWarehouse(customerMedicineDeispense2.getMedicinePkId());
								deispense1.setOrderBalance(mDeispense.getOrderBalance());
							}
						}
						
						deispense1.setType(customerMedicineDeispense.getType());
						deispense1.setInvoiceDtl(customerMedicineDeispense.getInvoiceDtl());
						getPharmasistDeispenses().add(deispense1);
					}
				}


				// em nairuulagch
				//				List<CustomerMedicineDeispense> pharmacists =pharmacyLogic.getPharmacyDispenseLst(deispense.getInvPkId(),BigDecimal.ONE);
				//				pharmacistTreeNode =  new DefaultTreeNode(new CustomerMedicineDeispense(),null);
				//
				//				for (CustomerMedicineDeispense customerMedicineDeispense : pharmacists) {
				//					TreeNode tNode =  new DefaultTreeNode(customerMedicineDeispense,pharmacistTreeNode);
				//					List<CustomerMedicineDeispense> medicineDeispenses = pharmacyLogic.getPharmacyDispenseDates(deispense.getInvPkId(),BigDecimal.ZERO,customerMedicineDeispense.getInvDate());
				//					for (CustomerMedicineDeispense cusMed : medicineDeispenses) {
				//						new DefaultTreeNode(cusMed,tNode);
				//					}
				//				}
				setPharmacyDispensePage(true);

				// enr 
				setEnrVitalSigns(pharmacyLogic.getVitalSign(deispense.getInpatientHdrPkId()));

				// umnuh jor
				List<CustomerMedicineDeispense> medicineDeispenses =pharmacyLogic.getPharmacyBeforeOrder(deispense.getInpatientHdrPkId());
				pharmacistBeforeTreeNode = new DefaultTreeNode(new CustomerMedicineDeispense(),null);
				for (CustomerMedicineDeispense customerMedicineDeispense : medicineDeispenses) {
					TreeNode node =  new DefaultTreeNode(customerMedicineDeispense,pharmacistBeforeTreeNode);
					for (CustomerMedicineDeispense customerMedicineDeispense2 : customerMedicineDeispense.getListCustomerDipenses()) {
						new DefaultTreeNode(customerMedicineDeispense2,node);
					}
				}
				context.update("form:inpatientPharmacy");
				context.update("@(.pharmacyDispensePanel)");
				ret = "pharmacy_list";
			} else ret=""; 
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.getMessage());
			e.printStackTrace();
		}
		return ret;
	}


	// umnuh  jor
	public void pharmacistHover(Long time,BigDecimal pkId) {
		try {
			int timeDay =  (int) (long) time;
			CustomerMedicineDeispense  customerMedicineDeispense = pharmacyLogic.getNursingFilter(pkId,timeDay);
			setMedicineActing("тайлбар: " + customerMedicineDeispense.getMedicineDescription() +"<br/>" +
					"Эмч : " +customerMedicineDeispense.getEmployeeName()
					);
		} catch (Exception e) {
			e.printStackTrace();
			getUserSessionController().showErrorMessage(e.getMessage());
		}
	}

	// medicine dispense 
	public void pharmacyDispense(InvoiceDtl  dtl) {
		try {
			RequestContext context =  RequestContext.getCurrentInstance();
			dtl.setStatus(Tool.MODIFIED);
			dtl.setPharmacistPkId(getUserSessionController().getLoggedInfo().getEmployeePkId());
			pharmacyLogic.updatePharmacy(dtl,getUserSessionController().getLoggedInfo());
			context.update("@(.dispensePanelClass)");
		} catch (Exception e) {
			e.printStackTrace();
			getUserSessionController().showErrorMessage(e.getMessage());
		}
	}

	public void changeTotal(CustomerMedicineDeispense deispense){
		try {
			RequestContext  context   =  RequestContext.getCurrentInstance();
			if(deispense.getCountMedicineCustomerOrder()>deispense.getCustomerMedicine().getRepeatCount()){
				getUserSessionController().showErrorMessage("Хэт их  утга байна");
				deispense.setCountMedicineCustomerOrder(0);
			} else {
				CustomerMedicineDeispense dtl =  pharmacyLogic.getCustomerMedicineDtl(deispense.getCustomerMedicine().getPkId());
				int sum  = dtl.getCustomerDiespense()+deispense.getCountMedicineCustomerOrder();
				if(sum>deispense.getCustomerMedicine().getRepeatCount()){
					getUserSessionController().showErrorMessage("Утга хэт их  байна");
					deispense.setCountMedicineCustomerOrder(0);
				} else {
					deispense.setMedicineBalance(deispense.getCustomerMedicine().getRepeatCount()-sum);
					deispense.getCustomerMedicine().setDispenseCount(sum);
				}
			}
			context.update("form:pharmacyMedId");
		} catch (Exception e) {
			e.printStackTrace();
			getUserSessionController().showErrorMessage(e.getMessage());
		}
	}

	public String   updateCustomerMedicine() {
		String ret="";
		try {
			pharmacyLogic.saveCustomerMedicine(userSessionController.getLoggedInfo(), getLstMedicineDates());
			userSessionController.showSuccessMessage("Амжилттай хадгалагдлаа");
			return ret ="pharmacy_list";
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.getMessage());
			e.printStackTrace();
		}
		return ret;
	}

	public void getCustomerMedicine(CustomerMedicine customerMedicine) {

		for (CustomerMedicine cd : getCustomerMedicines()) {
			if (cd.getPkId().compareTo(customerMedicine.getPkId()) != 0) {
				getCustomerMedicines().add(customerMedicine);
				System.out.println(getCustomerMedicines().size());
			}
		}
		getCustomerMedicines().add(customerMedicine);
		System.out.println(customerMedicines.size());
	}

	private List<Integer> findExaminationValueHdr(BigDecimal questionPkId, BigDecimal requestPkId,
			List<ExaminationValueHdr> results) {
		List<Integer> index = new ArrayList<Integer>();
		for (Integer i = 0; i < results.size(); i++) {
			if (results.get(i).getRequestPkId().compareTo(requestPkId) == 0
					&& results.get(i).getQuestionPkId().compareTo(questionPkId) == 0) {
				index.add(i);
			}
		}
		return index;
	}

	public void examinationResult() {
		try {
			examinationRequestCompleteds = pharmacyLogic.getExaminationRequestCompletedByFilter(null, null,
					getCurrentCustomer().getPkId());
			getCustomerExaminations().clear();
			int index = 0;
			for (ExaminationRequestCompleted cd : examinationRequestCompleteds) {
				index = findExaminationGroup(cd.getExaminationPkId());
				if (index == -1) {
					CustomerExamination examination = new CustomerExamination();
					examination.setExaminationName(cd.getExaminationName());
					examination.setExaminationPkId(cd.getExaminationPkId());
					examination.setExaminationDate(cd.getUpdatedDate());
					getCustomerExaminations().add(examination);
				}
			}
			for (CustomerExamination exam : getCustomerExaminations()) {
				int count = 0;
				for (ExaminationRequestCompleted cd : examinationRequestCompleteds) {
					if (exam.getExaminationPkId().compareTo(cd.getExaminationPkId()) == 0)
						count++;
				}
				exam.setCount(count);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			userSessionController.showErrorMessage(e.getMessage());
			e.printStackTrace();
		}
	}

	public int findExaminationGroup(BigDecimal pkId) {
		for (int i = 0; i < getCustomerExaminations().size(); i++) {
			if (getCustomerExaminations().get(i).getExaminationPkId().equals(pkId))
				return i;
		}
		return -1;
	}

	// Даралтын  диаграмм
	public String pressureChart() {
		String series = "series: [";
		String data ="";
		String categories ="categories:[";
		String cat1 = "data:[";
		String cat2 = "data:[";
		String cat3 = "data:[";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		if (enrVitalSigns != null) {
			for (VitalSign bl : enrVitalSigns) {
				if (bl.getCreatedDate() != null) {
					categories +="'"+format.format(bl.getDate())+"',";
					if(BigDecimal.ZERO.compareTo(bl.getSystol())!=0)
						cat1 += "{y:" + bl.getSystol() + ", name:'"
								+ bl.getNurseName() + "'" + " }, ";
					if(BigDecimal.ZERO.compareTo(bl.getDiastol())!=0)
						cat2 += "{y:" + bl.getDiastol() + ", name:'"
								+ bl.getNurseName() + "'" + " }, ";
					if(BigDecimal.ZERO.compareTo(bl.getDiastol())!=0)
						cat3 += "{y:" + bl.getTemperature() + ", name:'"
								+ bl.getNurseName() + "'" + " },";
				}
			}
		}
		categories+="]";
		cat1 += "]";
		cat2 += "]";
		cat3 +="]";
		series += "{ name : 'Дээд даралт',color:'#4C0B5F'," + cat1 + ",tooltip:{valueSuffix:'мин'} },{ name:'Доод даралт' ,color:'#0105FE',"
				+ cat2 + ",tooltip:{valueSuffix:'°C'} },{name:'Халуун',color:'#000000',"+"yAxis: 1,"+cat3 + ",tooltip:{valueSuffix:'°C'}}]";
		data="xAxis:{ " + categories+"},"+series;
		return data;
	}

	public void packageMedicine(InvoiceDtl dtl) {
		try {
			//			if(dtl.getPkId()==null) {
			//				pharmacyLogic.updateDate(dtl.getInvDate(),getUserSessionController().getLoggedInfo());
			//			}
			dtl.setStatus(Tool.ADDED);
			dtl.setPrescribePkId(getUserSessionController().getLoggedInfo().getEmployeePkId());
			pharmacyLogic.updatePharmacy(dtl, getUserSessionController().getLoggedInfo());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ApplicationController getApplicationController() {
		return applicationController;
	}

	public void setApplicationController(ApplicationController applicationController) {
		this.applicationController = applicationController;
	}

	public UserSessionController getUserSessionController() {
		return userSessionController;
	}

	public void setUserSessionController(UserSessionController userSessionController) {
		this.userSessionController = userSessionController;
	}

	public String getCustomerFilterKey() {
		return customerFilterKey;
	}

	public void setCustomerFilterKey(String customerFilterKey) {
		this.customerFilterKey = customerFilterKey;
	}

	public List<CustomerMedicineDeispense> getCustomerMedicineDeispenses() {
		if (customerMedicineDeispenses == null)
			customerMedicineDeispenses = new ArrayList<>();
		return customerMedicineDeispenses;
	}

	public void setCustomerMedicineDeispenses(List<CustomerMedicineDeispense> customerMedicineDeispenses) {
		this.customerMedicineDeispenses = customerMedicineDeispenses;
	}

	public CustomerMedicineDeispense getCurrentMedicineDeispense() {
		if(currentMedicineDeispense==null)
			currentMedicineDeispense = new CustomerMedicineDeispense();
		return currentMedicineDeispense;
	}

	public void setCurrentMedicineDeispense(CustomerMedicineDeispense currentMedicineDeispense) {
		this.currentMedicineDeispense = currentMedicineDeispense;
	}

	public List<CustomerMedicineDeispense> getEmployeeList() {
		if (employeeList == null)
			employeeList = new ArrayList<>();
		return employeeList;
	}

	public void setEmployeeList(List<CustomerMedicineDeispense> employeeList) {
		this.employeeList = employeeList;
	}

	public BigDecimal getSelectedEmployeePkId() {
		if (selectedEmployeePkId == null)
			return BigDecimal.ZERO;
		return selectedEmployeePkId;
	}

	public void setSelectedEmployeePkId(BigDecimal selectedEmployeePkId) {
		this.selectedEmployeePkId = selectedEmployeePkId;
	}

	public List<Diagnose> getSelectCustomerDiagnoseList() {
		if (selectCustomerDiagnoseList == null)
			selectCustomerDiagnoseList = new ArrayList<>();
		return selectCustomerDiagnoseList;
	}

	public void setSelectCustomerDiagnoseList(List<Diagnose> selectCustomerDiagnoseList) {
		this.selectCustomerDiagnoseList = selectCustomerDiagnoseList;
	}

	public String getDateConvert(Date d) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		return format.format(d);
	}
	public String getDateConvertNotHour(Date d) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(d);
	}

	public List<CustomerMedicineDeispense> getListCustomerMedicinePharmacy() {
		if (listCustomerMedicinePharmacy == null)
			listCustomerMedicinePharmacy = new ArrayList<>();
		return listCustomerMedicinePharmacy;
	}

	public void setListCustomerMedicinePharmacy(List<CustomerMedicineDeispense> listCustomerMedicinePharmacy) {
		this.listCustomerMedicinePharmacy = listCustomerMedicinePharmacy;
	}

	public List<SubOrganization> getListSubOrganization() {
		if (listSubOrganization == null)
			listSubOrganization = new ArrayList<>();
		return listSubOrganization;
	}

	public void setListSubOrganization(List<SubOrganization> listSubOrganization) {
		this.listSubOrganization = listSubOrganization;
	}

	public BigDecimal getEmployeePkId() {
		if (employeePkId == null)
			return BigDecimal.ZERO;
		return employeePkId;
	}

	public void setEmployeePkId(BigDecimal employeePkId) {
		this.employeePkId = employeePkId;
	}

	public BigDecimal getSubOrganizationPkId() {
		if (subOrganizationPkId == null)
			subOrganizationPkId = BigDecimal.ONE;
		return subOrganizationPkId;
	}

	public void setSubOrganizationPkId(BigDecimal subOrganizationPkId) {
		this.subOrganizationPkId = subOrganizationPkId;
	}

	public boolean isSelectCheckBoxMedicine() {
		return selectCheckBoxMedicine;
	}

	public void setSelectCheckBoxMedicine(boolean selectCheckBoxMedicine) {
		this.selectCheckBoxMedicine = selectCheckBoxMedicine;
	}

	public int getSelectDateValue() {
		return selectDateValue;
	}

	public void setSelectDateValue(int selectDateValue) {
		this.selectDateValue = selectDateValue;
	}

	public int getTotalInputValue() {
		return totalInputValue;
	}

	public void setTotalInputValue(int totalInputValue) {
		this.totalInputValue = totalInputValue;
	}

	public int getLastCount() {
		return lastCount;
	}

	public void setLastCount(int lastCount) {
		this.lastCount = lastCount;
	}

	public List<ExaminationRequestCompleted> getExaminationRequestCompleteds() {
		if (examinationRequestCompleteds == null)
			examinationRequestCompleteds = new ArrayList<>();
		return examinationRequestCompleteds;
	}

	public void setExaminationRequestCompleteds(List<ExaminationRequestCompleted> examinationRequestCompleteds) {
		this.examinationRequestCompleteds = examinationRequestCompleteds;
	}

	public List<ExaminationResults> getExaminationResult() {
		if (examinationResult == null)
			examinationResult = new ArrayList<>();
		return examinationResult;
	}

	public void setExaminationResult(List<ExaminationResults> examinationResult) {
		this.examinationResult = examinationResult;
	}

	public List<ExaminationRequestCompleted> getCurrentCustomerExaminations() {
		if (currentCustomerExaminations == null)
			currentCustomerExaminations = new ArrayList<>();
		return currentCustomerExaminations;
	}

	public void setCurrentCustomerExaminations(List<ExaminationRequestCompleted> currentCustomerExaminations) {
		this.currentCustomerExaminations = currentCustomerExaminations;
	}

	public List<CustomerMedicine> getCustomerMedicines() {
		if (customerMedicines == null)
			customerMedicines = new ArrayList<>();
		return customerMedicines;
	}

	public void setCustomerMedicines(List<CustomerMedicine> customerMedicines) {
		this.customerMedicines = customerMedicines;
	}

	public List<CustomerMedicineDeispense> getListCustomerMedicineCt() {
		if (listCustomerMedicineCt == null)
			listCustomerMedicineCt = new ArrayList<>();
		return listCustomerMedicineCt;
	}

	public void setListCustomerMedicineCt(List<CustomerMedicineDeispense> listCustomerMedicineCt) {
		this.listCustomerMedicineCt = listCustomerMedicineCt;
	}

	public List<CustomerMedicineDeispense> getListCustomerMedicineDate() {
		if (listCustomerMedicineDate == null)
			listCustomerMedicineDate = new ArrayList<>();
		return listCustomerMedicineDate;
	}

	public void setListCustomerMedicineDate(List<CustomerMedicineDeispense> listCustomerMedicineDate) {
		this.listCustomerMedicineDate = listCustomerMedicineDate;
	}

	public Customer getCurrentCustomer() {
		if(currentCustomer==null)
			currentCustomer = new Customer();
		return currentCustomer;
	}

	public void setCurrentCustomer(Customer currentCustomer) {
		this.currentCustomer = currentCustomer;
	}

	public BigDecimal getTypeSelectMenu() {
		if(typeSelectMenu==null)
			return BigDecimal.ZERO;
		return typeSelectMenu;
	}

	public void setTypeSelectMenu(BigDecimal typeSelectMenu) {
		this.typeSelectMenu = typeSelectMenu;
	}

	public List<CustomerMedicineDeispense> getLstMedicineDates() {
		if(lstMedicineDates==null)
			lstMedicineDates =new ArrayList<>();
		return lstMedicineDates;
	}

	public void setLstMedicineDates(List<CustomerMedicineDeispense> lstMedicineDates) {
		this.lstMedicineDates = lstMedicineDates;
	}

	public List<CustomerExamination> getCustomerExaminations() {
		if (customerExaminations == null)
			customerExaminations = new ArrayList<>();
		return customerExaminations;
	}

	public void setCustomerExaminations(List<CustomerExamination> customerExaminations) {
		this.customerExaminations = customerExaminations;
	}

	public List<OutcomeInfo> getOutcomeList() {
		return outcomeList;
	}

	public void setOutcomeList(List<OutcomeInfo> outcomeList) {
		this.outcomeList = outcomeList;
	}

	public int getShowTypeOutcome() {
		return showTypeOutcome;
	}

	public void setShowTypeOutcome(int showTypeOutcome) {
		this.showTypeOutcome = showTypeOutcome;
	}

	public BigDecimal getOutEmployeePkId() {
		return outEmployeePkId;
	}

	public void setOutEmployeePkId(BigDecimal outEmployeePkId) {
		this.outEmployeePkId = outEmployeePkId;
	}

	public Date getOutcomeDate() {
		if(outcomeDate == null)
			outcomeDate = new Date();
		return outcomeDate;
	}

	public void setOutcomeDate(Date outcomeDate) {
		this.outcomeDate = outcomeDate;
	}

	public String getOutcomeFilterKey() {
		return outcomeFilterKey;
	}

	public void setOutcomeFilterKey(String outcomeFilterKey) {
		this.outcomeFilterKey = outcomeFilterKey;
	}

	public boolean isPharmacyDispensePage() {
		return pharmacyDispensePage;
	}

	public void setPharmacyDispensePage(boolean pharmacyDispensePage) {
		this.pharmacyDispensePage = pharmacyDispensePage;
	}

	public TreeNode getTreeNode() {
		if(treeNode==null)
			treeNode = new DefaultTreeNode();
		return treeNode;
	}

	public void setTreeNode(TreeNode treeNode) {
		this.treeNode = treeNode;
	}

	public List<InspectionDetail> getInspectionDetails() {
		if(inspectionDetails==null)
			inspectionDetails = new ArrayList<>();
		return inspectionDetails;
	}

	public void setInspectionDetails(List<InspectionDetail> inspectionDetails) {
		this.inspectionDetails = inspectionDetails;
	}

	public TreeNode getPharmacistTreeNode() {
		if(pharmacistTreeNode==null)
			pharmacistTreeNode =new DefaultTreeNode();
		return pharmacistTreeNode;
	}

	public void setPharmacistTreeNode(TreeNode pharmacistTreeNode) {
		this.pharmacistTreeNode = pharmacistTreeNode;
	}

	public boolean isShowRole() {
		return showRole;
	}

	public void setShowRole(boolean showRole) {
		this.showRole = showRole;
	}

	public List<VitalSign> getEnrVitalSigns() {
		if(enrVitalSigns==null)
			enrVitalSigns =new ArrayList<>();
		return enrVitalSigns;
	}

	public void setEnrVitalSigns(List<VitalSign> enrVitalSigns) {
		this.enrVitalSigns = enrVitalSigns;
	}

	public TreeNode getPharmacistBeforeTreeNode() {
		if(pharmacistBeforeTreeNode==null)
			pharmacistBeforeTreeNode = new DefaultTreeNode();
		return pharmacistBeforeTreeNode;
	}

	public void setPharmacistBeforeTreeNode(TreeNode pharmacistBeforeTreeNode) {
		this.pharmacistBeforeTreeNode = pharmacistBeforeTreeNode;
	}

	public String getMedicineActing() {
		return medicineActing;
	}

	public void setMedicineActing(String medicineActing) {
		this.medicineActing = medicineActing;
	}

	public List<CustomerMedicineDeispense> getCustomerMedicineLsts() {
		if(customerMedicineLsts==null)
			customerMedicineLsts = new ArrayList<>();
		return customerMedicineLsts;
	}

	public void setCustomerMedicineLsts(List<CustomerMedicineDeispense> customerMedicineLsts) {
		this.customerMedicineLsts = customerMedicineLsts;
	}

	public List<CustomerMedicineDeispense> getPharmasistDeispenses() {
		if(pharmasistDeispenses==null)
			pharmasistDeispenses = new ArrayList<>();
		return pharmasistDeispenses;
	}

	public void setPharmasistDeispenses(List<CustomerMedicineDeispense> pharmasistDeispenses) {
		this.pharmasistDeispenses = pharmasistDeispenses;
	}

	public List<CustomerMedicineDeispense> getPrescriptionDeispenses() {
		if(prescriptionDeispenses==null)
			prescriptionDeispenses = new ArrayList<>();
		return prescriptionDeispenses;
	}

	public void setPrescriptionDeispenses(List<CustomerMedicineDeispense> prescriptionDeispenses) {
		this.prescriptionDeispenses = prescriptionDeispenses;
	}
	public List<CustomerMedicineDeispense> getPrescriptionMedicines() {
		if(prescriptionMedicines==null)
			prescriptionMedicines =new ArrayList<>();
		return prescriptionMedicines;
	}
	public void setPrescriptionMedicines(List<CustomerMedicineDeispense> prescriptionMedicines) {
		this.prescriptionMedicines = prescriptionMedicines;
	}

	public CustomerMedicineDeispense getCurrentDeispense() {
		if(currentDeispense==null)
			currentDeispense = new CustomerMedicineDeispense();
		return currentDeispense;
	}

	public void setCurrentDeispense(CustomerMedicineDeispense currentDeispense) {
		this.currentDeispense = currentDeispense;
	}
	
	public List<CustomerMedicineDeispense> getMedicinePackgePrints() {
		if(medicinePackgePrints==null)
			medicinePackgePrints = new ArrayList<>();
		return medicinePackgePrints;
	}
	
	public void setMedicinePackgePrints(List<CustomerMedicineDeispense> medicinePackgePrints) {
		this.medicinePackgePrints = medicinePackgePrints;
	}
	
	public List<CustomerMedicineDeispense> getPackagePrintDates() {
		if(packagePrintDates==null)
			packagePrintDates = new ArrayList<>();
		return packagePrintDates;
	}
	
	public void setPackagePrintDates(List<CustomerMedicineDeispense> packagePrintDates) {
		this.packagePrintDates = packagePrintDates;
	}

	public Date getBeginDate() {
		if(beginDate==null)
			beginDate =new Date();
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		if(endDate==null)
			endDate = new Date();
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<CustomerMedicineDeispense> getListDispense() {
		if(listDispense==null)
			listDispense =new ArrayList<>();
		return listDispense;
	}

	public void setListDispense(List<CustomerMedicineDeispense> listDispense) {
		this.listDispense = listDispense;
	}

	public CustomerMedicineDeispense getSelectedDeispense() {
		if(selectedDeispense==null)
			selectedDeispense =new CustomerMedicineDeispense();
		return selectedDeispense;
	}

	public void setSelectedDeispense(CustomerMedicineDeispense selectedDeispense) {
		this.selectedDeispense = selectedDeispense;
	}

	public List<PharmacyImformation> getOrderPharmacyList() {
		if(orderPharmacyList==null)
			orderPharmacyList = new ArrayList<>();
		return orderPharmacyList;
	}

	public void setOrderPharmacyList(List<PharmacyImformation> orderPharmacyList) {
		this.orderPharmacyList = orderPharmacyList;
	}

	public Date getReportBeginDate() {
		if(reportBeginDate==null) {
			Date bDate  = new Date();
			Date mDate =  new Date(bDate.getYear(),bDate.getMonth(),1);
			reportBeginDate  = mDate;
		}
			
		return reportBeginDate;
	}

	public void setReportBeginDate(Date reportBeginDate) {
		this.reportBeginDate = reportBeginDate;
	}

	public Date getReportEndDate() {
		if(reportEndDate==null) {
			reportEndDate = new Date();
		}
			
		return reportEndDate;
	}

	public void setReportEndDate(Date reportEndDate) {
		this.reportEndDate = reportEndDate;
	}

	public Date getPresBeginDate() {
		if(presBeginDate==null)
			presBeginDate = new Date();
		return presBeginDate;
	}

	public void setPresBeginDate(Date presBeginDate) {
		this.presBeginDate = presBeginDate;
	}

	public Date getPresEndDate() {
		if(presEndDate==null)
			presEndDate = new Date();
		return presEndDate;
	}

	public void setPresEndDate(Date presEndDate) {
		this.presEndDate = presEndDate;
	}


	public List<CustomerMedicineDeispense> getLstMedicineReports() {
		if(lstMedicineReports==null)
			lstMedicineReports = new ArrayList<>();
		return lstMedicineReports;
	}


	public void setLstMedicineReports(List<CustomerMedicineDeispense> lstMedicineReports) {
		this.lstMedicineReports = lstMedicineReports;
	}


	public Date getReportMedBeginDate() {
		if (reportMedBeginDate==null) {
			reportMedBeginDate = new Date();
		}
		return reportMedBeginDate;
	}


	public void setReportMedBeginDate(Date reportMedBeginDate) {
		this.reportMedBeginDate = reportMedBeginDate;
	}


	public Date getReportMedEndDate() {
		if(reportMedEndDate==null)
			reportMedEndDate = new Date();
		return reportMedEndDate;
	}


	public void setReportMedEndDate(Date reportMedEndDate) {
		this.reportMedEndDate = reportMedEndDate;
	}


	public List<PharmacyImformation> getPharmacyImformations() {
		if(pharmacyImformations==null)
			pharmacyImformations =  new ArrayList<>();
		return pharmacyImformations;
	}


	public void setPharmacyImformations(List<PharmacyImformation> pharmacyImformations) {
		this.pharmacyImformations = pharmacyImformations;
	}
	
	
	
	
}