package pharmacy.web.controller;

import pharmacy.businessentity.*;
import pharmacy.businesslogic.interfaces.IConfigLogicLocal;
import pharmacy.businesslogic.interfaces.IInfoLogicLocal;
import pharmacy.businesslogic.interfaces.ILogicTwoLocal;
import pharmacy.entity.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;

import base.*;
import mondrian.rolap.BitKey.Big;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.primefaces.context.RequestContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SessionScoped
@ManagedBean(name = "infoController")
public class InfoController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB(beanName = "InfoLogic")
	IInfoLogicLocal infoLogic;

	@ManagedProperty(value = "#{userController}")
	private UserSessionController userSessionController;

	@ManagedProperty(value = "#{applicationController}")
	private ApplicationController applicationController;

	@EJB(beanName = "LogicTwo")
	ILogicTwoLocal logicTwo;

	@EJB(beanName = "LogicConfig")
	IConfigLogicLocal logicConfig;

	public ApplicationController getApplicationController() {
		return applicationController;
	}

	public void setApplicationController(
			ApplicationController applicationController) {
		this.applicationController = applicationController;
	}

	private List<SubOrganizationType> subOrganizationTypee;
	private List<Medicine> medicines;
	private List<Injection> injections;
	private List<View_ConstantATC> atcs;
	private List<View_ConstantMedicineType> mTypes;
	private List<Measurement> measurements;
	private List<Item> listItem;

	// Cursor
	private Organization currentOrganization;
	private Aimag currentAimag;
	private Users currentUser;
	private Employee currentEmployee;
	private SubOrganization currentSubOrganization;
	private Role currentRole;
	private Medicine currentMedicine;
	private Injection currentInjection;
	private List<MedicinePrice> medicinePriceList;

	private boolean updateSums = false;
	private Message message;
	private TreeNode treeRoles;
	private TreeNode[] selectedNodes;
	private int[] inspectionTimeInterval = { 15, 30, 45, 60, 75, 90, 105, 120,
			135, 150, 165, 180 };
	private String filterKey = "";
	private String filterKey1 = "";
	private String filterNameKey = "";
	private String filterUsageType = "All";
	private byte filterActiveMed = 1;
	private int filterByMonth = 0;
	private boolean filterHasDR = false;
	private int filterDrugPurpose;

	private BigDecimal filterPkId;
	private BigDecimal filterActPkId;
	private BigDecimal selectedPkId;

	//MedicineWarning
	private List<Medicine> notUsedTogetherMedicineList;
	private boolean notUsedTogetherMedicineListCheck;
	private List<String> notUseGroup;
	private boolean notUseGroupCheck;
	private PregnantWarning pregnantWarning;
	private boolean pregnantWarningCheck;
	private WarningAge warningAge;
	private boolean warningAgeCheck;
	private WarningMedicineMaxDay warningMedicineMaxDay;
	private boolean warningMedicineMaxDayCheck;
	private WarningMedicineDose warningMedicineDose;
	private boolean warningMedicineDoseCheck;
	private List<Medicine> listMedicine;
	private Medicine selMedicine;
	private List<String> listMedicineGroup;
	private String selectedGroup;
	private List<Medicine> medicineByGroup;

	private Date medicineUsageDate;

	private StreamedContent file;

	public InfoController() {

	}

	public void getInjectionList(){
		try {
			injections = infoLogic.getInjection(getFilterActPkId(),getUserSessionController().getLoggedInfo(),getFilterNameKey());
		} catch (Exception e) {
			e.printStackTrace();
			userSessionController.showErrorMessage(e.getMessage());
		}
	}

	public String modifiedInjection(Injection  injection){
		String ret="";
		try {
			injection.setStatus(Tool.MODIFIED);
			setCurrentInjection(injection);
			ret="injection_register";
		} catch (Exception e) {
			userSessionController.showErrorMessage(e.getMessage());
			e.printStackTrace();
		}
		return  ret;
	}

	public void  getMedicinePriceList(BigDecimal pkId){
		try {
			medicinePriceList  = infoLogic.getMedicinePrice(pkId);
		} catch (Exception e) {
			// TODO: handle exception
			userSessionController.showErrorMessage(e.getMessage());
			e.printStackTrace();
		}
	}

	public void refreshMedicineList() {
		try {
			medicines =  infoLogic.getMedicineForList(getFilterUsageType(),
					userSessionController.getLoggedInfo(), getFilterKey(), getFilterActiveMed(), filterByMonth, filterHasDR, filterDrugPurpose);
			List<Object[]> objects  = infoLogic.getConnectionPharmacy("EXEC GetInfo @merchantId = '001', @password = 'merchant00!', @objectId = 'Item'");
			List<Object[]> warehouselst  = infoLogic.getConnectionPharmacy("EXEC GetInfo @merchantId = '001', @password = 'merchant00!', @objectId = 'Warehouse'");
			for (Object[] objects2 : objects) {
				for (Medicine med : medicines) {
					if(med.getId().equals(objects2[0].toString())) {
						List<CustomerMedicineDeispense> customerMedicineDeispenses = infoLogic.getItemWharehouse(new BigDecimal(objects2[1].toString()));
						List<CustomerMedicineDeispense> pharmacyMedicineName =  new ArrayList<>();
						for (Object[] object : warehouselst) {
							for (CustomerMedicineDeispense customerMedicineDeispense : customerMedicineDeispenses) {
								if(customerMedicineDeispense.getWarehousePkId().compareTo(new BigDecimal(object[1].toString()))==0) {
									CustomerMedicineDeispense  medicineDeispense  = new CustomerMedicineDeispense();
									medicineDeispense.setMedicineName(object[2].toString());
									pharmacyMedicineName.add(medicineDeispense);
								}
							}
						}
						med.setPharmacyMedicineName(pharmacyMedicineName);
					}
				}
			}
			
			
		} catch (Exception ex) {
			userSessionController.showErrorMessage(ex.getMessage());
		}
	}

	public void refreshItemList() {
		try {
			listItem = infoLogic.getItemList();
		} catch (Exception ex) {
			userSessionController.showErrorMessage(ex.getMessage());
		}
	}

	public void loadItem() throws Exception{
		if(listItem == null)
			listItem = new ArrayList<>();
		listItem.addAll(logicConfig.getAllItem());
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("form:itemList");
	}

	public StreamedContent getFile() {
		InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resource/MedicineExcelTemplate.xls");
		file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "MedicineExcelTemplate.xls");
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	//ЭМИЙН МЭДЭЭЛЭЛ ЭКСЭЛЭЭС ТАТАХ
	public void fileUploadListener(FileUploadEvent event) throws IOException{
		try
		{
			FileInputStream fis= (FileInputStream) event.getFile().getInputstream();
			HSSFWorkbook wb = new HSSFWorkbook(fis);
			HSSFSheet sheet = wb.getSheetAt(0);
			FormulaEvaluator formulaEvaluator = wb.getCreationHelper().createFormulaEvaluator();
			int i = 0;
			List<Medicine> lstMedicine = new ArrayList<Medicine>();
			List<String> listMedId = new ArrayList<String>();
			for(Row row: sheet){
				if(i == 0)
				{
					i++;
					continue;
				}
				Medicine medicine = new Medicine();
				for(Cell cell: row){
					switch(formulaEvaluator.evaluateInCell(cell).getCellType()){
					case Cell.CELL_TYPE_NUMERIC:
						//customer.setBloodType(((int)cell.getNumericCellValue()) + "");
						break;
					case Cell.CELL_TYPE_STRING:
						if(cell.getColumnIndex() == 0)
						{
							medicine.setId(cell.getStringCellValue());
							listMedId.add(cell.getStringCellValue());
						}
						else if(cell.getColumnIndex() == 1)
							medicine.setName(cell.getStringCellValue());
						else if(cell.getColumnIndex() == 2)
							medicine.setiName(cell.getStringCellValue());
						else if(cell.getColumnIndex() == 6)
							medicine.setDose(cell.getStringCellValue());
						else if(cell.getColumnIndex() == 7)
							medicine.setDayDose(cell.getStringCellValue());
						break;
					}
				}
				if(medicine.getId() != null)
					lstMedicine.add(medicine);
			}
			BigDecimal newPkId = Tools.newPkId();
			List<Medicine> listMed = infoLogic.getMedicineByListId(listMedId);
			for (Medicine tmp : lstMedicine) {
				BigDecimal customerPkId = BigDecimal.ZERO;
				for (Medicine med : listMed) {
					if(tmp.getId().equals(med.getId()))
						customerPkId = med.getPkId();
				}
				if(customerPkId.compareTo(BigDecimal.ZERO) != 0)
				{
					userSessionController.showErrorMessage(String.format("%s кодтой эм системд бүртгэлтэй байна.", tmp.getId()));
					return;
				}
				newPkId = newPkId.add(BigDecimal.ONE);
				tmp.setPkId(newPkId);
			}
			infoLogic.saveMedicineByList(lstMedicine);
			//refreshMedicineList();
			userSessionController.showSuccessMessage("Амжилттай хадгаллаа");
		}catch (Exception ex) {
			userSessionController.showErrorMessage(ex.getMessage());
		}
	}

	public void setUserSessionController(
			UserSessionController userSessionController) {
		this.userSessionController = userSessionController;
	}

	public UserSessionController getUserSessionController() {
		return userSessionController;
	}

	public void setStmpImageDefault() {
		currentOrganization.setStmp(Tool.UserDefaultImage);
	}

	public void setLogoImageDefault() {
		currentOrganization.setLogo(Tool.UserDefaultImage);
	}

	public List<Medicine> getMedicines() {
		if (medicines == null)
			medicines = new ArrayList<Medicine>();
		return medicines;
	}

	public void setMedicines(List<Medicine> medicines) {
		this.medicines = medicines;
	}

	public void setCurrentMedicine(Medicine currentMedicine) {
		this.currentMedicine = currentMedicine;
	}

	public Medicine getCurrentMedicine() {
		if (currentMedicine == null)
			currentMedicine = new Medicine();
		return currentMedicine;
	}

	public void newMedicine() {
		currentMedicine = new Medicine();
		currentMedicine.setMaxAge(150);
		currentMedicine.setStatus(Tool.ADDED);
	}

	public void  newInjection(){
		currentInjection  =  new Injection();
		currentInjection.setStatus(Tool.ADDED);
	}

	public String  saveInjection(){
		String ret="";
		try {
			if(currentInjection.getName()==null || currentInjection.getName().isEmpty() ||
					"".equals(currentInjection.getName()) || " ".equals(currentInjection.getName())){
				userSessionController.showErrorMessage("Injection Name Empty  ERROR ");
				return  ret;
			}
			else if(currentInjection.getId()==null || currentInjection.getId().isEmpty() ||
					"".equals(currentInjection.getId()) || " ".equals(currentInjection.getId())){
				userSessionController.showErrorMessage("Injection Code Empty  ERROR ");
				return  ret;
			}
			else if(currentInjection.getiName()==null || currentInjection.getiName().isEmpty() ||
					"".equals(currentInjection.getiName()) || " ".equals(currentInjection.getiName())){
				userSessionController.showErrorMessage("Injection  non-national Counrty  Name Empty  ERROR");
				return  ret;
			}
			infoLogic.saveInjection(userSessionController.getLoggedInfo(), currentInjection);
			ret="drug_injection";

		} catch (Exception e) {
			// TODO: handle exception
			userSessionController.showErrorMessage(e.getMessage() + "save Error");
			e.printStackTrace();
		}
		return  ret;
	}

	public String saveMedicine() {
		String ret = "";
		try {
			if (currentMedicine.getName().isEmpty()
					|| currentMedicine.getName() == null
					|| "".equals(currentMedicine.getName())
					|| " ".equals(currentMedicine.getName())) {
				userSessionController.showMessage(40);
				return ret;
			} else if (currentMedicine.getiName().isEmpty()
					|| currentMedicine.getiName() == null
					|| "".equals(currentMedicine.getiName())
					|| " ".equals(currentMedicine.getiName())) {
				userSessionController.showMessage(41);
				return ret;
			} else if (currentMedicine.getId().isEmpty()
					|| currentMedicine.getId() == null
					|| "".equals(currentMedicine.getId())
					|| " ".equals(currentMedicine.getId())) {
				userSessionController.showMessage(42);
				return ret;
			} else if(currentMedicine.getPrice()==null	
					||  "".equals(currentMedicine.getPrice()) 
					|| " ".equals(currentMedicine.getPrice())){
				userSessionController.showMessage(101);
				return ret;
			} else if(currentMedicine.getUsageDate()== null 
					||  "".equals(currentMedicine.getUsageDate()) 
					||  " ".equals(currentMedicine.getUsageDate())){
				userSessionController.showMessage(82);
				return  ret;
			} else if(currentMedicine.getStatus().equals(Tool.ADDED) && currentMedicine.getUsageDate().before(new Date())){
				userSessionController.showMessage(83);
				return  ret;
			}
			if (currentMedicine.isActiveAllAge()) {
				currentMedicine.setMinAge(0);
				currentMedicine.setMaxAge(150);

			} else if (currentMedicine.getMinAge() > currentMedicine
					.getMaxAge()) {
				int temp = currentMedicine.getMinAge();
				currentMedicine.setMinAge(currentMedicine.getMaxAge());
				currentMedicine.setMaxAge(temp);

			}
			infoLogic.saveMedicine(currentMedicine, notUsedTogetherMedicineListCheck, getNotUsedTogetherMedicineList(), pregnantWarningCheck, getPregnantWarning(), warningAgeCheck, getWarningAge(), warningMedicineMaxDayCheck, getWarningMedicineMaxDay(), warningMedicineDoseCheck, getWarningMedicineDose(), notUseGroupCheck, getNotUseGroup(), userSessionController.getLoggedInfo(), medicineUsageDate);
			notUsedTogetherMedicineList = null;
			pregnantWarning = null;
			warningAge = null;
			warningMedicineMaxDay = null;
			warningMedicineDose = null;
			notUsedTogetherMedicineListCheck = false;
			notUseGroup = null;
			notUseGroupCheck = false;
			pregnantWarningCheck = false;
			warningAgeCheck = false;
			warningMedicineMaxDayCheck = false;
			warningMedicineDoseCheck = false;


			getApplicationController().setWarningMedicineDoses(null);
			getApplicationController().getWarningMedicineDoses();
			getApplicationController().setWarningMedicineMaxDays(null);
			getApplicationController().getWarningMedicineMaxDays();
			getApplicationController().setListWarningAge(null);
			getApplicationController().getListWarningAge();
			getApplicationController().setPregnantWarningMedicineMap(null);
			getApplicationController().setNotUserTogetherMedicineMap(null);
			getApplicationController().setMapWarningMedicineGroup(null);
			ret = "medicine_list";
		} catch (Exception ex) {
			userSessionController.showErrorMessage(ex.getMessage());
		}
		return ret;
	}

	public void selectMed(){
		if(selMedicine != null){
			selMedicine.setMessage(getCurrentMedicine().getName() + " эмийг " + selMedicine.getName() + " эмтэй хамт хэрэглэж болохгүй.");
			getNotUsedTogetherMedicineList().add(selMedicine);
		}
	}

	public void changeMedicineGroup(){
		try{
			medicineByGroup = infoLogic.getListMedicineByGroup(selectedGroup);
		}catch(Exception ex){
			getUserSessionController().showErrorMessage(ex.getMessage());
		}
	}

	public String modifiedMedicine(Medicine medicine) {
		medicine.setStatus(Tool.MODIFIED);
		setCurrentMedicine(medicine);
		try{
			medicineUsageDate = medicine.getUsageDate();
			notUseGroup = infoLogic.getNotUseGroup(medicine.getPkId());
			notUsedTogetherMedicineList = infoLogic.getNotUsedTogetherMedicineList(medicine.getPkId());
			pregnantWarning = infoLogic.getPregnantWarning(medicine.getPkId());
			warningAge = infoLogic.getWarningAge(medicine.getPkId());
			warningMedicineMaxDay = infoLogic.getWarningMedicineMaxDay(medicine.getPkId());
			warningMedicineDose = infoLogic.getWarningMedicineDose(medicine.getPkId());
			notUsedTogetherMedicineListCheck = true;
			if(notUsedTogetherMedicineList == null || notUsedTogetherMedicineList.size() < 1) notUsedTogetherMedicineListCheck = false;
			pregnantWarningCheck = true;
			if(pregnantWarning == null) pregnantWarningCheck = false;
			warningAgeCheck = true;
			if(warningAge == null) warningAgeCheck = false;
			warningMedicineMaxDayCheck = true;
			if(warningMedicineMaxDay == null) warningMedicineMaxDayCheck = false;
			warningMedicineDoseCheck = true;
			if(warningMedicineDose == null) warningMedicineDoseCheck = false;
			notUseGroupCheck = true;
			if(notUseGroup == null || notUseGroup.size() < 1) notUseGroupCheck = false;
		}catch(Exception ex){
			getUserSessionController().showErrorMessage(ex.getMessage());
		}
		return "medicine_register";
	}

	public List<Medicine> getNotUsedTogetherMedicineList() {
		if(notUsedTogetherMedicineList == null) notUsedTogetherMedicineList = new ArrayList<>();
		return notUsedTogetherMedicineList;
	}

	public void setNotUsedTogetherMedicineList(List<Medicine> notUsedTogetherMedicineList) {
		this.notUsedTogetherMedicineList = notUsedTogetherMedicineList;
	}

	public PregnantWarning getPregnantWarning() {
		if(pregnantWarning == null) pregnantWarning = new PregnantWarning();
		return pregnantWarning;
	}

	public void setPregnantWarning(PregnantWarning pregnantWarning) {
		this.pregnantWarning = pregnantWarning;
	}

	public WarningAge getWarningAge() {
		if(warningAge == null) warningAge = new WarningAge();
		return warningAge;
	}

	public void setWarningAge(WarningAge warningAge) {
		this.warningAge = warningAge;
	}

	public WarningMedicineMaxDay getWarningMedicineMaxDay() {
		if(warningMedicineMaxDay == null) warningMedicineMaxDay = new WarningMedicineMaxDay();
		return warningMedicineMaxDay;
	}

	public void setWarningMedicineMaxDay(WarningMedicineMaxDay warningMedicineMaxDay) {
		this.warningMedicineMaxDay = warningMedicineMaxDay;
	}

	public WarningMedicineDose getWarningMedicineDose() {
		if(warningMedicineDose == null) warningMedicineDose = new WarningMedicineDose();
		return warningMedicineDose;
	}

	public void setWarningMedicineDose(WarningMedicineDose warningMedicineDose) {
		this.warningMedicineDose = warningMedicineDose;
	}

	public List<Medicine> getListMedicine() {
		if(listMedicine == null) {
			try{
				//listMedicine = logicTwo.getListMedicine();
			}catch(Exception ex){
				getUserSessionController().showErrorMessage(ex.getMessage());
			}
		}
		return listMedicine;
	}

	public void setListMedicine(List<Medicine> listMedicine) {
		this.listMedicine = listMedicine;
	}

	public Medicine getSelMedicine() {
		return selMedicine;
	}

	public void setSelMedicine(Medicine selMedicine) {
		this.selMedicine = selMedicine;
	}

	public Date getMedicineUsageDate() {
		return medicineUsageDate;
	}

	public void setMedicineUsageDate(Date medicineUsageDate) {
		this.medicineUsageDate = medicineUsageDate;
	}

	public boolean isNotUsedTogetherMedicineListCheck() {
		return notUsedTogetherMedicineListCheck;
	}

	public void setNotUsedTogetherMedicineListCheck(boolean notUsedTogetherMedicineListCheck) {
		this.notUsedTogetherMedicineListCheck = notUsedTogetherMedicineListCheck;
	}

	public boolean isPregnantWarningCheck() {
		return pregnantWarningCheck;
	}

	public void setPregnantWarningCheck(boolean pregnantWarningCheck) {
		this.pregnantWarningCheck = pregnantWarningCheck;
	}

	public boolean isWarningAgeCheck() {
		return warningAgeCheck;
	}

	public void setWarningAgeCheck(boolean warningAgeCheck) {
		this.warningAgeCheck = warningAgeCheck;
	}

	public boolean isWarningMedicineMaxDayCheck() {
		return warningMedicineMaxDayCheck;
	}

	public void setWarningMedicineMaxDayCheck(boolean warningMedicineMaxDayCheck) {
		this.warningMedicineMaxDayCheck = warningMedicineMaxDayCheck;
	}

	public boolean isWarningMedicineDoseCheck() {
		return warningMedicineDoseCheck;
	}

	public void setWarningMedicineDoseCheck(boolean warningMedicineDoseCheck) {
		this.warningMedicineDoseCheck = warningMedicineDoseCheck;
	}

	public List<String> getNotUseGroup() {
		if(notUseGroup == null) notUseGroup = new ArrayList<>();
		return notUseGroup;
	}

	public void setNotUseGroup(List<String> notUseGroup) {
		this.notUseGroup = notUseGroup;
	}

	public boolean isNotUseGroupCheck() {
		return notUseGroupCheck;
	}

	public void setNotUseGroupCheck(boolean notUseGroupCheck) {
		this.notUseGroupCheck = notUseGroupCheck;
	}

	public void setListMedicineGroup(List<String> listMedicineGroup) {
		this.listMedicineGroup = listMedicineGroup;
	}

	public String getSelectedGroup() {
		return selectedGroup;
	}

	public void setSelectedGroup(String selectedGroup) {
		this.selectedGroup = selectedGroup;
	}

	public List<Medicine> getMedicineByGroup() {
		return medicineByGroup;
	}

	public void setMedicineByGroup(List<Medicine> medicineByGroup) {
		this.medicineByGroup = medicineByGroup;
	}

	public String getFilterKey() {
		return filterKey;
	}

	public void setFilterKey(String filterKey) {
		this.filterKey = filterKey;
	}

	public String getFilterKey1() {
		return filterKey1;
	}

	public void setFilterKey1(String filterKey1) {
		this.filterKey1 = filterKey1;
	}

	public String getFilterNameKey() {
		return filterNameKey;
	}

	public void setFilterNameKey(String filterNameKey) {
		this.filterNameKey = filterNameKey;
	}

	public String getFilterUsageType() {
		return filterUsageType;
	}

	public void setFilterUsageType(String filterUsageType) {
		this.filterUsageType = filterUsageType;
	}

	public byte getFilterActiveMed() {
		return filterActiveMed;
	}

	public void setFilterActiveMed(byte filterActiveMed) {
		this.filterActiveMed = filterActiveMed;
	}

	public int getFilterByMonth() {
		return filterByMonth;
	}

	public void setFilterByMonth(int filterByMonth) {
		this.filterByMonth = filterByMonth;
	}

	public boolean isFilterHasDR() {
		return filterHasDR;
	}

	public void setFilterHasDR(boolean filterHasDR) {
		this.filterHasDR = filterHasDR;
	}

	public int getFilterDrugPurpose() {
		return filterDrugPurpose;
	}

	public void setFilterDrugPurpose(int filterDrugPurpose) {
		this.filterDrugPurpose = filterDrugPurpose;
	}

	public BigDecimal getFilterPkId() {
		return filterPkId;
	}

	public void setFilterPkId(BigDecimal filterPkId) {
		this.filterPkId = filterPkId;
	}

	public BigDecimal getFilterActPkId() {
		return filterActPkId;
	}

	public void setFilterActPkId(BigDecimal filterActPkId) {
		this.filterActPkId = filterActPkId;
	}

	public List<MedicinePrice> getMedicinePriceList() {
		if(medicinePriceList==null)
			medicinePriceList  =  new ArrayList<>();
		return medicinePriceList;
	}

	public void setMedicinePriceList(List<MedicinePrice> medicinePriceList) {
		this.medicinePriceList = medicinePriceList;
	}

	public void setAtcs(List<View_ConstantATC> atcs) {
		this.atcs = atcs;
	}

	public List<View_ConstantATC> getAtcs() {
		if (atcs == null)
			try {
				atcs = infoLogic.getAtcs();
			} catch (Exception ex) {
				userSessionController.showErrorMessage(ex.getMessage());
			}
		return atcs;
	}

	public void setmTypes(List<View_ConstantMedicineType> mTypes) {
		this.mTypes = mTypes;
	}

	public List<View_ConstantMedicineType> getmTypes() {
		if (mTypes == null)
			try {
				mTypes = infoLogic.getMedicineTypes();
			} catch (Exception ex) {
				userSessionController.showErrorMessage(ex.getMessage());
			}
		return mTypes;
	}


	public void setMeasurements(List<Measurement> measurements) {
		this.measurements = measurements;
	}

	public List<Measurement> getMeasurements() {
		if (measurements == null)
			try {
				measurements = infoLogic.getMeasurements();
			} catch (Exception ex) {
				userSessionController.showErrorMessage(ex.getMessage());
			}
		return measurements;
	}

	public List<String> getListMedicineGroup() {
		if(listMedicineGroup == null) {
			try{
				listMedicineGroup = infoLogic.getNotUseGroup();
			}catch(Exception ex){
				getUserSessionController().showErrorMessage(ex.getMessage());
			}
		}
		return listMedicineGroup;
	}

	public List<Injection> getInjections() {
		if(injections==null)
			injections =  new ArrayList<>();
		return injections;
	}

	public void setInjections(List<Injection> injections) {
		this.injections = injections;
	}

	public Injection getCurrentInjection() {
		if(currentInjection==null)
			currentInjection =  new Injection();
		return currentInjection;
	}

	public void setCurrentInjection(Injection currentInjection) {
		this.currentInjection = currentInjection;
	}

	public List<Item> getListItem() {
		return listItem;
	}

	public void setListItem(List<Item> listItem) {
		this.listItem = listItem;
	}
}