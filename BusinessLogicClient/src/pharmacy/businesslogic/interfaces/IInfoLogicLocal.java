package pharmacy.businesslogic.interfaces;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import pharmacy.businessentity.CustomerMedicineDeispense;
import pharmacy.businessentity.LoggedUser;
import pharmacy.entity.*;

@Local
public interface IInfoLogicLocal {
	
	public List<SubOrganization> getListSubOrganization() throws Exception;
	public List<Menu> getMenus() throws Exception;
	public List<Menu> getMenusFilter(LoggedUser loggedUser) throws Exception;
	public List<Menu> getMenus(LoggedUser loggedUser) throws Exception;
	public List<Menu> getListMenus(LoggedUser loggedUser) throws Exception;
	public SystemConfig getLicense() throws Exception;
	public void saveLicense(SystemConfig config) throws Exception;
	public List<Customer> getCustomers(int first, int count, String sortField, String sortType, Map<String, String> filters)
			throws Exception;
	public long getCustomerCount(Map<String, String> filters) throws Exception;
	public <T> List<T> getAll(Class<T> type) throws Exception;
	public List<Employee> getEmployeeInfo() throws Exception;
	public List<Medicine> getNotUsedTogetherMedicineList(BigDecimal medicinePkId) throws Exception;
	public PregnantWarning getPregnantWarning(BigDecimal medicinePkId) throws Exception;
	public WarningAge getWarningAge(BigDecimal medicinePkId) throws Exception;
	public WarningMedicineMaxDay getWarningMedicineMaxDay(BigDecimal medicinePkId) throws Exception;
	public WarningMedicineDose getWarningMedicineDose(BigDecimal medicinePkId) throws Exception;
	public List<Medicine> getMedicineForList(String usageType, LoggedUser lu, String filterKey, byte filterActiveMed, int month, boolean filterHasDR, int filterDrugPurpose) throws Exception;
	public List<Medicine> getMedicineByListId(List<String> listId) throws Exception;
	public void saveMedicineByList(List<Medicine> listMedicine) throws Exception;
	public List<String> getNotUseGroup(BigDecimal medicinePkId) throws Exception;
	public List<String> getNotUseGroup() throws Exception;
	public List<Medicine> getListMedicineByGroup(String groupId) throws Exception;
	public List<Medicine>				getMedicine(BigDecimal atcPkId, LoggedUser lu, String filterKey) throws Exception;
	public void saveMedicine(Medicine medicine, LoggedUser lu) throws Exception;
	public void saveMedicine(Medicine medicine, boolean notUsedTogetherMedicineListCheck, List<Medicine> notUsedTogetherMedicineList, boolean pregnantWarningCheck, PregnantWarning pregnantWarning, boolean warningAgeCheck, WarningAge warningAge, boolean warningMedicineMaxDayCheck, WarningMedicineMaxDay warningMedicineMaxDay, boolean warningMedicineDoseCheck, WarningMedicineDose warningMedicineDose, boolean notUseGroupCheck, List<String> notUseGroup, LoggedUser lu, Date usageDate) throws Exception;
	public List<MedicinePrice> getMedicinePrice(BigDecimal pkId) throws Exception;
	public List<View_ConstantATC> getAtcs() throws Exception;
	public List<View_ConstantMedicineType> getMedicineTypes() throws Exception;
	public List<Measurement> getMeasurements() throws Exception;
	public List<Injection> getInjection(BigDecimal pkId, LoggedUser lu, String filterKey) throws Exception;
	public void saveInjection(LoggedUser lu, Injection injection) throws Exception;
	public List<Item> getItemList() throws Exception;
	public List<Object[]> getConnectionPharmacy(String procedure) throws Exception;
	public List<CustomerMedicineDeispense> getItemWharehouse(BigDecimal pkId ) throws Exception;
}
