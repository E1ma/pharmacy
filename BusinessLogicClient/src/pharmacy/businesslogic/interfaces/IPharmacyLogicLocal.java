package pharmacy.businesslogic.interfaces;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import pharmacy.businessentity.*;
import pharmacy.entity.*;

@Local
public interface IPharmacyLogicLocal {
	public List<CustomerMedicineDeispense> getCustomerMedicineOcs(BigDecimal typeSelect) throws  Exception;
	public List<CustomerMedicineDeispense>  getEmployees() throws Exception;
	public List<Diagnose>		getCusromerDiagnoseList(BigDecimal customerPkId) throws Exception;
	public List<CustomerMedicineDeispense>	getPharmacyDispense(Date date,BigDecimal selectedCustomerPkId,BigDecimal employeePkId,BigDecimal subPkId,int val) throws Exception;
	public List<SubOrganization> getSubOrganizations() throws  Exception;
	public List<CustomerMedicineDeispense> getCustomerMedicineCt(BigDecimal customerPkId)  throws Exception;
	public List<CustomerMedicineDeispense> getCustomerMedicineDate(BigDecimal customerPkId,List<Date> date) throws Exception;
	public List<CustomerMedicine> getCustomerMedicine(BigDecimal customerPkId, Date beginDate, Date endDate, BigDecimal employeePkId, String filterKey) throws Exception;
	public List<CustomerMedicineDeispense> getPharmacyDates(BigDecimal customerPkId) throws Exception;
	public List<ExaminationRequestCompleted> getExaminationRequestCompletedByFilter(List<BigDecimal> subOrgaPkId, List<BigDecimal> employeePkId, BigDecimal customerPkId) throws Exception;
	public List<OutcomeInfo> getOutcomeInfoList(Date outcomeDate, BigDecimal employeePkId, String filterStr) throws Exception;
	public List<CustomerMedicineDeispense> getPharmacyDispenseLst(BigDecimal pkId,BigDecimal type,Date date,Date endDate) throws Exception;
	public List<CustomerMedicineDeispense> getPharmacyDispenseDates(BigDecimal hdrpkId,BigDecimal medPkId,Date date) throws Exception;
	public List<InspectionForm> getPharmacyEmr(BigDecimal hdrPkId,String  type) throws Exception;
	public List<VitalSign> getVitalSign(BigDecimal pkId) throws Exception;
	public List<CustomerMedicineDeispense> getMedicinePackagePrint(BigDecimal pkId) throws Exception;
	public List<CustomerMedicineDeispense> getPharmacyBeforeOrder(BigDecimal pkId) throws Exception;
	public List<CustomerMedicineDeispense> getPresriptionMedicines(BigDecimal invPkId,Date bDate,Date eDate) throws Exception;
	public List<CustomerMedicineDeispense> getPackagePrintDates(BigDecimal invPkId) throws Exception;
	public List<CustomerMedicineDeispense> getItemAll() throws Exception;
	public List<CustomerMedicineDeispense> getCustomerList() throws Exception;
	public List<PharmacyImformation> getOrderPharmacy(Date date,Date beginDate)  throws Exception;
	public PharmacyImformation getOrderPharmacyDispense(String id,String name,BigDecimal type,Date date,Date bDate)  throws Exception;
	
	public List<PharmacyImformation> getOrderSubOrganization(String id,String name,Date date,Date eDate)  throws Exception;
	
	
	public CustomerMedicineDeispense getWarehouse(BigDecimal itemPkId) throws Exception;
	
	public void updatePharmacy(InvoiceDtl dtl,LoggedUser user) throws Exception;
	public Role getRoleUser(LoggedUser  user) throws Exception; 
	public CustomerMedicineDeispense getCustomerMedicineDtl(BigDecimal pkId) throws Exception;
	public CustomerMedicineDeispense getNursingFilter(BigDecimal servicePkId,int time) throws Exception;
	public void saveCustomerMedicine(LoggedUser loggedInfo, List<CustomerMedicineDeispense> customerMedicineDeispenses) throws Exception;
	public void sendSaveItem(List<CustomerMedicineDeispense> customerMedicineDeispenses) throws Exception;
	public void connectionPharmacy(String storedProcedure) throws Exception;
	public List<Object[]> getConnectionPharmacy(String storedProcedure) throws Exception;
	public List<CustomerMedicineDeispense> getCustomerItem(BigDecimal invHdrPkId) throws Exception;
	public List<PharmacyImformation> getItemReport(Date date,Date endDate) throws Exception;
	
	
	public List<CustomerMedicineDeispense> getMedicineSubOrganizationReport(Date bDate,Date eDate) throws Exception;
	
	
}
