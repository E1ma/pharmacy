package pharmacy.businesslogic.interfaces;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import pharmacy.businessentity.*;
import pharmacy.entity.*;

@Local
public interface IConfigLogicLocal {

	public List<SubOrganization> getSubOrganizations() throws Exception;
	public void saveAccountConfig(SubOrgAccountMap map, List<SubOrgAccountMap> listAccountMap) throws Exception;
	public List<SubOrgAccountMap> getListData() throws Exception;
	public List<SubOrgAccountMap> getAllAccount() throws Exception;
	public List<Item> getAllItem() throws Exception;
}
