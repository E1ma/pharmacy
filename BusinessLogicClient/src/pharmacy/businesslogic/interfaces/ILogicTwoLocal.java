package pharmacy.businesslogic.interfaces;

import pharmacy.businessentity.LoggedUser;

import java.math.BigDecimal;

import javax.ejb.Local;

@Local
public interface ILogicTwoLocal {
	
	public void changePassword(LoggedUser loggedUser, String newPassword) throws Exception;
}
